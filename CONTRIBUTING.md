# Contribuindo para o br.gnome.org

This file is written in Portuguese as probably most of the readers
are Brazilians. If that is not your case and you want to read in
English, please see [CONTRIBUTING-EN.md]

Obrigado pelo seu interesse em nosso projeto!

Aqui você encontra algumas informações para guiá-lo no processo de
participar da construção e manutenção do página web da comunidade
brasileira do GNOME, seja relatando erros, solicitando alguma
alteração ou até mesmo submetendo propostas de alterações.

## Como posso ajudar

Se você ainda não nos contatou, sinta-se à vontade para fazê-lo nos
meios de comunicação abaixo.

Seguem aqui alguns recursos:
 - Mailing list for general subjects:
    [gnome-br-list](https://mail.gnome.org/mailman/listinfo/gnome-br-list)
 - Mailing list for translation:
    [gnome-pt_br-list](https://mail.gnome.org/mailman/listinfo/gnome-pt_br-list)
 - IRC: [#gnome-br at irc.gimp.org](irc://irc.gnome.org/gnome-br)
 - Erros ou melhorias: Relate uma
    [issue](https://gitlab.gnome.org/Infrastructure/br.gnome.org/issues) 
    ou proponha uma
    [merge request](https://gitlab.gnome.org/Infrastructure/br.gnome.org/merge_requests)

### Submetendo alterações

Se você tiver uma proposta de alteração ao código-fonte use, de preferência,
o sistema de Merge Request do Gitlab. Caso não esteja familiarizado com esse
recurso, pode utilizar algum dos recursos acima.

Ao propor _merge requests_, procure seguir as convenções de formatação abaixo
e procure manter seus commits atômicos (um recurso por commit).

Sempre escreva uma mensagem de log limpa em seus commits. Mensagens de uma
linha são ótimas para pequenas alterações, mas grandes alterações ficam
melhor descritas dessa forma:

```
 $ git commit -m "This is a brief summary of the commit
 >
 > A paragraph describing what changed and its impact."
 
```

### Convenções de formatação

TODO