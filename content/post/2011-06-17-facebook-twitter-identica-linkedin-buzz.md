---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
date: 2011-06-17 21:42:13
guid: http://gnome.gr/?p=940
id: 940
permalink: /2011/06/17/facebook-twitter-identica-linkedin-buzz/
tags:
  - facebook
  - identi.ca
  - linkedin
  - twitter
title: Κοινωνική δικτύωση και κοινότητα GNOME.gr
url: /2011/06/17/facebook-twitter-identica-linkedin-buzz/
---

Η κοινωνική δικτύωση είναι σημαντική για την προώθηση ακόμα περισσότερο του έργου που κάνει η ελληνική κοινότητα GNOME.

Για τα παρακάτω θα ήταν εξαιρετικό αν κάνετε follow, friend, Like, κτλ το χρήστη «gnomegr», που είναι η οντότητα της ελληνικής κοινότητας GNOME στα κοινωνικά δίκτυα.

&nbsp;

[<img class="alignnone img-responsive " title="Identi.ca" src="http://gnome.gr/wp-content/uploads/2011/06/identica.png" alt="Identi.ca" />](http://identi.ca/gnomegr)
  
**Identi.ca**
  
Ο λογαριασμός μας είναι
  
<http://identi.ca/gnomegr>

&nbsp;

[<img class="alignnone img-responsive " title="Twitter" src="http://gnome.gr/wp-content/uploads/2011/06/twitter.png" alt="Twitter" />](http://twitter.com/gnomegr/)  ****
  
**Twitter**
  
Ο λογαριασμός μας είναι
  
<http://twitter.com/gnomegr/>

&nbsp;

[<img class="alignnone img-responsive " title="Facebook" src="http://gnome.gr/wp-content/uploads/2011/06/facebook.png" alt="Facebook" />](https://www.facebook.com/gnomegr)
  
**Facebook**
  
Η σελίδα μας είναι [](http://on.fb.me/l02tiq)<http://on.fb.me/kyT4eY>
  
πλήρης URL: [Gnomegr@facebook](https://www.facebook.com/gnomegr)
  
Υπάρχει επίσης και το group: <http://on.fb.me/l02tiq>

Όσοι είστε χρήστες Facebook, μπορείτε να μαθαίνετε νέα της κοινότητας, να συμμετέχετε σε εκδηλώσεις και συζητήσεις.
  
Όσοι δεν είστε εξοικειωμένοι στο IRC (#gnome-el) για συζητήσεις, μπορείτε να συζητήσετε με μέλη της κοινότητας στο Facebook.

&nbsp;

[<img class="alignnone img-responsive " title="Linkedin" src="http://gnome.gr/wp-content/uploads/2011/06/linkedin.png" alt="Linkedin" />](http://www.linkedin.com/e/ds5zik-goxxth40-5t/vgh/3958031/eml-grp-sub/)
  
Linkedin
  
Υπάρχει το group gnomegr, <http://linkd.in/mHGOIb>
  
πλήρες σύνδεσμος:
  
<http://www.linkedin.com/e/ds5zik-goxxth40-5t/vgh/3958031/eml-grp-sub/>

&nbsp;

[<img class="alignnone img-responsive " title="Buzz" src="http://gnome.gr/wp-content/uploads/2011/06/buzz.png" alt="Buzz" />](http://mailto:gnomegr@gmail.com)
  
**Buzz (Google)**
  
Αν έχετε λογαριασμό GMail και έχετε ενεργή την υπηρεσία Buzz, μπορείτε να προσθέσετε <gnomegr@gmail.com>