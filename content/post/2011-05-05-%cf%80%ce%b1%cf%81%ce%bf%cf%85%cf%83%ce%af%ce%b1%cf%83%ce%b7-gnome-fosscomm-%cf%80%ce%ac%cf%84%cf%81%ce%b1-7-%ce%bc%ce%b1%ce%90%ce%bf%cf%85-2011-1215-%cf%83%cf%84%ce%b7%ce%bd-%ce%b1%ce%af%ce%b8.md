---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2011-05-05 22:15:08
guid: http://gnome.gr/?p=878
id: 878
permalink: /2011/05/05/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-gnome-fosscomm-%cf%80%ce%ac%cf%84%cf%81%ce%b1-7-%ce%bc%ce%b1%ce%90%ce%bf%cf%85-2011-1215-%cf%83%cf%84%ce%b7%ce%bd-%ce%b1%ce%af%ce%b8/
title: 'Παρουσίαση GNOME: FOSSCOMM Πάτρα, 7 Μαΐου 2011, 12:15 στην Αίθουσα Σεμιναρίων'
url: /2011/05/05/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-gnome-fosscomm-%cf%80%ce%ac%cf%84%cf%81%ce%b1-7-%ce%bc%ce%b1%ce%90%ce%bf%cf%85-2011-1215-%cf%83%cf%84%ce%b7%ce%bd-%ce%b1%ce%af%ce%b8/
---

[<img class="size-medium wp-image-879 img-responsive " title="FOSSCOMM ΠαρουσίασηGNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM-ΠαρουσίασηGNOME-300x225.png" alt="FOSSCOMM ΠαρουσίασηGNOME" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM-ΠαρουσίασηGNOME.png) 

Το <a title="FOSSCOMM Patras" href="http://patras.fosscomm.gr/" target="_blank">συνέδριο κοινότητων ΕΛ/ΛΑΚ FOSSCOMM θα γίνει φέτος στην Πάτρα</a>, 7-8 Μαΐου 2011.

Η ελληνική κοινότητα GNOME θα είναι εκεί με τον <a title="Ευστάθιος Ιωσηφίδης" href="http://stathisuse.blogspot.com/" target="_blank">Ευστάθιο Ιωσηφίδη</a>, για την παρουσίαση «Ελληνική Κοινότητα GNOME, Γνωριμία με το GNOME 3». Η παρουσίαση θα γίνει το Σάββατο 7 Μαΐου 2011, **στις 12:15**, στην <a title="Αίθουσα Σεμιναρίων" href="http://patras.fosscomm.gr/location/" target="_blank">Αίθουσα Σεμιναρίων</a> της FOSSCOMM.

[<img class="size-medium wp-image-880 img-responsive " title="Φυλλάδιο GNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM-LeafletGNOME-225x300.png" alt="Φυλλάδιο GNOME" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM-LeafletGNOME.png) 

Για το stand του GNOME ο Στάθης ετοίμασε φυλλάδια και CD με το GNOME 3.

[<img class="size-medium wp-image-881 img-responsive " title="Κονκάρδες GNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/GNOME-buttons-211x300.png" alt="Κονκάρδες GNOME" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/GNOME-buttons.png) 

Ο Στάθης έφτιαξε και κονκάρδες για το GNOME!

Οπότε

  * **σημειώστε την ώρα 12:15 το Σάββατο**, στην Αίθουσα Σεμιναρίων (πρώτος όροφος) για την Παρουσίαση του GNOME
  * περάστε από τον πάγκο (Stand) του GNOME για να πάρετε την κονκάρδα σας (πρέπει να πάτε νωρίς για να προλάβετε!). Ο πάγκος του GNOME θα είναι δίπλα στον πάγκο της <a title="Ελληνική κοινότητα OpenSUSE" href="http://opensuseambassadors.blogspot.com/" target="_blank">ελληνικής κοινότητας OpenSUSE</a>.
