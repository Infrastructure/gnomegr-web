---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
date: 2013-07-28 14:29:30
guid: http://gnome.gr/?p=2071
id: 2071
image: /wp-content/uploads/sites/7/2012/12/GUADEC2013.png
permalink: /2013/07/28/guadec-2013-program/
tags:
  - GUADEC
title: Ανακοινώθηκε το πρόγραμμα του συνεδρίου GUADEC 2013
url: /2013/07/28/guadec-2013-program/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/12/GUADEC2013-300x199.png" alt="GUADEC 2013" class="aligncenter size-medium wp-image-1998 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/12/GUADEC2013.png)

Το <a href="http://guadec.org/" target="_blank">GUADEC</a> είναι το κυρίως συνέδριο για το GNOME και όπως είναι γνωστό θα διεξαχθεί από 1 έως 8 Αυγούστου στο Brno της Τσεχίας.

Έχει ανακοινωθεί το κυρίως πρόγραμμα του συνεδρίου (<a href="https://www.guadec.org/schedule/" target="_blank">δείτε εδώ</a>). Θα υπάρξει ένα σύνολο 40 ομιλιών, 4 κύριες ομιλίες που θα ανοίξουν τις ημέρες του συνεδρίου αλλά και μεγάλος αριθμός lightning talk.

Τις τέσσερις κύριες ημέρες θα ακολουθούν τρεις ημέρες από διάφορες άλλες εκδηλώσεις και hacking sessions, όπου δίνουν την ευκαιρία στην κοινότητα GNOME να συνεργαστεί και να προγραμματίσει μαζί μελλοντικές κινήσεις της κοινότητας.

Για περισσότερες πληροφορίες σχετικά με την φετινή GUADEC θα βρείτε στην <a href="http://guadec.org/" target="_blank">ιστοσελίδα του συνεδρίου</a>.
