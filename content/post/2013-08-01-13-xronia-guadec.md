---
author: iosifidis
categories:
  - Εκδηλώσεις
date: 2013-08-01 10:14:54
guid: http://gnome.gr/?p=2095
id: 2095
permalink: /2013/08/01/13-xronia-guadec/
tags:
  - gnome
  - GUADEC
title: Δεκατρία χρόνια GUADEC και συνεχίζουμε&#8230;
url: /2013/08/01/13-xronia-guadec/
---

Το συνέδριο GUADEC είναι το ετήσιο Ευρωπαϊκό συνέδριο, η κύρια εκδήλωση για το GNOME. Με το <a href="https://www.guadec.org" target="_blank">14ο GUADEC έτοιμο να ξεκινήσει</a>, αποφασίσαμε να ρίξουμε μια ματιά σε μερικά παλιότερα συνέδρια και να δούμε πως ξεκίνησε η εκδήλωση αυτή.

Το ετήσιο Ευρωπαϊκό συνέδριο εμφανίστηκε ως ιδέα στις αρχές της δημιουργίας του έργου GNOME, όταν μερικοί προγραμματιστές θέλησαν να συναντηθούν από κοντά. Μέχρι τότε είχαν μιλήσει μόνο στο Internet. Ξεκίνησε μια προσπάθεια για να συγκέντρωση πόρων από διάφορες εταιρίες Ανοικτού Λογισμικού στις ΗΠΑ, Γερμανία και Γαλλία. Οι οργανωτές κατάφεραν να μαζέψουν χορηγίες για να χρηματοδοτήσουν 40 βασικούς προγραμματιστές του GNOME σε μια συνάντηση στο Παρίσι για 4 ημέρες. Το πρώτο συνέδριο GUADEC έλαβε μέρος το 2000 και διεξάγεται κάθε χρόνο από τότε.<figure id="attachment_2096" style="width: 640px" class="wp-caption aligncenter">

[<img class="size-full wp-image-2096 img-responsive " alt="Το πρώτο συνέδριο GUADEC, που διεξήχθη στο Παρίσι, το 2000" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/08/guadec-paris.jpg" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/08/guadec-paris.jpg)<figcaption class="wp-caption-text">Το πρώτο συνέδριο GUADEC, που διεξήχθη στο Παρίσι, το 2000</figcaption></figure> 

Από την πρώτη διοργάνωση, το συνέδριο GUADEC έχει μεγαλώσει. Στο συνέδριο αυτό συναντώνται άτομα που συνεισφέρουν στο GNOME αλλά και φίλοι του GNOME από όλο τον κόσμο και τους δίνεται η ευκαιρία να μοιραστούν μεταξύ τους εμπειρίες και ιδέες. Συμμετέχουν πολλοί προγραμματιστές λογισμικού, χρήστες, καλλιτέχνες καθώς και εκπρόσωποι από επιχειρήσεις, την κυβέρνηση και την εκπαίδευση. Υπάρχουν παρουσιάσεις από τους εμπνευστές κάποιου έργου, από προγραμματιστές ακόμα και από εθελοντές πάνω σε διάφορα θέματα συμπεριλαμβανομένων τεχνικών ανάπτυξης λογισμικού, νέα χαρακτηριστικά, σχέδια για το μέλλον και ιδέες για την τεχνολογία και τον πολιτισμό.

Birds of a Feather (BoF) και hackfests προστέθηκαν στο πρόγραμμα του συνεδρίου το 2008 και από τότε θα τα βρούμε μόνιμα στο πρόγραμμα των GUADEC. Αυτές οι συναντήσεις δίνουν την ευκαιρία στους προγραμματιστές να εργαστούν μαζί και να αναπτύξουν νέες στρατηγικές και τεχνικές. Το συνέδριο είναι μια εκδήλωση της κοινότητα και οργανώνεται κάθε χρόνο από τοπικούς εθελοντές. Κάθε χρόνο οι ομάδες μπορούν να κάνουν αίτηση να διοργανώσουν την εκδήλωση της επόμενης χρονιάς. Ακόμα υπάρχει η διαδικασία για το GUADEC 2015.

#gallery-1 {
				  
margin: auto;
			  
}
			  
#gallery-1 .gallery-item {
				  
float: left;
				  
margin-top: 10px;
				  
text-align: center;
				  
width: 33%;
			  
}
			  
#gallery-1 img {
				  
border: 2px solid #cfcfcf;
			  
}
			  
#gallery-1 .gallery-caption {
				  
margin-left: 0;
			  
}

<!-- see gallery_shortcode() in wp-includes/media.php -->

<div id='gallery-1' class='gallery galleryid-4289 gallery-columns-3 gallery-size-thumbnail'>
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/20120724-DSCF2298.jpg' title='Jon McCann'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20120724-DSCF2298-210x140.jpg" class="attachment-thumbnail img-responsive " alt="William Jon McCann at the GUADEC 2012 UX Hackfest (Copyright Garrett LeSage)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      William Jon McCann at the GUADEC 2012 UX Hackfest (Copyright Garrett LeSage)
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/20090712-IMG_4971.jpg' title='Traveling GNOME'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20090712-IMG_4971-210x140.jpg" class="attachment-thumbnail img-responsive " alt="The famous traveling GNOME makes an appearance at GUADEC 2009 (Copyright Garrett LeSage)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      The famous traveling GNOME makes an appearance at GUADEC 2009 (Copyright Garrett LeSage)
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/20050530-crw_5757.jpg' title='Stuttgart'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20050530-crw_5757-210x140.jpg" class="attachment-thumbnail img-responsive " alt="GUADEC 2005, Stuttgart﻿ (Copyright Garrett LeSage)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      GUADEC 2005, Stuttgart﻿ (Copyright Garrett LeSage)
    </dd>
  </dl>
  
  <p>
    <br style="clear: both" />
  </p>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/birthday-cake.jpg' title='GNOME&#039;s Birthday Cake'><img src="http://www.gnome.org/wp-content/uploads/2013/07/birthday-cake-210x140.jpg" class="attachment-thumbnail img-responsive " alt="GNOME&#039;s 10 Year Anniversary Birthday Cake (Copyright James Scott Remnant, CC BY-NC-ND 2.0)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      GNOME&#8217;s 10 Year Anniversary Birthday Cake (Copyright James Scott Remnant, CC BY-NC-ND 2.0)
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/20120730-DSCF2815.jpg' title='Coastal Figures'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20120730-DSCF2815-210x147.jpg" class="attachment-thumbnail img-responsive " alt="A Coruña, 2012 (Copyright Garrett LeSage)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      A Coruña, 2012 (Copyright Garrett LeSage)
    </dd>
  </dl>
  
  <dl class='gallery-item'>
    <dt class='gallery-icon'>
      <a href='http://www.gnome.org/wp-content/uploads/2013/07/hackfest-at-525.jpg' title='hackfest at 525'><img src="http://www.gnome.org/wp-content/uploads/2013/07/hackfest-at-525-210x157.jpg" class="attachment-thumbnail img-responsive " alt="An outdoor hacking session at GUADEC 2006 (Copyright Pedro Villavicencio, CC BY-SA 2.0)" /></a>
    </dt>
    
    <dd class='wp-caption-text gallery-caption'>
      An outdoor hacking session at GUADEC 2006 (Copyright Pedro Villavicencio, CC BY-SA 2.0)
    </dd>
  </dl>
  
  <p>
    <br style="clear: both" /><br /> <br style='clear: both' /> </div> 
    
    <p>
      Μέχρι σήμερα είχαμε 13 συνέδρια GUADEC. Η διοργάνωση γινόταν κάθε φορά σε διαφορετική Ευρωπαϊκή πόλη. Μέχρι στιγμής το συνέδριο GUADEC επισκέφθηκε τις πόλεις: Paris (2000), Copenhagen (2001), Seville (2002), Dublin (2003), Kirstiansand (2004), Stuttgart (2005), Vilanova (2006), Birmingham (2007), Istanbul (2008), Gran Canaria (2009), The Hague (2010), Berlin (2011) και A Coruña (2012). Αυτή τη χρονιά την διεξαγωγή του συνεδρίου GUADEC έχει αναλάβει η τοπική ομάδα του Brno (Τσεχία), μια φοιτητούπολη που όμως είναι η έδρα πολλών γνωστών εταιριών πληροφορικής όπως IBM, Motorola, Honeywell και Red Hat.<br /> Το συνέδριο αφενός είναι το σημείο όπου γίνονται σοβαρές συζητήσεις αλλά δεν είναι μόνο ένα συνέδριο πληροφορικής. Δίνεται η ευκαιρία στους προγραμματιστές που συνεργάζονται μέσω του δικτύου, να βρεθούν από κοντά και να διασκεδάσουν, να κάνουν νέες φιλίες αλλά να συναντήσουν και παλιούς φίλους.
    </p>
    
    <p>
      #gallery-2 {<br /> margin: auto;<br /> }<br /> #gallery-2 .gallery-item {<br /> float: left;<br /> margin-top: 10px;<br /> text-align: center;<br /> width: 33%;<br /> }<br /> #gallery-2 img {<br /> border: 2px solid #cfcfcf;<br /> }<br /> #gallery-2 .gallery-caption {<br /> margin-left: 0;<br /> }
    </p>
    
    <p>
      <!-- see gallery_shortcode() in wp-includes/media.php -->
    </p>
    
    <div id='gallery-2' class='gallery galleryid-4289 gallery-columns-3 gallery-size-thumbnail'>
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/20090704-IMG_3669.jpg' title='Richard Stallman'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20090704-IMG_3669-140x210.jpg" class="attachment-thumbnail img-responsive " alt="Richard Stallman speaks at the 2009 Gran Canaria Desktop Summit (Copyright Garrett LeSage)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          Richard Stallman speaks at the 2009 Gran Canaria Desktop Summit (Copyright Garrett LeSage)
        </dd>
      </dl>
      
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/down-with-the-release-team.jpg' title='Down With The Release Team'><img src="http://www.gnome.org/wp-content/uploads/2013/07/down-with-the-release-team-210x140.jpg" class="attachment-thumbnail img-responsive " alt="A tongue in cheek protest at GUADEC 2012 (Copyright Claudio Saavendra, CC BY-NC-SA 2.0)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          A tongue in cheek protest at GUADEC 2012 (Copyright Claudio Saavendra, CC BY-NC-SA 2.0)
        </dd>
      </dl>
      
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/flaming-booze.jpg' title='Queimada'><img src="http://www.gnome.org/wp-content/uploads/2013/07/flaming-booze-210x140.jpg" class="attachment-thumbnail img-responsive " alt="Queimada, a traditional Galician drink, is made at GUADEC 2012 (Copyright Ana Rey, CC BY-SA 2.0)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          Queimada, a traditional Galician drink, is made at GUADEC 2012 (Copyright Ana Rey, CC BY-SA 2.0)
        </dd>
      </dl>
      
      <p>
        <br style="clear: both" />
      </p>
      
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/desktop-summit.jpg' title='Desktop Summit'><img src="http://www.gnome.org/wp-content/uploads/2013/07/desktop-summit-210x157.jpg" class="attachment-thumbnail img-responsive " alt="The 2011 Desktop Summit in Berlin (Copyright Alberto Garcia, CC BY-SA 2.0)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          The 2011 Desktop Summit in Berlin (Copyright Alberto Garcia, CC BY-SA 2.0)
        </dd>
      </dl>
      
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/20120731-DSCF3017.jpg' title='New Recruits'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20120731-DSCF3017-210x118.jpg" class="attachment-thumbnail img-responsive " alt="New contributors at GUADEC 2012 (Copyright Garrett LeSage)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          New contributors at GUADEC 2012 (Copyright Garrett LeSage)
        </dd>
      </dl>
      
      <dl class='gallery-item'>
        <dt class='gallery-icon'>
          <a href='http://www.gnome.org/wp-content/uploads/2013/07/20090711-IMG_4943.jpg' title='Jumping People'><img src="http://www.gnome.org/wp-content/uploads/2013/07/20090711-IMG_4943-210x118.jpg" class="attachment-thumbnail img-responsive " alt="Gran Canaria, 2009 (Copyright Garrett LeSage)" /></a>
        </dt>
        
        <dd class='wp-caption-text gallery-caption'>
          Gran Canaria, 2009 (Copyright Garrett LeSage)
        </dd>
      </dl>
      
      <p>
        <br style="clear: both" /><br /> <br style='clear: both' /> </div> 
        
        <p>
          Με το πέρας των χρόνων, στα συνέδρια δημιουργήθηκαν πολλές παραδόσεις. Ένας ετήσιος ποδοσφαιρικός αγώνας είναι τακτικό γεγονός όπως επίσης και η παρουσίαση του βραβείου GNOME «pants». Με τοπέρας των GUADEC έγινε ανάγκη στην κοινότητα GNOME να φτιάξει ένα συγκρότημα που θα παίζει (μερικές φορές καλά, συνήθως όχι και τόσο) σε ένα από τα parties.
        </p>
        
        <div id="attachment_4352" class="wp-caption alignleft" style="width: 430px">
          <a href="http://www.gnome.org/wp-content/uploads/2013/07/gnome-band.jpg"><img src="http://www.gnome.org/wp-content/uploads/2013/07/gnome-band-420x280.jpg" alt="One of the outings of the GUADEC band (Copyright Jesús Corrius, CC BY)" class="size-medium wp-image-4352 img-responsive " /></a></p> 
          
          <p class="wp-caption-text">
            One of the outings of the GUADEC band (Copyright Jesús Corrius, CC BY)
          </p>
        </div>
        
        <p>
          Μια χρονιά είχε διοργανωθεί διαγωνισμός κατανάλωσης παγωτού με μεγάλη επιτυχία.
        </p>
        
        <div id="attachment_4346" class="wp-caption alignleft" style="width: 430px">
          <a href="http://www.gnome.org/wp-content/uploads/2013/07/ice-cream-death-match.jpg"><img src="http://www.gnome.org/wp-content/uploads/2013/07/ice-cream-death-match-420x279.jpg" alt="One of the GUADEC Ice Cream Death Match competitions (Copyright penguincakes, CC BY-NC-SA 2.0)" class="size-medium wp-image-4346 img-responsive " /></a></p> 
          
          <p class="wp-caption-text">
            One of the GUADEC Ice Cream Death Match competitions (Copyright penguincakes, CC BY-NC-SA 2.0)
          </p>
        </div>
        
        <p>
          Και δεν θα ήταν ένα ολοκληρωμένο συνέδριο εάν έλλειπε το ιδιότυπο χιούμορ. Αυτό συχνά οδηγούσε σε φάρσες&#8230;
        </p>
        
        <div id="attachment_4338" class="wp-caption alignleft" style="width: 430px">
          <a href="http://www.gnome.org/wp-content/uploads/2013/07/i-am-vincent-untz.jpg"><img src="http://www.gnome.org/wp-content/uploads/2013/07/i-am-vincent-untz-420x315.jpg" alt="Ποτέ μην εμπιστεύεστε κάποιον άλλον να γράψει τις διαφάνειες για εσάς. (Copyright Allan Day, CC BY-NC-SA 2.0)" class="size-medium wp-image-4338 img-responsive " /></a></p> 
          
          <p class="wp-caption-text">
            Never trust someone else to write your slides for you
          </p>
        </div>
        
        <p>
          Ραντεβού στο συνέδριο <a href="https://www.guadec.org/" target="_blank">GUADEC 2013</a>.
        </p>
