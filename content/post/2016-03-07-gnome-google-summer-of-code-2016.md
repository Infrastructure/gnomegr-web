---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2016-03-07 12:29:59
guid: https://static.gnome.org/gnome-gr/?p=3306
id: 3306
permalink: /2016/03/07/gnome-google-summer-of-code-2016/
tags:
  - gnome
  - Google Summer of Code
  - GSoC
  - ΕΛ/ΛΑΚ
title: Συμμετοχή του GNOME στο Google Summer of Code 2016
url: /2016/03/07/gnome-google-summer-of-code-2016/
---

<img class=" img-responsive " src="https://www.gnome.org/wp-content/uploads/2016/03/GSoC2016Logo.jpg" alt="Google Summer of Code" />

Με χαρά σας ανακοινώνουμε ότι το GNOME έχει επιλεγεί να συμμετέχει στο <a href="https://summerofcode.withgoogle.com/" target="_blank">Google Summer of Code 2016</a>. Το GNOME συμμετέχει στο πρόγραμμα κάθε χρόνο από την έναρξή του το 2005 και με χαρά συμμετέχουμε ακόμα μια φορά!

Το Google Summer of Code επιτρέπει στους φοιτητές να εργαστούν το καλοκαίρι για ένα έργο Ανοικτού Λογισμικού. Οι φοιτητές που γίνονται δεκτοί λαμβάνουν μια οικονομική βοήθεια ώστε να επικεντρωθούν στην πρακτική τους, αντί να βρουν κάποια καλοκαιρινή δουλειά (όπως λένε «flip bits, not burgers»), και καθοδηγούνται από πεπειραμένους του έργου. Είναι μεγάλη ευκαιρία για τους φοιτητές να αναμιχθούν σε μια <a href="https://summerofcode.withgoogle.com/organizations/" target="_blank">μεγάλη γκάμα από έργα Ελεύθερου Λογισμικού</a>.

Οι φοιτητές και οι μέντορες μπορούν να βρουν περισσότερες πληροφορίες σχετικά με το πρόγραμμα στην σελίδα με το <a href="https://wiki.gnome.org/action/show/Outreach/SummerOfCode" target="_blank">Google Summer of Code</a>. Οι φοιτητές σύντομα θα μπορούν να υποβάλλουν την αίτησή τους (η περίοδος ανοίγει στις 14 Μαρτίου) και θα μπορούν να έρχονται σε επαφή με τους υποψήφιους μέντορες.

**Πηγή:** <a href="https://www.gnome.org" target="_blank">GNOME.org</a>,
  
Η εικόνα Google Summer of Code παρέχεται υπό <a href="http://creativecommons.org/licenses/by-nc-nd/3.0/" target="_blank">Creative Commons BY-NC-ND 3.0</a>.