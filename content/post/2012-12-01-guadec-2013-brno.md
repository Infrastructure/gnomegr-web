---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
  - Εκδηλώσεις
date: 2012-12-01 15:08:11
guid: http://gnome.gr/?p=1997
id: 1997
image: /wp-content/uploads/sites/7/2012/12/GUADEC2013.png
permalink: /2012/12/01/guadec-2013-brno/
tags:
  - Brno
  - GUADEC
title: Το συνέδριο GUADEC 2013 θα διεξαχθεί στο Brno της Τσεχίας
url: /2012/12/01/guadec-2013-brno/
---

<a href="http://gnome.gr/2012/12/01/guadec-2013-brno/guadec2013/" rel="attachment wp-att-1998"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/12/GUADEC2013.png" alt="" title="GUADEC 2013" class="aligncenter size-full wp-image-1998 img-responsive " /></a>

Όπως ανακοινώθηκε στην ιστοσελίδα του <a href="http://www.gnome.org/news/2012/11/brno-to-host-guadec-2013-and-strasbourg-to-host-guadec-2014/" target="_blank">GNOME.org</a>, το συνέδριο GUADEC για το 2013 θα πραγματοποιηθεί στο Brno της Τσεχίας από 1 έως 8 Αυγούστου. Το συνέδριο GUADEC είναι το σημείο συνάντησης για όλους τους χρήστες αλλά και τους προγραμματιστές του GNOME. Είναι ευκαιρία να συναντηθούμε με φίλους που μιλάμε καθημερινά στο IRC ή σε άλλα κανάλια επικοινωνίας.

Κατά τη διαδικασία αιτήσεων για την διοργάνωση του GUADEC για το 2013, υποβλήθηκε μια πρόταση από την ομάδα του Στρασβούργου (Γαλλία) που άρεσε πολύ στην ομάδα που αποφασίζει που θα διεξαχθεί το επόμενο συνέδριο. Ανακοινώθηκε λοιπόν ότι το συνέδριο το 2014 θα διεξαχθεί στο Στρασβούργο.

Για περισσότερες πληροφορίες για το συνέδριο, παρακολουθείτε την ιστοσελίδα <a href="http://www.guadec.org/" target="_blank">http://www.guadec.org/</a> αλλά και ενημερώσεις μέσω identi.ca / Twitter στο: **@guadec**.

Διαβάστε το πλήρες κείμενο στην ιστοσελίδα του <a href="http://www.gnome.org/press/2012/11/gnome-to-hold-guadec-2013-in-brno-guadec-2014-in-strasbourg/" target="_blank">GNOME</a>.