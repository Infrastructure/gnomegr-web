---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-02-18 10:53:57
guid: http://gnome.gr/?p=2213
id: 2213
image: /wp-content/uploads/sites/7/2014/02/opw-poster-2014-May-August.png
permalink: /2014/02/18/opw-2014-may-august/
tags:
  - GSoC
  - OPW
  - Outreach Program for Women
title: Ετοιμαστείτε για τον επόμενο γύρο για το Outreach Program for Women
url: /2014/02/18/opw-2014-may-august/
---

Αν και βρισκόμαστε στα μέσα του προηγούμενου γύρου του <a href="http://gnome.gr/2013/10/07/outreach-program-for-women/" target="_blank">Outreach Program for Women</a>, πρέπει να ανοίξουμε την διαδικασία υποβολής αιτήσεων διότι ακολουθούμε το πρόγραμμα του <a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2014" target="_blank">Google Summer of Code</a>.

Οι οργανισμοί που θα επιλεγούν να συμμετάσχουν στο GSoC θα ανακοινωθούν την επόμενη _Δευτέρα 24 Φεβρουαρίου_. Η καταληκτική ημερομηνία υποβολής αιτήσεων για το OPW θα είναι η **19η Μαρτίου** ενώ για το GSoC θα είναι η **21η Μαρτίου**. Οι ημερομηνίες και των 2 προγραμμάτων είναι από τις **19 Μαΐου** έως τις **18 Αυγούστου**.

Η Google ανέβασε το πληρωτέο ποσό στους φοιτητές σε $5500 και επειδή δεν θέλουμε οι συμμετέχοντες στο OPW να λαμβάνουν λιγότερα, ανεβάζουμε και εμείς το ποσό. Δείτε την ιστοσελίδα <a href="https://wiki.gnome.org/OutreachProgramForWomen/Admin/InfoForOrgs#Action" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen/Admin/InfoForOrgs#Action</a>. Σε προηγούμενους γύρους, ζητήθηκε από κάθε οργανισμό να χορηγήσει μια συμμετοχή με το ποσό των $6250. 

Όλοι οι οργανισμοί που συμμετέχουν στο OPW μπορούν να βρεθούν στην ιστοσελίδα <a href="https://wiki.gnome.org/OutreachProgramForWomen/2014/MayAugust#Participating_Organizations" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen/2014/MayAugust#Participating_Organizations</a> (καθημερινά θα προστίθενται νέοι).

Για περισσότερες πληροφορίες, δείτε:
  
<a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen</a>

Δείτε ένα βίντεο σχετικά με το Outreach Program for Women από τις διαχειρίστριες:

Μπορείτε να εκτυπώσετε την επίσημη αφίσα.
  
<figure id="attachment_2214" style="width: 765px" class="wp-caption aligncenter">[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/02/opw-poster-2014-May-August.png" alt="Outreach Program for Women 2014" class="size-full wp-image-2214 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/02/opw-poster-2014-May-August.png)<figcaption class="wp-caption-text">Outreach Program for Women 2014</figcaption></figure>
