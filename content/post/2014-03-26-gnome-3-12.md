---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-03-26 15:24:33
guid: http://gnome.gr/?p=2231
id: 2231
permalink: /2014/03/26/gnome-3-12/
tags:
  - gnome 3.12
title: Νέα έκδοση GNOME 3.12
url: /2014/03/26/gnome-3-12/
---

Με μεγάλη μας χαρά σας ανακοινώνουμε την τελευταία έκδοση GNOME 3.12 που εκδόθηκε στις 26 Μαρτίου 2014.

Τι νέο μας φέρνει η έκδοση αυτή;

– Μεγάλες αλλαγές στο GNOME web browser.
  
– 3 νέες εφαρμογές – το Logs, το Sound Recorder, το Polari – και νέο πρόγραμμα IRC για το GNOME.
  
– Οι φωτογραφίες έχουν προσαρμοστεί με το Facebook.
  
– Αναζήτηση στα τερματικά.
  
– Υποστήριξη για Google Cloud Print.
  
– Ανανεωμένος Βοηθός Αρχικής Ρύθμισης.
  
– Χρήση του νέου widget popovers στις βασικές εφαρμογές.

Διαβάστε το <a href="https://www.gnome.org/press/2014/03/gnome-3-12-released-with-new-features-for-users-and-developers/" target="1">δελτίο τύπου</a> με τι νέο μας φέρνει η έκδοση 3.12.