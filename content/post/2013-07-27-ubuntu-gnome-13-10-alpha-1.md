---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/4879804662648685642
blogger_permalink:
  - /2013/07/ubuntu-gnome-1310-alpha-1.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-07-27 11:30:00
guid: http://gnome.gr/2013/07/27/ubuntu-gnome-13-10-alpha-1/
id: 2918
permalink: /2013/07/27/ubuntu-gnome-13-10-alpha-1/
title: Ubuntu GNOME 13.10 Alpha 1
url: /2013/07/27/ubuntu-gnome-13-10-alpha-1/
---

Η ομάδα Ubuntu GNOME σας παρουσιάζει την έκδοση Ubuntu GNOME 13.10 (Saucy Salamander) Alpha 1.

Σε αυτή την έκδοση μπορείτε να δείτε τι έχουμε κάνει κατά την προετοιμασία για την επόμενη έκδοση που θα είναι έτοιμη τον Οκτώβριο.

Σημείωση: Αυτή η έκδοση Alpha, χρησιμοποιεί λογισμικό έκδοσης Alpha. Παρακαλώ δείτε τις <a href="https://wiki.ubuntu.com/SaucySalamander/Alpha1/UbuntuGNOME" target="1">σημειώσεις της έκδοσης</a> για περισσότερες πληροφορίες.

**Από που να το λάβετε**  
Την τελευταία έκδοση θα την λάβετε από:  
<a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/alpha-1/" target="1">http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/alpha-1/</a>

**Τι νέο υπάρχει**  
* Συμπεριλαμβάνεται το περισσότερο GNOME 3.8.  
* Συμπεριλαμβάνεται η κλασική συνεδρία GNOME.  
* Δεν συμπεριλαμβάνεται το Ubuntu Online Accounts.

Για περισσότερες πληροφορίες δείτε τις <a href="https://wiki.ubuntu.com/SaucySalamander/Alpha1/UbuntuGNOME" target="1">σημειώσεις της δοκιμαστικής έκδοσης</a>.