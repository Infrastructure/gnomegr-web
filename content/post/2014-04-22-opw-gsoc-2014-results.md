---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-04-22 11:00:39
guid: http://gnome.gr/?p=2526
id: 2526
permalink: /2014/04/22/opw-gsoc-2014-results/
tags:
  - Google Summer of Code
  - GSoC
  - OPW
  - Outreach Program for Women
title: Αποτελέσματα Outreach Program for Women και Google Summer of Code
url: /2014/04/22/opw-gsoc-2014-results/
---

Όπως θα έχετε παρατηρήσει στις τελευταίες αναρτήσεις, γίνεται μια εκτενής αναφορά στις αιτήσεις στο πρόγραμμα <a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">Outreach Program for Women</a> καθώς και το <a href="https://wiki.gnome.org/Outreach/SummerOfCode" target="_blank">Google Summer of Code</a>.

Στις 21 Απριλίου 2014, <a href="https://gnome.org/opw/" target="_blank">ανακοινώθηκαν τα ονόματα των συμμετεχόντων</a>.
  
Είμαστε στην ευχάριστη θέση να σας ανακοινώσουμε την συμμετοχή της <a href="https://wiki.gnome.org/OutreachProgramForWomen/2014/MayAugust" target="_blank">Μαρίας Μαυρίδου στο Outreach Program for Women</a>.

Συνολικά υπήρξαν 121 αιτήσεις, 44 από τις οποίες εγκρίθηκαν για το Outreach Program for Women και 5 στο Google Summer of Code. Οι υπεύθυνες του προγράμματος φρόντισαν ώστε να ελευθερώσουν θέσεις για το OPW, στέλοντας εργασίες στην συμμετοχή του GNOME στο GSoC. Από τις 121 αιτήσεις, 6 Ελληνίδες συμμετείχαν στο GNOME και 1 ακόμα σε άλλο οργανισμό. Από τις 6 Ελληνίδες του GNOME, οι 5 αιτήθηκαν στο έργο της μετάφρασης του GNOME.

Ευχαριστούμε τις Μαρία, Εύα, Γιώτα, Αφροδίτη για τις αιτήσεις τους και την συνεισφορά τους στη μετάφραση καθώς και την Χρυσαφή για την συνεισφορά της στην ομάδα των γραφικών. Η δουλειά σας δεν πάει χαμένη. Θα υπάρξει και επόμενος γύρος OPW στα τέλη του 2014, οπότε συνεχίστε την συνεισφορά σας και η αίτησή σας θα έχει καλύτερες πιθανότητες.

Όσον αφορά για την συμμετοχή μας, ας φροντίσουμε να βοηθήσουμε την Μαρία όσο μπορούμε. Εργασία της θα είναι να μεταφράσει αλλά και να διορθώσει τυχόν λάθη σε μεταφρασμένα αρχεία που φιλοξενούνται σε χώρο του GNOME, ώστε να ολοκληρωθούν στο 100%. Τον τελευταίο μήνα υπήρξε μεγάλη προσπάθεια από όλες τις αιτούσες, οπότε εφόσον τελειώσει η εργασία νωρίτερα, ελπίζουμε να ξεκινήσουμε και ορισμένες σελίδες του wiki.

Και πάλι συγχαρητήρια και καλή αρχή.