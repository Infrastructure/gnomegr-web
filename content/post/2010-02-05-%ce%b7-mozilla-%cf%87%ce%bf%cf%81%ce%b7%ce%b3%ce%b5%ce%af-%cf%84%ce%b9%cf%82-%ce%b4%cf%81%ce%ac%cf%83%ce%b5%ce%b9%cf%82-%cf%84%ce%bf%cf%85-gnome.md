---
author: iovarsamis
categories:
  - Ειδήσεις
date: 2010-02-05 13:57:01
excerpt: Εχθές, το GNOME Foundation έκανε γνωστή την νέα χρηματοδότηση από τη Mozilla Corporation. Έπειτα από παρόμοια κίνηση πριν δύο χρόνια, όπου είχαν παραχωρηθεί 10.000$ για τους τομείς της προσιτότητας που παρέχει το GNOME, η Mozilla συνεχίζει αυτήν την χορηγία και για την φετινή χρονιά στηρίζοντας τις προσπάθειες για προσιτές τεχνολογίες στο περιβάλλον εργασίας και στην εμπειρία του Web.
guid: http://gnome.gr/?p=363
id: 363
permalink: /2010/02/05/%ce%b7-mozilla-%cf%87%ce%bf%cf%81%ce%b7%ce%b3%ce%b5%ce%af-%cf%84%ce%b9%cf%82-%ce%b4%cf%81%ce%ac%cf%83%ce%b5%ce%b9%cf%82-%cf%84%ce%bf%cf%85-gnome/
tags:
  - accerciser
  - accessibility
  - dasher
  - gnome
  - mozilla
  - pdf
  - προσβασιμότητα
  - προσιτότητα
title: Η Mozilla χορηγεί τις δράσεις του GNOME για την προσιτότητα
url: /2010/02/05/%ce%b7-mozilla-%cf%87%ce%bf%cf%81%ce%b7%ce%b3%ce%b5%ce%af-%cf%84%ce%b9%cf%82-%ce%b4%cf%81%ce%ac%cf%83%ce%b5%ce%b9%cf%82-%cf%84%ce%bf%cf%85-gnome/
---

<p style="text-align: justify">
  Εχθές, το GNOME Foundation έκανε γνωστή την νέα χρηματοδότηση από τη Mozilla Corporation. Έπειτα από παρόμοια κίνηση πριν δύο χρόνια, όπου είχαν παραχωρηθεί $10.000 για τους τομείς της προσιτότητας (<em>accessibility</em>) που παρέχει το GNOME, η Mozilla συνεχίζει αυτήν την χορηγία και για την φετινή χρονιά στηρίζοντας τις προσπάθειες για προσιτές τεχνολογίες στο περιβάλλον εργασίας και στην εμπειρία του Web.
</p>

<p style="text-align: center">
  <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2010/02/gnome.png"><img class="size-medium wp-image-364 aligncenter img-responsive " title="gnome" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2010/02/gnome-300x142.png" alt="Gnome - Mozilla" /></a>
</p>

<p style="text-align: justify">
  Η χορηγία αφορά το 2010 και θα αξιοποιηθεί δραστηριοποιώντας προγραμματιστές του GNOME στο φετινό <a href="http://csunconference.org/index.cfm?EID=80000218&p=149" target="_blank">CSUN Conference</a> που θα λάβει χώρα στο San Diego, στις 22-27 Μαρτίου. Η CSUN αποτελεί μια από τις μεγαλύτερες τεχνολογικές διαλέξεις για τους ανθρώπους με αναπηρία και το GNOME έχει την ευκαιρία να διοργανώσει δράσεις που θα αλληλεπιδρούν με τους τελικούς χρήστες, κρατώντας τους προγραμματιστές κοντά στις εξελίξεις του τομέα αναπτύσσοντας τις γνώσεις για περαιτέρω αξιοποίηση.
</p>

<p style="text-align: justify">
  «Οι χορηγίες της Mozilla, μας βοήθησαν να προσθέσουμε νέες δυνατότητες προσιτότητας στο project <a href="http://live.gnome.org/Orca" target="_blank">Orca</a> καθώς και σε άλλες εφαρμογές προσιτότητας στο περιβάλλον GNOME», δήλωσε ο Willie Walker της ομάδας προσιτότητας του GNOME (GNOME Accessibility Team).
</p>

<p style="text-align: justify">
  To GNOME, έχει δουλέψει αρκετά ώστε να κάνει ευρέως προσιτή την εμπειρία του Ελεύθερου Λογισμικού στην επιφάνεια εργασίας. Με την ανάμειξη αυτή στην CSUN, οι προγραμματιστές του GNOME αναπτύσσουν τα γνωστικά αντικείμενα τους μαθαίνοντας τις τελευταίες εξελίξεις και προβλήματα που γίνονται γνωστά στον τομέα της προσιτότητας.
</p>

<p style="text-align: justify">
  Τόσο το GNOME όσο και η Mozilla, μαζί με τους εθελοντές που συμμετέχουν, συνεισφέρουν σε πολυάριθμα projects ώστε να κάνουν την εμπειρία χρήσης μιας ανοιχτής Επιφάνειας εργασίας (Desktop) και εξίσου ανοιχτού Web, προσβάσιμη από όλους τους χρήστες. Μεγάλο μέρος αυτών, αφορούν τις προσπάθειες για μια αλληλεπίδραση που στέκεται δίπλα σε χρήστες με ειδικές ανάγκες και καθιστούν την χρήση ενός Ελεύθερου περιβάλλοντος εργασίας ένα βήμα πιο κοντά τους.
</p>

## Οδηγός Προσιτότητας του GNOME

Υπάρχουν πολλά εργαλεία προσφερόμενα από το περιβάλλον του GNOME για τα οποία μπορείτε να ενημερωθείτε και στην ελληνική γλώσσα μέσα από τις ελληνικές μεταφράσεις της <a href="https://static.gnome.org/gnome-gr/" target="_blank">ελληνικής κοινότητας GNOME</a>.

Ο οδηγός προσιτότητας της επιφάνειας εργασίας του GNOME είναι διαθέσιμος (στα ελληνικά) στον σύνδεσμο : <http://library.gnome.org/users/gnome-access-guide/stable/index.html.el>

Ο ίδιος οδηγός, σε μορφή PDF, είναι διαθέσιμος από το [Οδηγός προσιτότητας GNOME](https://static.gnome.org/gnome-gr/uploads/sites/7/2009/05/gnome-access-guide.pdf).

Υπάρχουν επίσης και γνωστά εργαλεία, μεταξύ των οποίων,

  * το <a href="http://live.gnome.org/Mousetweaks/Home" target="_blank"><strong>Mousetweaks</strong></a>, που βοηθά στην προσιτότητα του ποντικιού, <http://library.gnome.org/users/mousetweaks/stable/mousetweaks.html.el>
  
    και η [Τεκμηρίωση Mousetweaks σε PDF
  
](https://static.gnome.org/gnome-gr/uploads/sites/7/2009/05/documentation_mousetweaks.pdf) 
  * το <a href="http://www.bltt.org/software/dasher/index.htm" target="_blank"><strong>Dasher</strong></a>, που αποτελεί ένα σύστημα διεπιφάνειας εισαγωγής κειμένου (η ελληνική τεκμηρίωση του δεν έχει ενημερωθεί ακόμα στο <a href="http://library.gnome.org/" target="_blank">http://library.gnome.org/</a> όμως αυτή παρέχεται κανονικά από το αρχείο PDF και το <a href="http://www.bltt.org/software/dasher/index.htm" target="_blank">Dasher</a> είναι πλήρως μεταφρασμένο)
  
    <http://library.gnome.org/users/dasher/stable/dasher.html.el> και η [Τεκμηρίωση Dasher σε PDF](https://static.gnome.org/gnome-gr/uploads/sites/7/2009/05/documentation_dasher-data.pdf)

Παρουσίαση του Dasher, μπορείτε να δείτε (στην αγγλική γλώσσα) στον σύνδεσμο : <a href="http://video.google.com/videoplay?docid=5078334075080674416#" target="_blank">http://video.google.com/videoplay?docid=5078334075080674416#</a>

Τέλος, υπάρχουν δύο ακόμα εργαλεία προσιτότητας στο περιβάλλον GNOME, το <a href="http://live.gnome.org/Accerciser" target="_blank"><em>Accerciser</em></a> και η _Παρακολούθηση προσιτότητας πληκτρολογίου._

To <a href="http://live.gnome.org/Accerciser" target="_blank"><strong>Accerciser</strong></a> είναι ένας διαδραστικός περιηγητής προσιτότητας για το περιβάλλον επιφάνειας εργασίας GNOME, γραμμένο σε Python και μπορείτε να δείτε το εγχειρίδιο του στον σύνδεσμο : <http://library.gnome.org/devel/accerciser/1.8/accerciser.html.el>

Η **Παρακολούθηση προσιτότητας πληκτρολογίου** (Keyboard Accessibility Monitor) εμφανίζει την κατάσταση των λειτουργιών προσιτότητας του πληκτρολογίου όταν αυτές είναι σε χρήση, εγχειρίδιο της οποίας υπάρχει στον σύνδεσμο : <http://library.gnome.org/users/accessx-status/2.28/accessx-status.html.el>

## Σύνδεσμοι

Gnome.org : <http://www.gnome.org/press/releases/2010-02-mozilla-accessibility.html>
