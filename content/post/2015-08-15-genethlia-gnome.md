---
author: iosifidis
categories:
  - Ειδήσεις
date: 2015-08-15 00:00:05
guid: https://static.gnome.org/gnome-gr/?p=3180
id: 3180
image: /wp-content/uploads/sites/7/2015/08/i-am-gnome.png
permalink: /2015/08/15/genethlia-gnome/
tags:
  - gnome
  - HappyBirthdayGnome
  - IAMGNOME
  - γενέθλια
title: Σήμερα το GNOME κλείνει τα 18!!!
url: /2015/08/15/genethlia-gnome/
---

Το GNOME γιορτάζει σήμερα, 15 Αυγούστου, τα γενέθλιά του. Το GNOME <a href="https://mail.gnome.org/archives/gtk-list/1997-August/msg00123.html" target="_blank">ιδρύθηκε</a> από δυο φοιτητές, τους Miguel de Icaza και Federico Mena Quintero. Ο στόχος τους ήταν να φτιάξουν ένα ελεύθερο περιβάλλον εργασίας που να είναι όμορφο και απλό στη χρήση. Τον Σεπτέμβριο θα καλωσορίσουμε την έκδοση GNOME 3.18.

<img src="https://www.gnome.org/wp-content/uploads/2015/08/federico_mena_guadec_2015.png" alt="Federico Mena Quintero στο GUADEC 2015" title="Federico Mena Quintero στο GUADEC 2015" class />

Είναι μεγάλη ευκαιρία να διαδώσετε γιατί αγαπάτε το GNOME, να εμπνεύσετε και άλλους να το δοκιμάσουν, να συνεισφέρουν στην κοινότητα του ή απλά να ευχηθείτε στο GNOME χαρούμενα γενέθλια!

Πως μπορείτε να συμμετέχετε στα γενέθλια του GNOME:

## Δημοσιεύσεις στα μέσα κοινωνικής δικτύωσης

  * Πείτε γιατί σας αρέσει το GNOME, πόσο καιρό το χρησιμοποιείτε ή για πόσο καιρό είστε κομμάτι της κοινότητας του.
  * Διαμοιράσετε το <a href="https://www.youtube.com/watch?v=WxRLa5hTGkg" target="_blank">βίντεο της τρέχουσας έκδοσης</a> GNOME ώστε να γνωρίζουν όλοι τι είναι το GNOME.
  * Δημοσιεύστε την εικόνα «I am GNOME» ή χρησιμοποιήστε την ως εικόνα προφίλ στα κοινωνικά μέσα δικτύωσης.
  * Θυμηθείτε να χρησιμοποιήστε τα hashtags **<a href="https://twitter.com/hashtag/HappyBirthdayGNOME" target="_blank">#HappyBirthdayGnome</a> <a href="https://twitter.com/hashtag/IAMGNOME" target="_blank">#IAMGNOME</a> <a href="https://twitter.com/hashtag/GNOME2015" target="_blank">#GNOME2015</a>** στις δημοσιεύσεις σας.

<img class="center img-responsive " src="https://www.gnome.org/wp-content/uploads/2015/08/i-am-gnome.png" alt="I am GNOME" />

## Ευχαριστήστε κάποιον που συνεισφέρει στο GNOME

Εάν γνωρίζετε κάποιον που συνεισφέρει στο GNOME, στείλτε του ένα μήνυμα να τον ευχαριστήσετε. Είναι ωραίο συναίσθημα όταν κάποιος σε ευχαριστεί για το έργο σου.

Επίσης μπορείτε να <a href="https://www.gnome.org/friends/" target="_blank">υιοθετήσετε έναν χάκερ ή να γίνετε φίλος του GNOME</a>.

## Δείξτε τον εξοπλισμό του GNOME που έχετε

Φορέστε ή δείξτε το αγαπημένο σας μπλουζάκι/αντικείμενο, ώστε να σας ρωτήσει κάποιος σχετικά με το GNOME. Δημοσιεύστε μια seflie φορώντας το αγαπημένο σας μπλουζάκι και χρησιμοποιήστε τα παραπάνω hashtags ώστε να δουν όλοι τα πρόσωπα της κοινότητας.

Χαιρετίστε κάποιον που φοράει μπλουζάκι και συναντηθείτε με άλλους χρήστες GNOME!

Ας δείξουμε στον κόσμο πόσο καταπληκτική κοινότητα είμαστε ώστε να συμμετέχουν και άλλοι.

**Χρόνια πολλά GNOME!**