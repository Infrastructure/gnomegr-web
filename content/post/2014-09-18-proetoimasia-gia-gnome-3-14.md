---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-09-18 17:39:43
guid: http://gnome.gr/?p=2605
id: 2605
image: /wp-content/uploads/sites/7/2014/04/translation_tools.png
permalink: /2014/09/18/proetoimasia-gia-gnome-3-14/
tags:
  - events
  - gnome
  - gnome 3.14
  - gnome shell
  - release party
  - μετάφραση
title: Προετοιμασία για το GNOME 3.14
url: /2014/09/18/proetoimasia-gia-gnome-3-14/
---

Λιγότερο από μια βδομάδα απομένει για να ανακοινωθεί το νέο <a href="https://wiki.gnome.org/ThreePointThirteen" title="Χρονοδιάγραμμα GNOME 3.14" target="_blank">GNOME 3.14</a>, όπου θα πάρουμε μια γεύση της νέας έκδοσης. Βρισκόμαστε σε <a href="https://mail.gnome.org/archives/devel-announce-list/2014-August/msg00003.html" title="Ανακοίνωση για το String freeze" target="_blank">string freeze</a> που σημαίνει πως έχουν οριστικοποιηθεί οι μεταφράσεις τόσο για το γραφικό περιβάλλον όσο και για την τεκμηρίωση.

Κατά τη διάρκεια του καλοκαιριού στα πλαίσια του [OPW](http://gnome.gr/2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/ "Νέα από τη συμμετοχή μας στο Outreach Program for Women"), η <a href="http://mavridou.blogspot.gr/" title="Ιστολόγιο της Μαρίας Μαυρίδου" target="_blank">Μαρία Μαυρίδου</a> είχε αναλάβει τη μετάφραση αλλά και τον έλεγχο μεγάλου μέρους του GNOME. Αυτό φαίνεται και από τα στατιστικά στη <a href="https://l10n.gnome.org/teams/el/" title="Ιστόποτος συντονισμού της μετάφρασης" target="_blank">σελίδα μεταφράσεων</a>, όπου αυτή τη στιγμή βρισκόμαστε στο 99% για το γραφικό περιβάλλον και στο 96% στην τεκμηρίωση, με αρκετές από τις μεταφράσεις να έχουν μείνει για αναθεώρηση πριν καταχωρηθούν στο σύστημα.

### Πώς μπορείτε να βοηθήσετε;

Μπορείτε να μεταφράσετε τα μηνύματα που έχουν απομείνει στο γραφικό περιβάλλον ή στην τεκμηρίωση για να φτάσουμε στο 100%.

Μπορείτε επίσης να δοκιμάσετε τη νέα έκδοση του GNOME χρησιμοποιώντας μια <a href="http://build.gnome.org/continuous/buildmaster/images/z/current/" title="Εικόνες για εικονική μηχανή" target="_blank">εικονική μηχανή</a> για μας βοηθήσετε στην εύρεση σφαλμάτων στα μηνύματα της γραφικής διεπαφής ή της τεκμηρίωσης. Οι χρήστες Fedora και Archlinux μπορούν να το δοκιμάσουν μέσω των αποθετηρίων των διανομών τους.

Να διοργανώσετε ένα release party ή ένα <a href="https://wiki.gnome.org/Events/GBeers" title="Μπύρες, πολλές μπύρες!" target="_blank">GBeers</a> στην πόλη σας για τη νέα έκδοση που πρόκειται να ανακοινωθεί.

Πείτε μας και τις δικές σας ιδέες!!

### Συμμετοχή

Αν είστε νέο μέλος και θέλετε να βοηθήσετε στο έργο μετάφρασης της ελληνικής κοινότητας του GNOME, συμβουλευτείτε τη σελίδα [«Συμμετοχή»](http://gnome.gr/contribute/ "Σελίδα Συμμετοχή") ή και το wiki της κοινότητας στο <a href="http://wiki.gnome.gr" title="Wiki της ελληνικής κοινότητας GNOME" target="_blank">http://wiki.gnome.gr</a>. 

Για οποιαδήποτε απορία μη διστάσετε να επικοινωνήσετε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" title="Λίστα αλληλογραφίας gnomegr" target="_blank">λίστα αλληλογραφίας</a>. Θα χαρούμε να σας βοηθήσουμε σε οτιδήποτε χρειαστείτε. Θα μας βρείτε επίσης και στα [social media](http://gnome.gr/#social_media "Social media gnome gr")!