---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-09-18 10:00:38
guid: http://gnome.gr/?p=2610
id: 2610
permalink: /2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/
tags:
  - gnome
  - GNOME Women
  - OPW
  - Outreach Program for Women
  - Women
title: Νέα από τη συμμετοχή μας στο Outreach Program for Women
url: /2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/
---

<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/gnome-women-outreach-logo-no-background-e1410961068554.png" alt="GNOME Women"  class="img-responsive" />

Πριν ένα μήνα περίπου ολοκληρώθηκε το πρόγραμμα <a href="http://gnome.gr/2014/04/22/opw-gsoc-2014-results/" title="Αποτελέσματα OPW" target="_blank">Outreach Program for Women</a> μαζί και η πρώτη συμμετοχή μας στο πρόγραμμα με την <a href="http://mavridou.blogspot.gr/" title="Ιστολόγιο της Μαρίας Μαυρίδου" target="_blank">Μαρία Μαυρίδου</a>.

Η Μαρία έχει κάνει εξαιρετική δουλειά τόσο στον τομέα της μετάφρασης όσο και στον έλεγχο. Στα πλαίσια του προγράμματος δημιουργήσαμε και το <a href="http://gnome.gr/glossary/" title="Γλωσσάρι ελληνικής κοινότητας GNOME" target="_blank">γλωσσάρι</a> της ελληνικής κοινότητας του GNOME, το οποίο χρησιμοποιήθηκε καθ” όλη τη διάρκεια του OPW και συνεχίζει μιας και καθιερώθηκε από την ομάδα. Βασικός λόγος για τη δημιουργία του, είναι η ομοιογένεια που θέλουμε να κρατήσουμε μεταξύ των μεταφράσεων. 

Η Μαρία συνεχίζει πλέον το έργο της στην ελληνική κοινότητα του GNOME με τον ρόλο του <a href="https://l10n.gnome.org/users/MarMav/" target="_blank">ελεγκτή</a> και την ευχαριστούμε θερμά για αυτό.

### Συμμετοχή

Η ελληνική κοινότητα του GNOME θα προσπαθήσει να συμμετάσχει και στον επόμενο γύρο του Outreach Program for Women.

Δείτε επίσης τη σελίδα [«Συμμετοχή»](http://gnome.gr/contribute/) και το <a href="http://wiki.gnome.gr/" target="_blank">wiki</a> της ελληνικής κοινότητας για το πως μπορείτε να συνεισφέρετε στο έργο μας. Αν έχετε απορίες σχετικά με το πρόγραμμα OPW ή τη διαδικασία μετάφρασης, επικοινωνήστε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" title="Λίστα αλληλογραφίας της κοινότητας" target="_blank">λίστα αλληλογραφίας</a>.