---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2013-11-26 14:50:11
guid: http://gnome.gr/?p=2205
id: 2205
image: /wp-content/uploads/sites/7/2013/10/opw-poster-2013-December-March.png
permalink: /2013/11/26/results-outreach-program-for-women-2013/
tags:
  - gnome
  - OPW
  - Outreach Program for Women
title: Αποτελέσματα του Outreach Program for Women 2013
url: /2013/11/26/results-outreach-program-for-women-2013/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-poster-2013-December-March.png" alt="Αφίσα Outreach Program for Women" class="size-full wp-image-2161 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-poster-2013-December-March.png) 

Όσοι παρακολουθείτε τα γεγονότα στην κοινότητα GNOME, έγινε μια προσπάθεια στη χώρα μας να γίνει γνωστό το πρόγραμμα <a href="http://gnome.gr/2013/10/07/outreach-program-for-women/" target="_blank">Outreach Program for Women 2013</a>. Αν και το πρόγραμμα Δεκεμβρίου-Μαρτίου αφορά κυρίως το Νότιο ημισφαίριο, είχαμε και Ελληνική αίτηση. Συγκεκριμένα, υπήρχαν 23 αιτήσεις, 18 από τις οποίες συμπλήρωσαν την απαιτούμενη συνεισφορά. Οι αιτήσεις αναλυτικά ήταν:

1) Rashi Aswani (Rashi) – Cheese – student, Hyderabad, India
  
mentor: David King
  
2) Pratibha Chhimpa (flame) – GCompris – repeat applicant – student, Jaipur, India
  
mentor: Bruno Coudoin
  
3) Clara Lemos e Fialho (clarafialho) – Design, Juiz de Fora, Brazil
  
mentor: Allan Day
  
4) Sakshi Jain (sakshi) – Damned Lies – student, Ajmer, India
  
mentor: Gil Forcada
  
5) Sphinx Jiang – Chinese translation – masters student, done with courses, Beijing, China
  
mentors: Wylmer Wang, YunQiang Su, Eleanor Chen
  
6) Kriti Kansal (kan4) – Anjuta – student, Hyderabad, India
  
mentors: Johannes Schmid and Sébastien Granjoux
  
7) Emily Karungi (karungi) – Music – student, Kampala, Uganda
  
mentor: Vadim Rutkovsky
  
8) Veena Katiyar (div1) – Empathy and GNOME Boxes, Lucknow, India
  
mentor: Chandni Verma
  
9) Nikoleta Papanastasiou (NikoletaPapanastasiou) – Translation – student, Thessaloniki, Greece
  
mentor: Stathis Iosifidis
  
10) Spandana Raj Babbula (spandana) – Evince – student, Chennai, India
  
mentor: Germán Póo-Caamaño
  
11) Arpita Raj Gupta (arpita_) – Web Development – student, Hyderabad, India
  
mentor: Saumya Pathak
  
12) Eliane Ramos Pereira (elianeramos) – Brno, Czech Republic
  
mentor: Parin Porecha
  
13) Paula Rodrigues Furtado (paulets) – masters student writing her thesis, Campinas, Brazil
  
mentor: Ekaterina Gerasimova
  
14) Mayya Sharipova (mayya) – GCompris – PhD student doing testing of the system she developed in classrooms, Oakville, Ontario, Canada
  
mentor: Bruno Coudoin
  
15) Aparajita Sahoo (aparajita) – Design – student, Mumbai, India
  
mentor: Allan Day
  
16) Himangi Saraogi (himangi) – Web Development or Engagement – student, Hyderabad, India
  
mentors: Saumya Pathak, Andreas Nilsson, Fabiana Simões for Web Development; Allan Day, Sriram Ramkrishna for Engagement
  
17) Priyanka Suresh (Priyanka) – Web Development – student, Hyderabad, India
  
mentor: Saumya Pathak
  
18) Shobha Tyagi (curiousDTU) – Documentation – 4th time repeat applicant, Muzaffarnagar, India
  
mentor: Ekaterina Gerasimova

Η αποδοχή των συμμετοχών έγινε βάση βαθμολογίας που λάβανε οι αιτήσεις ανάλογα με την συνεισφορά τους. Όπως παρατηρούμε υπήρχαν και γυναίκες που έκαναν πολλαπλές φορές αίτηση για το πρόγραμμα.

Χθες λοιπόν βγήκαν τα αποτελέσματα στην ιστοσελίδα του <a href="https://wiki.gnome.org/OutreachProgramForWomen/2013/DecemberMarch#Accepted_Participants" target="_blank">Outreach Program for Women 2013</a>. Όσον αφορά το GNOME, είναι οι:

**GNOME**
  
coordinator: Marina Zhurakhinskaya

1. _Shobha Tyagi (curiousDTU)_, Muzaffarnagar, Uttar Pradesh, India – Documentation – Ekaterina Gerasimova
  
2. _Sphinx Jiang_, Beijing, China – Chinese translation – Wylmer Wang and YunQiang Su
  
3. _Eliane Ramos Pereira (elianeramos)_, Brno, Czech Republic – Getting Things GNOME! – Parin Porecha 

Όσες γυναίκες δεν πέρασαν, ενθαρρύνονται να συνεχίσουν να συνεισφέρουν ώστε να κάνουν αίτηση στον επόμενο γύρο, ώστε να είναι πιο πιθανό να γίνουν δεκτές. Επίσης υπάρχει και η περίπτωση του Google Summer of Code – <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2014" target="_blank">http://www.google-melange.com/gsoc/homepage/google/gsoc2014</a>.
