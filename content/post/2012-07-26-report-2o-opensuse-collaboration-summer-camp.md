---
author: iosifidis
categories:
  - Ειδήσεις
date: 2012-07-26 23:42:59
guid: http://gnome.gr/?p=1670
id: 1670
image: /wp-content/uploads/sites/7/2012/07/family.jpg
permalink: /2012/07/26/report-2o-opensuse-collaboration-summer-camp/
tags:
  - collaboration
  - openSUSE
  - summer camp
title: Εντυπώσεις από το 2o openSUSE collaboration summer camp
url: /2012/07/26/report-2o-opensuse-collaboration-summer-camp/
---

<a href="http://gnome.gr/2012/07/26/report-2o-opensuse-collaboration-summer-camp/family/" rel="attachment wp-att-1671"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/07/family.jpg" alt="" title="2o openSUSE collaboration summer camp" class="aligncenter size-full wp-image-1671 img-responsive " /></a>

Στις 20-22 Ιουλίου πραγματοποιήθηκε με επιτυχία το 2o openSUSE Collaboration Summer Camp στην Κατερίνη. Πολλές συμμετοχές από διαφορετικές κοινότητες, ενδιαφέρουσες ομιλίες και workshops πάνω σε αγαπημένα projects. Αυτό που μένει αξέχαστο σε όλους ήταν οι συζητήσεις εκτός προγράμματος, η χαλάρωση και τα παιχνίδια στο νερό.

Η διαφορά με την περσινή διοργάνωση ήταν αφενός οι περισσότερες συμμετοχές κοινοτήτων ΕΛ/ΛΑΚ και αφετέρου οι ομιλίες που ήταν στα Αγγλικά, λόγω της συμμετοχής από τα γραφεία της SUSE της Πράγας.

Παρουσιάστηκαν τεχνολογίες όπως το Puppet από τον Θοδωρή Χατζημίχο, το ARM από τον Michal Hrušecký, τα νέα χαρακτηριστικά του KWin και το Plasmate από τους Αντώνη και Γιώργο Τσιαπαλιώκα, τη χρήση του LVM από τον Ευστάθιο Αγραπίδη, τα Mozilla web applications από τον Χρίστο Μπαχαράκη, <a href="https://docs.google.com/presentation/pub?id=1oq7wR-29hDTDGFuLjsGeFD-gk0wgI882L00SGtWR1oI&start=false&loop=false&delayms=3000" target="_blank">το owncloud από τον Στάθη Ιωσηφίδη</a>, το Eclipse, το openSUSE Connect και το Trello (μία πλατφόρμα οργάνωσης εργασιών για κάποιο project) από τον Αθανάσιο – Ηλία Ρουσινόπουλο, την οργάνωση του Συστήματος αρχείων στο Linux από τον Κώστα Κουδάρα, τη τεχνολογία VServers από τον Δημήτρη Παπαπούλιο και τη σωστή χρήση του LibreOffice για την παραγωγή κειμένων από τον Σίμο Ξενιτέλη.

Επίσης υπήρχε παρουσίαση για την <a href="https://docs.google.com/presentation/pub?id=16xmy5VvLy9DDFDxP0yAd2T-fRQ7IhaplOak9t0Hv5Fk&start=false&loop=false&delayms=3000" target="_blank">σωστή συμπεριφορά στα booths από τον Στάθη Ιωσηφίδη</a>, μία συζήτηση γύρω από τις κοινότητες με συντονιστή τον Σίμο Ξενιτέλη και τέλος παρουσιάστηκαν και μερικοί λόγοι για να επισκεφθείτε την Πράγα και το openSUSE Conference από τον Michal Hrušecký.

Από πλευράς κοινότητας Gnome, εξηγήθηκε το σύστημα μεταφράσεων που χρησιμοποιεί η κοινότητα και δόθηκαν ορισμένα αρχεία προς μετάφραση. Λόγω χρόνου δεν ολοκληρώθηκαν. Θα συνεχιστούν όμως και θα υποβληθούν όταν ολοκληρωθούν.

Η διοργάνωση κρίνεται επιτυχημένη. Ανυπομονούμε για την επόμενο openSUSE Collaboration Summercamp.

ΔΕΙΤΕ 3 ΣΕΤ ΦΩΤΟΓΡΑΦΙΩΝ:
  
1. <a href="https://picasaweb.google.com/105770476640273513358/OpenSUSECollaborationCamp2012" target="_blank">Σετ Μπαχαράκη</a>
  
2. <a href="https://picasaweb.google.com/104738679296987729958/OpenSUSECollaborationSummerCamp2012?authuser=0&feat=directlink" target="_blank">Σετ Κουδάρα</a>
  
3. <a href="https://plus.google.com/photos/113898425601074911439/albums/5768743233887852865" target="_blank">Σετ Michal Hrušecký</a>