---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2017-02-15 16:51:39
guid: https://www.gnome.gr/?p=3613
id: 3613
image: /wp-content/uploads/sites/7/2017/02/gnomegr_l10n_banner.png
permalink: /2017/02/15/metafrasi-tou-gnome-3-24/
tags:
  - gnome
  - ΕΛ/ΛΑΚ
  - μεταφράσεις
  - μετάφραση
title: Έναρξη μετάφρασης του GNOME 3.24
url: /2017/02/15/metafrasi-tou-gnome-3-24/
---

<img class="size-full wp-image-3629 aligncenter img-responsive " src="https://static.gnome.org/gnome-gr/uploads/sites/7/2017/02/gnomegr_l10n_banner.png" alt="gnomegr l10n banner" />

Στις 24 Μαρτίου θα γίνει διαθέσιμο το νέο **GNOME 3.24**, και έφτασε η ώρα για να ξεκινήσουμε τη μετάφραση της νέας έκδοσης.

Στη σελίδα <a href="https://l10n.gnome.org/teams/el/" target="_blank">https://l10n.gnome.org/teams/el/</a>, μεταφράζουμε την έκδοση **«GNOME 3.24 (σε ανάπτυξη)»** και για αρχή θα επικεντρωθούμε στα μηνύματα της **«Διεπαφής χρήστη»** και στη συνέχεια στην **«Τεκμηρίωση»**.

Πληροφοριακά, τα ποσοστά μετάφρασης είναι τα ακόλουθα:

  * Διεπαφή χρήστη: **93%**, με **762 αμετάφραστα** και **2325 ασαφή μηνύματα**,
  * Τεκμηρίωση: **95%**, με **740 αμετάφραστα** και **396 ασαφή** μηνύματα

Για καλύτερες μεταφράσεις, χρησιμοποιούμε τα εξής εργαλεία και υπηρεσίες της ελληνικής κοινότητας GNOME:

  * Το γλωσσάρι της κοινότητας <https://www.gnome.gr/glossary/>
  * Την μεταφραστική μνήμη: <a href="http://memory.gnome.gr/" target="_blank">http://memory.gnome.gr/</a>
  * Συμβουλές μετάφρασης: <a href="https://wiki.gnome.org/el/Translation/Tips" target="_blank">https://wiki.gnome.org/el/Translation/Tips</a>

### Είμαι νέος στην κοινότητα, πως μπορώ να βοηθήσω στη μετάφραση του έργου;

Η ελληνική κοινότητα του GNOME, δέχεται με χαρά οποιαδήποτε βοήθεια στο έργο της. Αν θέλετε να βοηθήσετε με τη μετάφραση του GNOME και δεν γνωρίζετε πως, σας προσκαλούμε να επικοινωνήσετε μαζί μας στη <a href="https://mail.gnome.org/mailman/listinfo/gnome-el-list" target="_blank">λίστα αλληλογραφίας</a> ή στα [μέσα κοινωνικής δικτύωσης](https://www.gnome.gr/#social_media) για νας καθοδηγήσουμε.

Στη σελίδα [«Συμμετοχή»](https://www.gnome.gr/contribute/) θα βρείτε οδηγίες για το πως να εγγραφείτε στην πλατφόρμα μεταφράσεων του GNOME, ποια είναι τα βήματα που ακολουθούμε σε κάθε μετάφραση καθώς επίσης και χρήσιμες πληροφορίες σχετικά με τα εργαλεία που χρησιμοποιούμε στην ελληνική κοινότητα του GNOME.

**Ας ξεκινήσουμε λοιπόν τις μεταφράσεις!**
