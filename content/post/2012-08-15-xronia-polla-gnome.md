---
author: iosifidis
categories:
  - Ειδήσεις
date: 2012-08-15 23:49:12
guid: http://gnome.gr/?p=1679
id: 1679
permalink: /2012/08/15/xronia-polla-gnome/
tags:
  - gnome
title: Χρόνια Πολλά Gnome!!!
url: /2012/08/15/xronia-polla-gnome/
---

Πριν 15 χρόνια (15 Αυγούστου 1997), οι <a href="http://en.wikipedia.org/wiki/Miguel_de_Icaza" target="_blank">Miguel de Icaza</a> και <a href="http://en.wikipedia.org/wiki/Federico_Mena" target="_blank">Federico Mena Quintero</a> αποφάσισαν να κατασκευάσουν ένα γραφικό περιβάλλον (μαζί με τις εφαρμογές του), που θα μοιάζει με το CDE και το KDE αλλά θα είναι βασισμένο στο ελεύθερο λογισμικό. Έτσι φτιάχτηκε το Gnome. Από τότε απέκτησε πολλούς χρήστες και πολλά άτομα το λάτρεψαν.

Δείτε την ιστορία του Gnome στην ιστοσελίδα <a href="http://www.happybirthdaygnome.org/" target="_blank">http://www.happybirthdaygnome.org/</a>

Χρόνια πολλά λοιπόν Gnome.<figure id="attachment_1680" style="width: 219px" class="wp-caption aligncenter">

<a href="http://gnome.gr/2012/08/15/xronia-polla-gnome/miguel_de_icaza/" rel="attachment wp-att-1680"><img class="size-medium wp-image-1680 img-responsive " title="Miguel de Icaza" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/08/Miguel_de_Icaza-219x300.jpg" alt="" /></a><figcaption class="wp-caption-text">Miguel de Icaza</figcaption></figure> <figure id="attachment_1681" style="width: 225px" class="wp-caption aligncenter"><a href="http://gnome.gr/2012/08/15/xronia-polla-gnome/federico_mena_quintero/" rel="attachment wp-att-1681"><img class="size-medium wp-image-1681 img-responsive " title="Federico Mena Quintero" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/08/Federico_Mena_Quintero-225x300.jpg" alt="" /></a><figcaption class="wp-caption-text">Federico Mena Quintero</figcaption></figure>
