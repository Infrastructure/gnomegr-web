---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-10-07 10:56:38
guid: http://gnome.gr/?p=2744
id: 2744
image: /wp-content/uploads/sites/7/2014/09/opw-dec-mar-14-15.png
permalink: /2014/10/07/neos-kuklos-outreach-program-for-women-dekemvrios-martios/
tags:
  - gnome
  - GNOME Women
  - Google Summer of Code
  - OPW
  - Outreach Program for Women
  - Women
title: Νέος κύκλος Outreach Program for Women (Δεκέμβριος – Μάρτιος)
url: /2014/10/07/neos-kuklos-outreach-program-for-women-dekemvrios-martios/
---

<figure id="attachment_2745" style="width: 249px" class="wp-caption aligncenter">[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/opw-dec-mar-14-15-249x300.png" alt="Αφίσα διοργάνωσης Δεκ-Μαρ" class="size-medium wp-image-2745 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/opw-dec-mar-14-15.png)<figcaption class="wp-caption-text">Αφίσα διοργάνωσης Δεκ-Μαρ</figcaption></figure> 

Θα έχετε δει ότι σαν Ελληνική Κοινότητα συμμετείχαμε στον <a href="http://gnome.gr/2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/" title="Συμμετοχή Ελληνικής Κοινότητας στο Outreach Program for Women" target="_blank">προηγούμενο κύκλο του Outreach Program for Women</a>.

**Τι είναι το Outreach Program for Women;**

Είναι ένα πρόγραμμα όπου γυναίκες συμμετέχουν ως ειδικευόμενες στον χώρο του Ελεύθερου Λογισμικού, φέρνοντας εις πέρας κάποιες εργασίες που έχουν θέσει οργανισμοί, οι οποίοι πληρώνουν τις ειδικευόμενες **$5500** (στην αρχή δίνονται **$500** και τα υπόλοιπα **$2500** κάπου στο μέσον του προγράμματος και τα λοιπά στο τέλος). Επιπλέον δίνονται **$500** για να ταξιδέψει η ειδικευόμενη σε εκδηλώσεις, συνέδρια. Το GNOME απλά διοργανώνει το όλο εγχείρημα, δεν το χρηματοδοτεί (εκτός των συμμετοχών της).

Συνήθως διοργανώνεται δυο φορές το χρόνο και αφορά το Βόρειο ημισφαίριο όταν έχουμε καλοκαίρι και το Νότιο ημισφαίριο όταν έχουμε χειμώνα. Βέβαια αυτός ο κανονισμός δεν είναι τόσο αυστηρός. Αυστηρός είναι ο κανονισμός που δεν μπορεί να συμμετάσχει κάποια κοπέλα για 2η φορά, ούτε μπορεί να συμμετάσχει εάν έχει πάρει μέρος στο Google Summer of Code.

Για τα ψιλά γράμματα, ποιος μπορεί να συμμετάσχει, πως είναι η διαδικασία, διαβάστε καλύτερα στην ιστοσελίδα <a href="https://wiki.gnome.org/OutreachProgramForWomen" title="Outreach Program for Women" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen</a>.

Μην ξεχνάτε τις ευκαιρίες εκτός Outreach Program for Women και Google Summer of Code που μπορείτε να βρείτε στην ιστοσελίδα <a href="https://wiki.gnome.org/OutreachProgramForWomen/Opportunities" title="Ευκαιρίες απασχόλησης" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen/Opportunities</a>.

Για να δούμε για τον γύρο που θα ξεκινήσει τον Δεκέμβριο.
  
Αρχικά πρέπει να διαβάσει προσεκτικά την σελίδα <a href="https://wiki.gnome.org/OutreachProgramForWomen/2014/DecemberMarch" title="Δεκέμβριος 2014 - Μάρτιος 2015" target="_blank">αυτού του γύρου</a>.

Στις 12 Σεπτεμβρίου ανακοινώθηκαν οι οργανισμοί που θα συμμετάσχουν με εργασίες. Από τις 12 Σεπτεμβρίου έως τις 22 Οκτωβρίου πρέπει να βρείτε ποιο project σας αρέσει, να βρείτε κάποιον που θα σας καθοδηγήσει (mentor) και να κάνετε συνεισφορά (ώστε να δείξετε το ενδιαφέρον σας). Από τις 12 Σεπτεμβρίου έχει ανοίξει και το σύστημα ώστε να δηλώσετε συμμετοχή. Ημερομηνία λήξεις αιτήσεων είναι η 22 Οκτωβρίου (7pm UTC). Θα τα βρείτε όλα στην διεύθυνση <a href="http://gnome.org/opw/" title="Outreach Program for Women" target="_blank">http://gnome.org/opw/</a>. Τα αποτελέσματα θα βγουν στις 12 Νοεμβρίου (7pm UTC). Η περίοδος του προγράμματος είναι 9 Δεκεμβρίου 2014 έως 9 Μαρτίου 2014.

Μερικά σχόλια στα παραπάνω.
  
1. Την περίοδο από την λήξη (22 Οκτωβρίου) έως την απόφαση ποιές θα συμμετάσχουν (12 Νοεμβρίου), οι περισσότερες κοπελιές σταματούν την συνεισφορά. Όμως εάν συνεχίσετε, θα έχετε περισσότερες πιθανότητες να συμμετέχετε. Ερωτάται ο mentor σας σχετικά με την συμμετοχή σας.
  
2. Όσον αφορά το GNOME, δύσκολο να συμμετέχουμε σαν Ελληνική Κοινότητα. Από όσο γνωρίζουμε, τα projects που θα συμμετέχουν θα είναι αυτά που έχουν σχέση με το triage και fix των σφαλμάτων που υπάρχουν. Γι’αυτό θα αποφασίσει το board. Σας συμβουλεύουμε να ψάξετε τους άλλους οργανισμούς για παρόμοιες εργασίες. Μπορείτε να βρείτε σε άλλους οργανισμούς από μετάφραση έως και συγγραφή τεκμηρίωσης. Εάν δεν βρείτε, μπείτε στα κανάλια επικοινωνίας που παρέχουν και ζητήστε από μόνες σας. Αναφέρετε τι γνωρίζετε και με τι θα θέλατε να ασχοληθείτε, ώστε να βεθεί κάποιος mentor να σας αναλάβει (ίσως σε project που δεν αναγράφεται).
