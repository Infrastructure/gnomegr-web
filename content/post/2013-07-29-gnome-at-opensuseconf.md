---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
date: 2013-07-29 14:29:54
guid: http://gnome.gr/?p=2074
id: 2074
image: /wp-content/uploads/sites/7/2013/07/osc13-logo.png
permalink: /2013/07/29/gnome-at-opensuseconf/
tags:
  - gnome
  - openSUSE
  - openSUSE conference
title: Παρουσία κοινότητας GNOME στο παγκόσμιο συνέδριο openSUSE 2013
url: /2013/07/29/gnome-at-opensuseconf/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/osc13-logo-300x212.png" alt="openSUSE conf 13 logo" class="aligncenter size-medium wp-image-2075 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/osc13-logo.png)

Στο <a href="http://conference.opensuse.org/" target="_blank">παγκόσμιο συνέδριο της κοινότητας openSUSE</a> συγκεντρώνονται μέλη, χρήστες και φίλοι του ελεύθερου λογισμικού, λογισμικού ανοικτού κώδικα με σκοπό να παρουσιάσουν τα νέα της διανομής, να παρουσιάσουν τα project που δουλεύουν αλλά κυρίως να περάσουν καλά. Η <a href="http://opensuse.gr/" target="_blank">Ελληνική κοινότητα openSUSE</a> από τότε που ξεκίνησε, έδειξε την δράση της τόσο τοπικά όσο και παγκόσμια. Έτσι, μετά από ελάχιστα χρόνια, η παγκόσμια κοινότητα τους εμπιστεύθηκε την διοργάνωση του παγκόσμιου συνεδρίου στην Θεσσαλονίκη (απ” όπου κατάγονται τα περισσότερα μέλη της). Το συνέδριο διεξήχθη σε ένα όμορφο χώρο, το Ολυμπιακό Μουσείο Θεσσαλονίκης, από 18 έως 22 Ιουλίου.

Ως κοινότητα GNOME θα θέλαμε να τους συγχαρούμε τόσο για την επιτυχία τους αλλά και για την άψογη διοργάνωση του συνεδρίου.

Στο συνέδριο συμμετείχαμε ως παγκόσμια κοινότητα GNOME με δικό μας booth. Θα θέλαμε να ευχαριστήσουμε τους <a href="https://dl.dropboxusercontent.com/u/20413076/opensuse/conference/report/gnome-motion.gif" target="_blank">Inene Chen, Ευστάθιο Ιωσηφίδη, Chunhung Huang και Izabel Valverde</a> για την λειτουργία του booth κατά τη διάρκεια του συνεδρίου.

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/gnome_team-300x200.jpg" alt="Inene Chen, Stathis Iosifidis, Chunhung Huang, Izabel Valverde" class="aligncenter size-medium wp-image-2077 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/gnome_team.jpg)

Ραντεβού στο συνέδριο <a href="https://www.guadec.org/" target="_blank">GUADEC</a>.
