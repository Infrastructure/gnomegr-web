---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
  - Εκδηλώσεις
date: 2013-10-07 11:00:26
guid: http://gnome.gr/?p=2141
id: 2141
permalink: /2013/10/07/gnome-women-no1/
tags:
  - gnome
  - GNOME Women
  - Women
title: 1η συνάντηση GNOME Women στη Θεσσαλονίκη
url: /2013/10/07/gnome-women-no1/
---

Είδαμε σε προηγούμενο άρθρο τι είναι η <a href="http://gnome.gr/2013/10/07/gnome-women/" target="_blank">ομάδα GNOME Women</a> και ποια είναι η ανάγκη να υπάρχει.

Την Κυριακή 6 Οκτωβρίου πραγματοποιήθηκε στη Θεσσαλονίκη η πρώτη συνάντηση της ομάδας των γυναικών.

Αρχικά έγινε η γνωριμία μεταξύ τους. Αναλύθηκε τι είναι η GNOME Women, ο λόγος ύπαρξης και που μπορεί να συνεισφέρει κάθε μια. Επίσης αναλύθηκε το <a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">Outreach Program for Women</a> και πως μπορεί κάποια να συμμετάσχει.<figure id="attachment_2142" style="width: 300px" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2142 img-responsive " alt="GNOME Women" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/gnome-women-1-300x202.png" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/gnome-women-1.png)<figcaption class="wp-caption-text">GNOME Women Θεσσαλονίκη 6/10/2013</figcaption></figure> 

Όσες χάσατε την 1η συνάντηση, θα ανακοινωθούν σύντομα και επόμενες δράσεις της ομάδας στην λίστα αλλά και σε μέσα κοινωνικής δικτύωσης του GNOME.

Σε περίπτωση που δεν είστε κοντά στη Θεσσαλονίκη, μπορείτε να γράψετε στην Ελληνική λίστα ώστε να μαζευτεί μια ομάδα στην πόλη-περιοχή σας και να συνεργαστείτε.
