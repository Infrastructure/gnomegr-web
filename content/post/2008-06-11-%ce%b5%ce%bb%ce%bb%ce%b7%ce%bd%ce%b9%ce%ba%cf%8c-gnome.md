---
author: nikosx
categories:
  - Ειδήσεις
date: 2008-06-11 14:36:55
guid: http://gnome.gr/gnome2gr/?p=19
id: 19
permalink: /2008/06/11/%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%cf%8c-gnome/
title: Ελληνικό Gnome
url: /2008/06/11/%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%cf%8c-gnome/
---

<span style="font-size: small">Η Ελληνική κοινότητα GNOME: Από το 1999 προγραμματιστές (και όχι μόνο) έχουν ξεκινήσει την προσαρμογή του GNOME στην ελληνική γλώσσα και την προώθηση του στον ελλαδικό χώρο. Αποτέλεσμα αυτής της δουλειάς είναι όλες οι εκδόσεις του GNOME μέχρι τώρα να έχουν πλήρη υποστήριξη της Ελληνική γλώσσας.</span>

##  <span style="font-size: small">Η ελληνική κοινότητα του GNOME είναι πάντα ανοικτή σε νέες συμμετοχές! </span>