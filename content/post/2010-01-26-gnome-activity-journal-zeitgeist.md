---
author: iovarsamis
categories:
  - Παρουσίαση
date: 2010-01-26 19:24:46
excerpt: 'Παρουσίαση εφαρμογής Gnome : GNOME Activity Journal - Zeitgeist.

  Το Zeitgeist, είναι μια υπηρεσία που καταγράφει τις χρήσεις αρχείων του χρήστη

  ομαδοποιώντας τα ανάλογα με την συνάφεια και τις μεταξύ τους σχέσεις.

  '
guid: http://gnome.gr/?p=352
id: 352
permalink: /2010/01/26/gnome-activity-journal-zeitgeist/
tags:
  - gnome
  - gnome activity journal
  - zeitgeist
title: Παρουσίαση εφαρμογής «GNOME Activity Journal / Zeitgeist»
url: /2010/01/26/gnome-activity-journal-zeitgeist/
---

<div id="_mcePaste" style="text-align: justify">
  Το <strong><a title="Zeitgeist" href="http://zeitgeist-project.com/">Zeitgeist</a></strong>, είναι μια υπηρεσία που καταγράφει τις χρήσεις αρχείων του χρήστη ομαδοποιώντας τα ανάλογα με την συνάφεια και τις μεταξύ τους σχέσεις.
</div>

<div>
  <p>
    <img class="alignnone img-responsive " title="Zeitgeist" src="http://www.ubuntu-pics.de/bild/39403/screenshot_001_WtCjMj.png" alt="" />
  </p>
</div>

<div id="_mcePaste" style="text-align: justify">
  Πιο αναλυτικά, φανταστείτε μια βάση δεδομένων που ενημερώνετε βάση των συνηθειών ενός χρήστη. Για παράδειγμα, άνοιγμα ή επεξεργασία πέντε αρχείων κειμένου, αναπαραγωγή δέκα μουσικών αρχείων άνοιγμα διάφορων φωτογραφιών και ούτω καθεξής. Αυτή η σειρά των βημάτων του χρήστη, καταγράφετε σε μορφή logs και είναι διαθέσιμη προς εκμετάλλευση από οποιοδήποτε πρόγραμμα το επιθυμεί για λόγους πληροφόρησης του χρήστη. Το <strong><a title="GNOME Activity Journal" href="https://launchpad.net/gnome-activity-journal">GNOME Activity Journal</a></strong> (GAJ) είναι ένα GUI για την παρουσίαση των πληροφοριών αυτών.
</div>

## Εμφάνιση εφαρμογής

<div>
  <img class="    img-responsive " title="GNOME Activity Journal" src="http://www.ubuntu-pics.de/bild/39404/screenshot_002_IY1KPZ.png" alt="" />
</div>

<div id="_mcePaste" style="text-align: justify">
  Η διασύνδεση χρήστη (interface) του αποτελείται από ένα μοναδικό παράθυρο που παρουσιάζει τις σχετικές πληροφορίες σε μορφή 3ημέρου με μία χρονολογική μπάρα στο κάτω μέρος. Τα ίδια τα αρχεία καθώς εμφανίζονται είναι άμεσα προσπελάσιμα με αριστερό κλικ. Πηγαίνοντας τον δείκτη του ποντικιού πάνω σε ένα στοιχείο γίνεται προεπισκόπηση του.Είτε είναι εικόνα, είτε βίντεο, είτε pdf, είτε αρχείο κειμένου κοκ. Εκτός από την ημερήσια ταξινόμηση αναφέρεται επίσης και η θέση στην διάρκεια της μέρας για κάθε ομάδα εγγράφων (πρωί, μεσημέρι, απόγευμα κλπ). Κάθε αντικείμενο μπορεί να γίνει pin στην λίστα Today (δεξί κλικ : Pin To Today) σε περίπτωση που θέλετε να μένει συνεχώς σε εμφανές μέρος. Στα αριστερά και δεξιά του παραθύρου υπάρχουν βελάκια πλοήγησης τόσο για την εμφάνιση του 3ημέρου όσο και για τα χρονολογικά ιστογράμματα.
</div>

## **Επίδειξη εφαρμογής
  
** 

## Εγκατάσταση εφαρμογής

Υπάρχει επιλογή εγκατάστασης από το PPA για τα Zeitgeist και GAJ κατά τη σελίδα

<div id="_mcePaste">
  <a href="https://launchpad.net/~zeitgeist/+archive/ppa">https://launchpad.net/~zeitgeist/+archive/ppa</a>
</div>

<div id="_mcePaste">
  Έπειτα εγκαθιστώνται <strong>είτε</strong> μέσω του Διαχειριστή πακέτων (Synaptic package manager) με τα πακέτα
</div>

<pre>zeitgeist <strong>και</strong> gnome-activity-journal</pre>

<div id="_mcePaste">
  <strong>είτε</strong> μέσω των αντίστοιχων εντολών από την γραμμή εντολών :
</div>

<pre>sudo apt-get install zeitgeist</pre>

<pre>sudo apt-get install gnome-activity-journal</pre>

<div>
  Εναλλακτικά,
</div>

<div>
  Το πακέτο πηγαίου κώδικα για το GNOME Activity Journal μπορείτε να το βρείτε εδώ :
</div>

<div id="_mcePaste">
  <a href="https://launchpad.net/gnome-activity-journal">https://launchpad.net/gnome-activity-journal</a>
</div>

<div>
  Και για το Zeitgeist εδώ :
</div>

<div id="_mcePaste">
  <a href="http://zeitgeist-project.com/download">http://zeitgeist-project.com/download</a>
</div>

## Μετάφραση

<p style="text-align: justify">
  Το λογισμικό του GNOME Activity Journal καθώς και του υποσυστήματος Zeitgeist μεταφράζεται μέσω του Launchpad.net, στην ιστοσελίδα <a href="https://translations.edge.launchpad.net/zeitgeist-project" target="_blank">https://translations.edge.launchpad.net/zeitgeist-project</a> Αυτή τη στιγμή δεν είναι μεταφρασμένο στα ελληνικά και είναι ένα έργο που μπορεί να αναλάβει κάποιος.
</p>

## Συμπεράσματα

<div id="_mcePaste" style="text-align: justify">
  Το Zeitgeist, λοιπόν, καταγράφει τις συνήθειες των χρηστών και συγκεντρώνει τις πληροφορίες ώστε να μπορούν να εκμεταλλευτούν από άλλες εφαρμογές. Περιπτώσεις χρησιμοποίησης των πληροφοριών αυτών μπορούν να υπάρξουν αμέτρητες. Μία εξ αυτών είναι το Docky, το οποίο μέσω ενός Helper που διαθέτει, χρησιμοποιεί τις δυνατότητες του Zeitgeist.
</div>

<div id="_mcePaste">
  <img src="http://www.ubuntu-pics.de/bild/39410/screenshot_001_wPaGnR.png" alt="" />
</div>

<div id="_mcePaste">
  Ένα άκρως ενδιαφέρον Project του GNOME με ποικίλες προεκτάσεις.
</div>

## Σύνδεσμοι

  * Zeitgeist : <http://zeitgeist-project.com/>
  * Zeitgeist @ live.gnome : <http://live.gnome.org/Zeitgeist>
  * GNOME Activity Journal : <http://live.gnome.org/action/show/GnomeActivityJournal>