---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2012-05-18 20:59:32
guid: http://gnome.gr/?p=1381
id: 1381
image: /wp-content/uploads/sites/7/2012/05/fosscomm2012.jpg
permalink: /2012/05/18/gnome-fosscomm-2012-serres/
tags:
  - events
  - fosscomm
title: Παρουσίαση GNOME στη FOSSCOMM 2012 (Σέρρες)
url: /2012/05/18/gnome-fosscomm-2012-serres/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/fosscomm2012.jpg" alt="FOSSCOMM 2012" title="FOSSCOMM 2012" class="aligncenter size-full wp-image-1382 img-responsive " />](http://serres.fosscomm.gr/)

Όπως κάθε χρόνο, έτσι και φέτος, πραγματοποιήθηκε το συνέδριο κοινοτήτων και έργων ανοικτού λογισμικού <a href="http://serres.fosscomm.gr" target="_blank">FOSSCOMM</a>. Φέτος πραγματοποιήθηκε το Σαββατοκύριακο 12-13 Μαΐου, στο ΤΕΙ των Σερρών.

Πολλές κοινότητες παρούσες, πολλές ενδιαφέρουσες παρουσιάσεις.
  
Δεν θα μπορούσε να λείπει η κοινότητα Gnome.gr

Η εκδήλωση ξεκίνησε χαλαρά την Παρασκευή με το Welcome Beer Party!!!
  
[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/beer2012-300x225.jpg" alt="Welcome Beer Party" title="Welcome Beer Party" class="aligncenter size-medium wp-image-1398 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/beer2012.jpg)

Στην διάρκεια του συνεδρίου, βρισκόμασταν στο τραπεζάκι του Gnome.gr όπου ενημερώναμε τους επισκέπτες των Σερρών (και όχι μόνο) για το αγαπημένο μας γραφικό περιβάλλον, για τις νέες λειτουργίες και τον χειρισμό του. Επίσης προμηθεύσαμε με κονκάρδες, μπλουζάκια και αυτοκόλλητα.

Συμμετείχαμε και με ομιλία σχετικά με μια νέα λειτουργία που προστέθηκε, τα Gnome Extensions.
  
Δείτε το αρχείο της παρουσίασης:

Σύντομα θα ακολουθήσει και βίντεο (αφού τα επεξεργαστούν οι διοργανωτές).

Φωτογραφία από την παρουσίαση:
  
[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/DSCF2282-300x225.jpg" alt="Ο Ευστάθιος Ιωσηφίδης στην παρουσίαση Gnome Extensions" title="Gnome Extensions" class="aligncenter size-medium wp-image-1384 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/DSCF2282.jpg)

Ο <a href="http://stathisuse.blogspot.com" target="_blank">Ευστάθιος Ιωσηφίδης</a> παρουσιάζει την ιστοσελίδα για τα Gnome Extensions

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/IMG_6051-300x200.jpg" alt="booth" title="Booth" class="aligncenter size-medium wp-image-1596 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/IMG_6051.jpg) [<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/IMG_5987-300x200.jpg" alt="" title="FOSSCOMM Friends" class="aligncenter size-medium wp-image-1595 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/IMG_5987.jpg)

Η παρουσίαση του φετινού FOSSCOMM είναι διαθέσιμη για [λήψη](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/Gnome-extensions.pdf "FOSSCOMM_2012"),όπως επίσης και οι παρουσιάσεις των προηγούμενων εκδηλώσεων στη σελίδα <http://gnome.gr/conferences/>
