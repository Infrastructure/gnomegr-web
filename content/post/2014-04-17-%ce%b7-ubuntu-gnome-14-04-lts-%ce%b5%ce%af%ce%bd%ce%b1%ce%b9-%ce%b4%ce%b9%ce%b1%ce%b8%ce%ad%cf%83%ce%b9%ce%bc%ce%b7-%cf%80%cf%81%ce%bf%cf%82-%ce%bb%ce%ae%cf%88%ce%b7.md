---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3288378048803482503
blogger_permalink:
  - /2014/04/ubuntu-gnome-1404-lts-released.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-04-17 23:36:00
guid: http://gnome.gr/2014/04/17/%ce%b7-ubuntu-gnome-14-04-lts-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b4%ce%b9%ce%b1%ce%b8%ce%ad%cf%83%ce%b9%ce%bc%ce%b7-%cf%80%cf%81%ce%bf%cf%82-%ce%bb%ce%ae%cf%88%ce%b7/
id: 2900
permalink: /2014/04/17/%ce%b7-ubuntu-gnome-14-04-lts-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b4%ce%b9%ce%b1%ce%b8%ce%ad%cf%83%ce%b9%ce%bc%ce%b7-%cf%80%cf%81%ce%bf%cf%82-%ce%bb%ce%ae%cf%88%ce%b7/
title: Η Ubuntu GNOME 14.04 LTS είναι διαθέσιμη προς λήψη
url: /2014/04/17/%ce%b7-ubuntu-gnome-14-04-lts-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b4%ce%b9%ce%b1%ce%b8%ce%ad%cf%83%ce%b9%ce%bc%ce%b7-%cf%80%cf%81%ce%bf%cf%82-%ce%bb%ce%ae%cf%88%ce%b7/
---

Η ομάδα <a href="https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved/WhoWeAre" target="1">Ubuntu GNOME</a> σας παρουσιάζει με χαρά την διάθεση της νέας έκδοσης _Ubuntu GNOME 14.04 LTS_.

Η <a href="https://wiki.ubuntu.com/UbuntuGNOME" target="1">Ubuntu GNOME</a> είναι πλέον επίσημη γεύση της διανομής <a href="http://www.ubuntu.com/" target="1">Ubuntu</a>, που σας παρουσιάζει το περιβάλλον εργασίας <a href="http://www.gnome.org/" target="1">GNOME</a>. Αυτή είναι η πρώτη μας έκδοση <a href="https://wiki.ubuntu.com/LTS" target="1">Long Term Release (LTS)</a> ή αλλιώς έκδοση μακράς υποστήριξης.

**Σημειώσεις Έκδοσης**

Πριν κατεβάσετε την έκδοση Ubuntu GNOME 14.04 LTS, παρακαλούμε διαβάστε τις **σημειώσεις της έκδοσης**:  
<a href="https://wiki.ubuntu.com/TrustyTahr/ReleaseNotes/UbuntuGNOME" target="1">https://wiki.ubuntu.com/TrustyTahr/ReleaseNotes/UbuntuGNOME</a>

**Λήψη Ubuntu GNOME 14.04 LTS**

Υπάρχουν κάποια σημαντικά βήματα πριν την εγκατάσταση της έκδοσης Ubuntu GNOME 14.04 LTS. Παρακαλούμε δαβάστε τα προσεκτικά: <a href="https://wiki.ubuntu.com/UbuntuGNOME/GetUbuntuGNOME" target="1">Λήψη Ubuntu GNOME 14.04 LTS</a>

**Ανακοίνωση και Σημειώσεις Έκδοσης Ubuntu**

Παρακαλούμε <a href="https://lists.ubuntu.com/archives/ubuntu-announce/2014-April/000182.html" target="1">δείτε τα στον σύνδεσμο</a>.

**Επικοινωνήστε μαζί μας**

<a href="https://wiki.ubuntu.com/UbuntuGNOME/ContactUs" target="1">Δείτε</a> τα κανάλια επικοινωνίας ή επικοινωνήστε μαζί μας στο mail info AT ubuntugnome ΤΕΛΕΙΑ gr

Σας ευχαριστούμε για την καλή δουλειά που κάνατε για να βγει η 14.04.