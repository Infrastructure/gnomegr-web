---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
date: 2016-04-13 12:15:08
guid: https://static.gnome.org/gnome-gr/?p=3270
id: 3270
image: /wp-content/uploads/sites/7/2016/02/Gstreamer-logo.png
permalink: /2016/04/13/gstreamer-spring-hackfest-2016-sti-thessaloniki/
tags:
  - events
  - gnome
  - Gstreamer
  - ΕΛ/ΛΑΚ
  - θεσσαλονίκη
title: GStreamer Spring Hackfest 2016 στη Θεσσαλονίκη
url: /2016/04/13/gstreamer-spring-hackfest-2016-sti-thessaloniki/
---

<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2016/02/Gstreamer-logo.png" alt="Gstreamer" class="aligncenter size-full wp-image-3271 img-responsive " />

Από 13 έως 15 Μαΐου 2016 θα πραγματοποιηθεί το GStreamer Spring Hackfest στο <a href="http://coho.gr/" target="_blank">http://coho.gr/</a> στη Θεσσαλονίκη. Η οργάνωσή του γίνεται από την ομάδα GStreamer του GNOME.

Όσοι θέλουν να συμμετάσχουν στο Hackfest, μπορούν να επισκεφτούν αρχικά το wiki
  
<a href="https://wiki.gnome.org/Hackfests/GstSpringHackfest2016" target="_blank">https://wiki.gnome.org/Hackfests/GstSpringHackfest2016</a> και να δηλώσουν συμμετοχή. Για οποιαδήποτε απορία, μπορείτε να επικοινωνήσετε με τους Βίβια Νικολαΐδου ή Sebastian Dröge.

**ΤΙ ΕΙΝΑΙ ΤΟ GSTREAMER**

Το **<a href="https://gstreamer.freedesktop.org/" target="_blank">GStreamer</a>** αποτελεί ίσως το πιο σημαντικό σύστημα πολυμέσων, γραμμένο στην γλώσσα C ενώ το βασικό σύστημα στηρίζεται στην GObject.

Το GStreamer επιτρέπει στον προγραμματιστή να δημιουργήσει μια ποικιλία από συστατικά που διαχειρίζονται τα πολυμέσα, συμπεριλαμβανομένων της απλής αναπαραγωγής ήχου, βίντεο, εγγραφή, μετάδοση αλλά και επεξεργασία ήχου. Το όλο σύστημα είναι σχεδιασμένο να εξυπηρετεί την δημιουργία πολλών τύπων εφαρμογών πολυμέσων όπως επεξεργαστές βίντεο, αναπαραγωγής μουσικής κ.α.

Είναι σχεδιασμένο να δουλεύει σε μια ποικιλία λειτουργικών συστημάτων όπως συστήματα βασισμένα σε πυρήνα Linux, BSD, OpenSolaris, Android, OS X, iOS, Windows, OS/400.