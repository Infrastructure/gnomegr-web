---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2012-01-24 12:21:57
guid: http://gnome.gr/?p=1198
id: 1198
permalink: /2012/01/24/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-google-code-in-20112012/
title: Αποτελέσματα Google Code In 2011/2012
url: /2012/01/24/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-google-code-in-20112012/
---

[<img class="aligncenter size-full wp-image-1200 img-responsive " title="GoogleCodeIn-Banner" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/01/GoogleCodeIn-Banner.png" alt="gci-banner" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/01/GoogleCodeIn-Banner.png)

&nbsp;

Το <a href="http://www.google-melange.com/gci/homepage/google/gci2011" target="_blank">Google Code In</a> είναι μια πρωτοβουλία της Google για να βοηθήσει τους μαθητές να προσφέρουν στο ελεύθερο λογισμικό.

Οι ελληνικές κοινότητες των <a href="http://gnome.gr/" target="_blank">GNOME</a>, <a href="http://el.opensuse.org/" target="_blank">OpenSUSE</a> και <a href="http://haiku-os.gr/" target="_blank">Haiku-OS</a> έλαβαν μέρος στο Google Code In 2011/2012 και έθεσαν **38 εργασίες** για να αναλάβουν οι μαθητές.

&nbsp;

Το Google Code In ολοκληρώθηκε πριν μια βδομάδα και αυτά είναι τα αποτελέσματα:

ΣΥΝΟΛΙΚΑ (OpenSUSE: 19 + GNOME: 16 + Haiku-OS: 3 = 38 εργασίες)

  * Vagelis:        3
  * Chris:         13
  * Panos:         5
  * Zacharias:    4
  * Giannis:       9
  * Panos_lead: 3
  * GeorgeA:     1

&nbsp;

Για κάθε τριάδα εργασιών, οι μαθητές λαμβάνουν $100, με μέγιστο τις 5 τριάδες ανά μαθητή.Οπότε, από το παραπάνω έργο, το συνολικό ποσό είναι $1100.Ο **Giannis** εργάστηκε και σε άλλα έργα και ολοκλήρωσε επιπλέον 22 εργασίες (σύνολο 31),που ίσως αποτελεί ρεκόρ.

&nbsp;

Ο κάθε μαθητής και ο κάθε μέντορας θα λάβει από ένα αναμνηστικό μπλουζάκι από την Google.Σε λίγες εβδομάδες θα υπάρξει μια διαδικασία για τη βράβευση μαθητών που διακρίθηκαν για το έργο τους.

&nbsp;

Θα θέλαμε να ευχαριστήσουμε τους μαθητές για το έργο τους,

&nbsp;

Οι μέντορες:

  * Haiku-OS: <a href="http://twitter.com/Drakevr" target="_blank">Αλέξης-Π. Νάτσιος</a>
  * GNOME: <a href="http://stathisuse.blogspot.com/" target="_blank">Ευστάθιος Ιωσηφίδης</a>, <a href="http://simos.info/blog/" target="_blank">Σίμος Ξενιτέλλης</a>
  * OpenSUSE: <a href="http://e-tote-kala.blogspot.com/" target="_blank">Κωνσταντίνος Κουδάρας</a>, <a href="http://stathisagrapidis.wordpress.com/" target="_blank">Ευστάθιος Αγραπίδης</a>, <a href="http://stathisuse.blogspot.com/" target="_blank">Ευστάθιος Ιωσηφίδης</a>
