---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/6963161652265980438
blogger_permalink:
  - /2014/04/ubuntu-gnome-trusty-tahr-rc.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-04-13 11:47:00
guid: http://gnome.gr/2014/04/13/ubuntu-gnome-trusty-tahr-release-candidate/
id: 2901
permalink: /2014/04/13/ubuntu-gnome-trusty-tahr-release-candidate/
title: Ubuntu GNOME Trusty Tahr Release Candidate
url: /2014/04/13/ubuntu-gnome-trusty-tahr-release-candidate/
---

Αυτή είναι η τελική εβδομάδα για το Trusty Tahr. Είμαστε στην τελική φάση του κύκλου ανάπτυξης.

**The Final Freeze vs The Final Release**  
Ποιά είναι η διαφορά μεταξύ του The Final Release και The Final Freeze.

<a href="https://wiki.ubuntu.com/FinalFreeze" target="1">Final Freeze</a> – April 10th

<a href="https://wiki.ubuntu.com/FinalRelease" target="1">Final Release</a> – April 17th

Ο Adam Conrad εξήγησε με λεπτομέρειες στο <a href="https://www.mail-archive.com/ubuntu-release@lists.ubuntu.com/msg02602.html" target="1">email</a> ανακοίνωσης The Final Freeze του κύκλου για την Trusty Tahr.

**Τι σημαίνουν όλα αυτά;**  
Σημαίνουν ότι τα καθημερινά στιγμιότυπα της Ubuntu GNOME Trusty είναι στην κατάσταση RC.

**Τι σημαίνει RC (Release Candidate);**

<a href="https://wiki.ubuntu.com/ReleaseCandidate" target="1">Release Candidate</a>

“Κατά τη διάρκεια της εβδομάδας που θα μας οδηγήσει στην τελική έκδοση (final release), τα στιγμιότυπα που παράγονται, χαρακτηρίζονται όλα ως release candidates.”

**Τελικός γύρος δοκιμών της Ubuntu GNOME Trusty Tahr**  
Αυτός είναι ο τελικός γύρος και η τελική εβδομάδα δοκιμών της Ubuntu GNOME Trusty Tahr.

Η ομάδα Ubuntu GNOME QA <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">δοκιμάζει πλέον την Ubuntu GNOME Trusty Tahr Release Candidate</a>.

<a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">Όλα όσα πρέπει να γνωρίζετε για να βοηθήσετε στις δοκιμές του Ubuntu GNOME.</a>

<a href="http://cdimage.ubuntu.com/ubuntu-gnome/daily-live/current" target="1">Λήψη και δοκιμή της Ubuntu GNOME Trusty Tahr Release Candidate.</a>

<a href="https://wiki.ubuntu.com/UbuntuGNOME/ContactUs" target="1">Τρόποι επικοινωνίας με την κοινότητα.</a>

Σας ευχαριστούμε που επιλέξατε και στηρίζετε το Ubuntu GNOME. Χωρίς την βοήθειά σας δεν θα είχαμε φτάσει σε αυτό το σημείο.