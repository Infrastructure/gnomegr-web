---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2015-03-10 12:53:39
guid: http://gnome.gr/?p=3024
id: 3024
permalink: /2015/03/10/gsoc-outreachy/
tags:
  - gnome
  - GSoC
  - GSoC 2015
  - Outreachy
title: Έγκριση συμμετοχής στο GSoC και συμμετοχή στο Outreachy
url: /2015/03/10/gsoc-outreachy/
---

Το GSoC και το Outreachy είναι δυο προγράμματα που προσφέρουν την δυνατότητα σε άτομα που ασχολούνται με το ΕΛ/ΛΑΚ, να αποκτήσουν χρήματα μέσα από την δουλειά που κάνουν ούτως ή άλλως αλλά και αναγνώριση στο βιογραφικό τους.
  
Το GSoC διοργανώνεται από την Google, αφορά τους φοιτητές και η διάρκειά του είναι το καλοκαίρι. Το Outreachy (μετονομασία του Outreach Program for Women και αλλαγή διοργανωτή) είναι ένα πρόγραμμα που μπορούν να συμμετέχουν γυναίκες. Διοργανώνεται δυο φορές τον χρόνο.

[<img class="aligncenter size-full wp-image-3026 img-responsive " src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/03/gsoc2015.png" alt="gsoc2015" />](https://www.google-melange.com/gsoc/homepage/google/gsoc2015)**Google Summer of Code**Το GSoC είναι ένα πρόγραμμα για φοιτητές, οι οποίοι μπορούν κατά τη διάρκεια των καλοκαιρινών διακοπών τους να φέρουν εις πέρας ένα θέμα, μια εργασία που επιλέγουν οι ίδιοι. Δουλεύουν σε συνεργασία με έναν mentor που ορίζεται από τον οργανισμό που συμμετέχει (GNOME). Ο φοιτητής πληρώνεται στην αρχή, στον ενδιάμεσο έλεγχο και με την παράδοση του αποτελέσματος. Το ποσό είναι 5500$.
  
Κάθε οργανισμός, κάνει αίτηση στην Google με τις εργασίες που θέλει. Ο οργανισμός GNOME εγκρίθηκε για ακόμα μια χρονιά. Περισσότερες πληροφορίες μπορείτε να δείτε στις παρακάτω ιστοσελίδες.

Η Κεντρική ιστοσελίδα με περισσότερες πληροφορίες είναι η
  
<a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2015" target="_blank">https://www.google-melange.com/gsoc/homepage/google/gsoc2015</a>
  
Η ιστοσελίδα του GNOME στο σύστημα της Google είναι η
  
<a href="https://www.google-melange.com/gsoc/org2/google/gsoc2015/gnome" target="_blank">https://www.google-melange.com/gsoc/org2/google/gsoc2015/gnome</a>
  
Ενώ για ιδέες των εργασιών βρίσκονται στην ιστοσελίδα
  
<a href="https://wiki.gnome.org/Outreach/SummerOfCode/2015/Ideas" target="_blank">https://wiki.gnome.org/Outreach/SummerOfCode/2015/Ideas</a>

Έχετε υπόψιν σας το ημερολόγιο
  
<a href="https://www.google-melange.com/gsoc/events/google/gsoc2015" target="_blank">https://www.google-melange.com/gsoc/events/google/gsoc2015</a>

Καταληκτική ημερομηνία υποβολής αίτησης από τους φοιτητές είναι η 27η Μαρτίου.
  
**ΣΥΜΒΟΥΛΗ:** Να έχετε έναν στόχο με τι θα ασχοληθείτε. Κάνετε κάποια συνεισφορά (ώστε να μην φαίνεστε ουρανοκατέβατος). Εγγραφείτε στις λίστες των οργανισμών και προτείνετε το θέμα που θέλετε να ασχοληθείτε, ώστε να απαντήσει κάποιος που θέλει να γίνει mentor. Καταχωρείτε το θέμα που θέλετε και περιμένετε να γίνει δεκτή η αίτησή σας.

## **Outreachy**

&nbsp;

Για το Outreachy έχουμε αναφερθεί και σε <a href="http://gnome.gr/2015/02/18/outreachy-2015-25-may-25-aug/" target="_blank">προηγούμενο άρθρο</a>. Περιληπτικά, είναι ένα πρόγραμμα που αφορά γυναίκες (cis&trans women, trans men, genderqueer people), που θέλουν να εργαστούν στο ΕΛ/ΛΑΚ, αποκτούν εμπειρία και αμοίβονται με 5500$ (με τον ίδιο τρόπο όπως και το GSoC).

Η ιστοσελίδα με τους οργανσιμούς και περισσότερες πληροφορίες σχετικά με το πρόγραμμα βρίσκεται στην
  
<a href="https://wiki.gnome.org/Outreachy/2015/MayAugust" target="_blank">https://wiki.gnome.org/Outreachy/2015/MayAugust</a>
  
Η ιστοσελίδα με τις προτεινόμενες εργασίες βρίσκεται στην
  
<a href="https://wiki.gnome.org/Outreach/SummerOfCode/2015/Ideas" target="_blank">https://wiki.gnome.org/Outreach/SummerOfCode/2015/Ideas</a>

Καταληκτική ημερομηνία υποβολής αίτησης είναι η 24η Μαρτίου.
  
**ΣΥΜΒΟΥΛΗ:** Να έχετε έναν στόχο με τι θα ασχοληθείτε. Κάνετε κάποια συνεισφορά (ώστε να μην φαίνεστε ουρανοκατέβατη). Εγγραφείτε στις λίστες των οργανισμών και προτείνετε το θέμα που θέλετε να ασχοληθείτε, ώστε να απαντήσει κάποιος που θέλει να γίνει mentor. Καταχωρείτε το θέμα που θέλετε και περιμένετε να γίνει δεκτή η αίτησή σας.

Η ιστοσελίδα με τα θέματα είναι ίδια, ώστε να μην γίνεται διπλή δουλειά.<figure id="attachment_3000" style="width: 664px" class="wp-caption aligncenter">

[<img class=" wp-image-3000 img-responsive " src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/02/outreachy-poster-2015-May-August.png" alt="Outreachy 2015" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/02/outreachy-poster-2015-May-August.png)<figcaption class="wp-caption-text">Outreachy 2015</figcaption></figure>
