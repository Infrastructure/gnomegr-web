---
author: iosifidis
categories:
  - Ειδήσεις
  - Παρουσίαση
date: 2015-08-19 11:18:30
guid: https://static.gnome.org/gnome-gr/?p=3213
id: 3213
permalink: /2015/08/19/allakste-to-gnome-me-extensions/
tags:
  - extensions
  - gnome
  - gnome shell
title: Αλλάξτε το GNOME με τα πρόσθετα
url: /2015/08/19/allakste-to-gnome-me-extensions/
---

Έχουμε δει σε προηγούμενο άρθρο την <a href="https://static.gnome.org/gnome-gr/2011/12/02/%CE%B9%CF%83%CF%84%CE%BF%CF%83%CE%B5%CE%BB%CE%AF%CE%B4%CE%B1-%CE%B3%CE%B9%CE%B1-%CF%84%CE%B1-%CF%80%CF%81%CF%8C%CF%83%CE%B8%CE%B5%CF%84%CE%B1-%CF%84%CE%BF%CF%85-gnome-shell/" target="_blank">ιστοσελίδα με τα πρόσθετα</a> του GNOME.

Η λειτουργία των πρόσθετων, έχει προστεθεί διότι υπήρξαν πολλοί που θεωρούσαν πως δεν ήταν χρηστικό το GNOME. Τα πρόσθετα δεν υποστηρίζονται επίσημα από το GNOME, ενώ ο κάθε προγραμματιστής είναι υπεύθυνος να συντηρεί το πρόσθετο που έχει αναπτύξει.

Αν και υπάρχουν πολλά πρόσθετα, εδώ θα αναφέρουμε μερικά χρήσιμα για πολλούς (προσοχή, μπορεί να μην ταιριάζουν όλα στην έκδοση GNOME που έχετε εγκατεστημένη).

<div class="crafted_content">
  <div class="section image-left">
    <div class="image">
      <img src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_307_VW5dorQ.png" alt="Dash to dock" />
    </div>
    
    <h2>
      <a href="https://extensions.gnome.org/extension/307/dash-to-dock/" target="_blank">Dash to Dock</a>
    </h2>
    
    <p>
      Ίσως είναι το πιο χρήσιμο πρόσθετο. Αυτό που κάνει είναι να εμφανίζει μόνιμα την μπάρα με τα αγαπημένα προγράμματα και να την εξαφανίζει μόλις έχετε ανοικτό σε πλήρη οθόνη το πρόγραμμα που χρησιμοποιείτε.
    </p>
  </div>
  
  <div class="section image-left">
    <div class="image">
      <img src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_442_1.png" alt="Drop Down Terminal" />
    </div>
    
    <h2>
      <a href="https://extensions.gnome.org/extension/442/drop-down-terminal/" target="_blank">Drop Down Terminal</a>
    </h2>
    
    <p>
      Το GNOME έχει σχεδιαστεί αρκετά γι’αυτούς που χρησιμοποιούν το πληκτρολόγιό τους. Αυτό το πρόσθετο εμφανίζει στο επάνω μέρος ένα τερματικό με το πάτημα του F12 ή του ~. Είναι χρήσιμο για όσους χρησιμοποιούν τερματικό συχνά.
    </p>
  </div>
  
  <div class="section image-left">
    <div class="image">
      <img src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_495.png" height="263px" alt="Top Icons" />
    </div>
    
    <h2>
      <a href="https://extensions.gnome.org/extension/495/topicons/" target="_blank">TopIcons</a>
    </h2>
    
    <p>
      Η μπάρα με τα ανοικτά προγράμματα (πχ skype) έχει αλλάξει στην έκδοση GNOME 3.16. Βρίσκεται πλέον κάτω αριστερά και εμφανίζεται μόνο εάν μετακινήσετε τον δείκτη προς τα εκεί. Με το πρόσθετο αυτό, μετακινούνται στην επάνω μπάρα και είναι μόνιμα εμφανή.
    </p>
  </div>
  
  <div class="section image-left">
    <h2>
      <a href="https://extensions.gnome.org/extension/937/laine/" target="_blank">Laine</a>
    </h2>
    
    <p>
      Το πρόσθετο αυτό ξεχωρίζει τον ήχο από το μενού του χρήστη. Εμφανίζει πολύ εύκολα την επιλογή εξόδου του ήχου (πχ από τα ακουστικά ή τα ηχεία), καθώς και εμφανίζει μπάρες ελέγχου του ήχου μεμονωμένα στα προγράμματα ήχου που εκτελούνται εκείνη τη στιγμή.
    </p>
  </div>
  
  <div class="section image-left">
    <h2>
      <a href="https://extensions.gnome.org/extension/6/applications-menu/" target="_blank">Applications Menu</a>
    </h2>
    
    <p>
      Το πρόσθετο αυτό είναι για τους λάτρεις του παλαιού GNOME. Περιέχει ανά κατηγορίες τα προγράμματα. Μάλιστα με τις τελευταίες αναβαθμίσεις, μοιάζει πολύ με το μενού του Cinnamon.
    </p>
  </div>
  
  <div class="section image-left">
    <div class="image">
      <img class="aligncenter img-responsive " src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_358_PShPFmS.png" alt="Activities Configurator" />
    </div>
    
    <h2>
      <a href="https://extensions.gnome.org/extension/358/activities-configurator/" target="_blank">Activities Configurator</a>
    </h2>
    
    <p>
      Το πρόσθετο αυτό χρησιμοποιήστε το με το παραπάνω. Αυτό που κάνει είναι να αλλάξει την λέξη <em>Δραστηριότητες</em> σε ότι θέλετε εσείς. Μπορείτε να εισάγετε ακόμα και το εικονίδιο της διανομής σας. </div> 
      
      <div class="section image-left">
        <div class="image">
          <img class="aligncenter img-responsive " src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_549_1.png" alt="Web Search Dialog" />
        </div>
        
        <h2>
          <a href="https://extensions.gnome.org/extension/549/web-search-dialog/" target="_blank">Web Search Dialog</a>
        </h2>
        
        <p>
          Τα πλήκτρα +space εμφανίζουν ένα πεδίο για αναζήτηση σε Google, Duckduckgo, Yandex, Bing, Wiki (το ρυθμίζετε εσείς). Είναι χρήσιμο, διότι όπως είπαμε δεν χρειάζεται να σηκώσετε τα χέρια σας από το πληκτρολόγιο. Επίσης τα πλήκτρα Super+Space αλλάζει η γλώσσα.
        </p>
      </div>
      
      <div class="section image-left">
        <div class="image">
          <img class="aligncenter img-responsive " src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_690_1.png" alt="EasyScreenCast" />
        </div>
        
        <h2>
          <a href="https://extensions.gnome.org/extension/690/easyscreencast/" target="_blank">EasyScreenCast</a>
        </h2>
        
        <p>
          Η εγγραφή της επιφάνειας εργασίας σε βίντεο είναι πανεύκολη διότι μπορείτε να πατήσετε τα πλήκτρα <em>ALT+CTRL+SHIFT+R</em> και να ξεκινήσει η εγγραφή. Όμως μερικές φορές δεν δουλεύει καλά σε όλες τις διανομές. Επίσης δεν έχει επιλογές. Με αυτό το πρόσθετο, μπορείτε εύκολα να ρυθμίσετε ότι θέλετε για την εγγραφή της επιφάνειας εργασίας σας. </div> 
          
          <div class="section image-left">
            <div class="image">
              <img class="aligncenter img-responsive " src="https://extensions.gnome.org/static/extension-data/screenshots/screenshot_48_LXIenlM.png" alt="Trash" />
            </div>
            
            <h2>
              <a href="https://extensions.gnome.org/extension/48/trash/" target="_blank">Trash</a>
            </h2>
            
            <p>
              Όταν διαγράφετε κάτι, αυτό αυτόματα μετακινείται στον κάδο απορριμάτων. Με το πρόσθετο αυτό, εμφανίζεται ένας κάδος απορριμάτων στην πάνω μπάρα, και έτσι γνωρίζετε ότι έχετε αρχεία που πρέπει να απομακρύνετε.
            </p>
          </div></div>