---
author: nikosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2008-09-08 11:52:07
guid: http://gnome.gr/?p=30
id: 30
permalink: /2008/09/08/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-224/
title: Ανακοίνωση έναρξης μετάφρασης του GNOME 2.24
url: /2008/09/08/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-224/
---

Τον Οκτώβρη 2008 θα βγει το νέο GNOME 2.24, <a class="urlextern" title="http://live.gnome.org/TwoPointTwentythree" rel="nofollow" href="http://live.gnome.org/TwoPointTwentythree">http://live.gnome.org/TwoPointTwentythree</a>

Το GNOME είναι το βασικό περιβάλλον εργασίας σε διανομές όπως Ubuntu Linux και Fedora Linux.

Για τη μετάφρασή του η πρώτη προθεσμία για τη συμπλήρωση είναι στις 20 Σεπτεμβρίου.

Το λεγόμενο string freeze (περίοδος όπου οι προγραμματιστές δεν κάνουν αλλαγές που θα μπορούσαν να αλλάξουν τα μεταφράσιμα μηνύματα) ξεκινά την 1τη Σεπτεμβρίου.

Η λίστα με τα πακέτα που είναι διαθέσιμα για μετάφραση είναι στο <a class="urlextern" title="http://l10n.gnome.org/languages/el/gnome-2-24" rel="nofollow" href="http://l10n.gnome.org/languages/el/gnome-2-24">http://l10n.gnome.org/languages/el/gnome-2-24</a>

Η μετάφραση του GNOME αποτελείται από δύο μέρη, την τεκμηρίωση και τα μηνύματα του γραφικού περιβάλλοντος. Αυτή τη στιγμή έχουμε αξιόλογη μετάφραση στην τεκμηρίωση (28%) ενώ για το γραφικό περιβάλλον έχουμε πέσει στο 86%.

Παραδοσιακά δίνουμε έμφαση στη μετάφραση του γραφικού περιβάλλοντος (UI). Είναι σημαντικό να προχωρήσει και η μετάφραση της τεκμηρίωσης, και συγκεκριμένα πακέτων που βλέπει πιο συχνά ο χρήστης (gnome-panel, κτλ).

Αν θέλετε περισσότερες προτάσεις για το τι να μεταφράσετε, ρωτήστε τη λίστα team at lists.gnome.gr.

Για το γραφικό περιβάλλον, τα στατιστικά λένε

<li class="level1">
  <div class="li">
    <code>Μεταφρασμένα μηνύματα  36060	(μηνύματα)</code>
  </div>
</li>

<li class="level1">
  <div class="li">
    <code>Ασαφή                3740	(μηνύματα)</code>
  </div>
</li>

<li class="level1">
  <div class="li">
    <code>Αμετάφραστα          1963	(μηνύματα)</code>
  </div>
</li>

Τα πακέτα στα οποία είναι καλό να επικεντρωθούμε είναι αυτά που είναι πιο πιθανό να δει ο τελικός χρήστης. Για τα παρακάτω δεν τα έχει αναλάβει κάποιος, οπότε αν κάποιος θέλει να δοκιμάσει μπορεί να το αναφέρει στη λίστα <a class="urlextern" title="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" rel="nofollow" href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr">http://lists.gnome.gr/listinfo.cgi/team-gnome.gr</a>

Είναι καλό, τουλάχιστον για τις μεγάλες μεταφράσεις, να πει κάποιος ότι έχει ξεκινήσει τη μετάφραση στην παραπάνω λίστα για να αποφύγουμε παράλληλες μεταφράσεις.

Κάποιες πρώτες επιλογές

<li class="level1">
  <div class="li">
    Hamster-applet (διαχείριση του χρόνου, π.χ. πότε κάνουμε τι, κτλ)
  </div>
</li>

<li class="level1">
  <div class="li">
    gnome-backgrounds
  </div>
</li>

Από την τεκμηρίωση,

<li class="level1">
  <div class="li">
    gnome-games (διάφορα παιχνίδια)
  </div>
</li>

<li class="level1">
  <div class="li">
    gnome-applets
  </div>
</li>

Αν είναι να ξεκινήσετε με μεταφράσεις, δοκιμάστε με μια από τις μικρές μεταφράσεις πρώτα.

Όταν ενημερωθεί μια μετάφραση, μπορεί να σταλθεί στη λίστα <a class="urlextern" title="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" rel="nofollow" href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr">http://lists.gnome.gr/listinfo.cgi/team-gnome.gr</a> για να προωθηθεί στο GNOME SVN.

Για νέες μεταφράσεις είναι καλό να χρησιμοποιήσετε τη μεταφραστική μνήμη του GNOME που συμπληρώνει αυτόματα κάποιο αριθμό μηνυμάτων.

Για οδηγίες, δείτε Το Wiki του GNOME.gr <a class="urlextern" title="http://wiki.gnome.gr/" rel="nofollow" href="http://wiki.gnome.gr/">http://wiki.gnome.gr/</a>

Σελίδα του Ubuntu-gr για τις μεταφράσεις <a class="urlextern" title="http://wiki.ubuntu-gr.org/Wiki/Translation/Upstream/Gnome" rel="nofollow" href="http://wiki.ubuntu-gr.org/Wiki/Translation/Upstream/Gnome">http://wiki.ubuntu-gr.org/Wiki/Translation/Upstream/Gnome</a>

Τα παρακάτω άτομα μπορούν να αναλάβουν να στείλουν μεταφράσεις στο GNOME SVN και έχουν καθήκον να σας εξυπηρετήσουν, <img class="middle img-responsive " src="http://wiki.gnome.gr/lib/images/smileys/icon_wink.gif" alt=";-)" />

<a class="urlextern" title="http://cia.vc/stats/author/dglezos" rel="nofollow" href="http://cia.vc/stats/author/dglezos">http://cia.vc/stats/author/dglezos</a>

<a class="urlextern" title="http://cia.vc/stats/author/frolix68" rel="nofollow" href="http://cia.vc/stats/author/frolix68">http://cia.vc/stats/author/frolix68</a>

<a class="urlextern" title="http://cia.vc/stats/author/pkst" rel="nofollow" href="http://cia.vc/stats/author/pkst">http://cia.vc/stats/author/pkst</a>

<a class="urlextern" title="http://cia.vc/stats/author/simos" rel="nofollow" href="http://cia.vc/stats/author/simos">http://cia.vc/stats/author/simos</a>

Από την ομάδα μετάφρασης του GNOME, <a class="urlextern" title="https://www.gnome.gr/" rel="nofollow" href="https://www.gnome.gr/">https://www.gnome.gr/</a>
