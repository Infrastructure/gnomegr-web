---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2011-11-22 22:49:09
guid: http://gnome.gr/?p=1109
id: 1109
permalink: /2011/11/22/%ce%bb%ce%ac%ce%b2%ce%b5%cf%84%ce%b5-%ce%bc%ce%ad%cf%81%ce%bf%cf%82-%cf%83%cf%84%ce%bf-google-code-in-2011-%ce%b3%ce%b9%ce%b1-%ce%bc%ce%b1%ce%b8%ce%b7%cf%84%ce%ad%cf%82-%ce%b3%cf%85%ce%bc%ce%bd/
title: Λάβετε μέρος στο Google Code In (2011) (για μαθητές Γυμνασίου/Λυκείου)
url: /2011/11/22/%ce%bb%ce%ac%ce%b2%ce%b5%cf%84%ce%b5-%ce%bc%ce%ad%cf%81%ce%bf%cf%82-%cf%83%cf%84%ce%bf-google-code-in-2011-%ce%b3%ce%b9%ce%b1-%ce%bc%ce%b1%ce%b8%ce%b7%cf%84%ce%ad%cf%82-%ce%b3%cf%85%ce%bc%ce%bd/
---

<div>
  Το<a title="Google Code In (2011/2012)" href="http://code.google.com/opensource/gci/2011-12/index.html" target="_blank"> Google Code In</a> είναι μια προσπάθεια της Google ώστε να ενθαρρύνει μαθητές Γυμνασίου και Λυκείου να ασχοληθούν με το ΕΛ/ΛΑΚ, ολοκληρώνοντας μικρές εργασίες.<br /> Η <a title="Ελληνική κοινότητα GNOME" href="https://static.gnome.org/gnome-gr" target="_blank">ελληνική κοινότητα GNOME</a> λαμβάνει μέρος μαζί με <a title="Έργο GNOME" href="http://www.gnome.org" target="_blank">την κοινότητα GNOME</a>, και έχουμε διαθέσιμες μια σειρά από εργασίες για ανάληψη.<br /> Για το Google Code In (2011), η ανταμοιβή των μαθητών είναι
</div>

<p style="padding-left: 30px">
  8. PRIZES FOR COMPLETED ENTRIES: Each Participant submitting at least <strong>one (1) Completed Entry</strong> will receive one T-shirt with an approximate value of $24 USD (twenty four U.S. dollars). For every three (3) Completed Entries submitted by a Participant, that Participant will receive a $100 USD (one hundred U.S. dollars) prize up to a maximum of $500. Participants may submit as many Entries as they choose but each eligible Participant is limited to one T-shirt and $500 USD (five hundred U.S. dollars) in prizes. No prizes in excess of this limit will be awarded, unless the Entrant is a Grand Prize Winner, in which case the provisions in Section 9 will apply.
</p>

<div>
  Δηλαδή με την ολοκλήρωση μιας εργασίας  λαμβάνεται ένα μπλουζάκι ενώ με την ολοκλήρωση 3 εργασιών λαμβάνεται $100.<br /> Για να λάβετε μέρος,</p> 
  
  <ol>
    <li>
      Γραφτείτε στη λίστα του gnome.gr, στο <a href="https://static.gnome.org/gnome-gr/">www.gnome.gr</a>
    </li>
    <li>
      Γραφτείτε στο <a href="http://google-melange.appspot.com/gci/homepage/google/gci2011">http://google-melange.appspot.com/gci/h &#8230; le/gci2011</a> και δείτε τις διαθέσιμες εργασίες για το GNOME. Για να δείτε τις εργασίες που έχουμε βάλει ως Ελληνική κοινότητα GNOME, γράψτε «Greek» στο κουτί του Title για να δείξει τις εργασίες που σχετίζονται με τα ελληνικά.
    </li>
    <li>
      Φυσικά μπορείτε να λάβετε μέρος σε οποιαδήποτε διαθέσιμη εργασία, ακόμα και αν δε σχετίζεται με τα ελληνικά.
    </li>
  </ol>
</div>

<div>
  Δείτε δείγμα διαθέσιμων εργασιών:
</div>

<div>
  <a href="http://www.google-melange.com/gci/task/view/google/gci2011/7176202" target="_blank">Translate to Greek the Python Demos in GNOME Developer documentation</a>
</div>

<div>
  <a href="http://www.google-melange.com/gci/task/view/google/gci2011/7130206" target="_blank">Complete the gbrainy translation into Greek</a>
</div>