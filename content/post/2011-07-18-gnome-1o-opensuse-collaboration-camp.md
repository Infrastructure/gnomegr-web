---
author: iosifidis
categories:
  - Ειδήσεις
date: 2011-07-18 21:11:16
guid: http://gnome.gr/?p=976
id: 976
image: /wp-content/uploads/sites/7/2011/07/oScc_poster.jpg
permalink: /2011/07/18/gnome-1o-opensuse-collaboration-camp/
title: Παρουσία GNOME στο 1o openSUSE collaboration weekend camp (Κατερίνη 15-17 Ιουλίου)
url: /2011/07/18/gnome-1o-opensuse-collaboration-camp/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/oScc_poster-300x211.jpg" alt="1ο openSUSE collaboration weekend camp" title="" class="size-medium wp-image-979 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/oScc_poster.jpg) 

Στις 15-17 Ιουλίου 2011 πραγματοποιήθηκε στην Κατερίνη το <a href="http://os-el.gr/content/opensuse-collaboration-weekend-camp" target="1">1ο openSUSE collaboration weekend camp</a>.

Από πλευράς οργάνωσης ήταν ότι καλύτερο για τους συμμετέχοντες γιατί εκτός της εκπαίδευσης με διάφορες ομιλίες και workshop (<a href="http://os-el.gr/content/%CF%80%CF%81%CF%8C%CE%B3%CF%81%CE%B1%CE%BC%CE%BC%CE%B1-1%CE%BF%CF%85-opensuse-collaboration-weekend-camp" target="1">δείτε πρόγραμμα</a>), ακολούθησαν ενδιαφέρουσες συζητήσεις σχετικά με το ΕΛ/ΛΑΚ κυρίως παίζοντας στην πισίνα.

Η κοινότητα GNOME ήταν παρούσα με μια ομιλία του <a href="http://stathisuse.blogspot.com/" target="1">Ευστάθιου Ιωσηφίδη</a> με τίτλο «Επιβίωση με Gnome 3 στον κόσμο του KDE». Η ομιλία επικεντρώθηκε στην χρήση του GΝΟΜΕ 3. Όπως πιθανό να γνωρίζετε, το KDE είναι το περιβάλλον που χρησιμοποιούν οι περισσότεροι χρήστες της διανομής openSUSE. Αρκετά άτομα έδειξαν ενδιαφέρον με ποικίλες ερωτήσεις.

**Φωτογραφίες:**

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/gnome_camp-300x225.jpg" alt="Ευστάθιος Ιωσηφίδης @ 1o openSUSE collaboration weekend camp" title="" class="size-medium wp-image-977 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/gnome_camp.jpg) [<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/pool-300x225.jpg" alt="1ο openSUSE collaboration weekend camp" title="" class="size-medium wp-image-978 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/pool.jpg)
