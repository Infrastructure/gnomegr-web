---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/8313064700707498951
blogger_permalink:
  - /2013/04/ubuntu-gnome-1304.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-04-26 23:56:00
guid: http://gnome.gr/2013/04/26/ubuntu-gnome-13-04/
id: 2920
permalink: /2013/04/26/ubuntu-gnome-13-04/
title: Ubuntu Gnome 13.04
url: /2013/04/26/ubuntu-gnome-13-04/
---

Η ομάδα GNOME του Ubuntu σας ανακοινώνει την πρώτη έκδοση ως επίσημο παράγωγο του Ubuntu, την Ubuntu GNOME 13.04.

Ο στόχος του Ubuntu GNOME είναι να προσφέρει μια καθαρή εμπειρία χρήσης του GNOME στο Ubuntu. Σε συνεργασία με την ομάδα ανάπτυξης του Ubuntu, αποφασίσαμε να μείνουμε στην έκδοση GNOME 3.6 για την έκδοση 13.04. Δείτε τις <a href="http://library.gnome.org/misc/release-notes/3.6/" target="1">σημειώσεις της έκδοσης GNOME 3.6</a> για πληροφορίες σχετικά με τα χαρακτηριστικά του GNOME 3.6.

**Από που θα το προμηθευτείτε&#8230;**

Την τελευταία έκδοση μπορείτε να την κατεβάσετε από:   
<a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.04/release/" target="1">http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.04/release/ </a>

**Τι νέο υπάρχει;**

Ο Firefox έχει αντικαταστήσει το GNOME Web (Epiphany) ως προεπιλεγμένο φυλλομετρητή.  
Το Κέντρο λογισμικού Ubuntu (Ubuntu Software Center) και ο διαχειριστής ενημερώσεων αντικατέστησαν το GNOME Software (gnome-packagekit).  
Το LibreOffice 4.0 είναι διαθέσιμο ως προεπιλογή, αντί του Abiword και του Gnumeric.

Επιπλέον πληροφορίες βρίσκονται στις <a href="https://wiki.ubuntu.com/RaringRingtail/ReleaseNotes" target="1">σημειώσεις έκδοσης Ubuntu</a>.

Για εσάς που θέλετε να έχετε την τελευταία έκδοση του GNOME, συντηρούμε τα <a href="https://launchpad.net/~gnome3-team/+archive/gnome3" target="1">GNOME3 PPA</a> ώστε να έχετε μια γεύση του GNOME 3.8. Σιγουρευτείτε ότι θα εκτελέσετε dist-upgrade εάν επιλέξετε να προσθέσετε αυτό το PPA και όχι μόνο απλή αναβάθμιση διότι το GDM και το GNOME shell δεν θα εκκινήσουν. Ως σηνήθως, εάν επιλέξετε κάποιο PPA, να γνωρίζετε την χρήση του <a href="http://manpages.ubuntu.com/manpages/raring/man1/ppa-purge.1.html" target="1">ppa-purge</a>.

**Επικοινωνία**

Θα θέλαμε να επικοινωνήσετε μαζί μας και να μας πείτε την γνώμη σας για αυτή την επίσημη πρώτη έκδοση. Δείτε την σελίδα της κοινότητας και πως να επικοινωνήσετε μαζί μας.