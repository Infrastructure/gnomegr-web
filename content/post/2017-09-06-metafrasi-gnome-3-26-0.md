---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2017-09-06 12:29:59
guid: https://www.gnome.gr/?p=3686
id: 3686
permalink: /2017/09/06/metafrasi-gnome-3-26-0/
tags:
  - gnome
  - ΕΛ/ΛΑΚ
  - μεταφράσεις
  - μετάφραση
title: Μετάφραση της έκδοσης GNOME 3.26.0
url: /2017/09/06/metafrasi-gnome-3-26-0/
---

<img class="size-full wp-image-3629 aligncenter img-responsive " src="https://static.gnome.org/gnome-gr/uploads/sites/7/2017/02/gnomegr_l10n_banner.png" alt="gnomegr l10n banner" />

Ίσως είναι λίγο αργά για την ανακοίνωση μετάφρασης της έκδοσης 3.26.0. Πέρασε το καλοκαίρι και οι διακοπές για τους περισσότερους, οπότε ήρθε η ώρα για να μεταφράσουμε τη επερχόμενη έκδοση.

Σύμφωνα με την <a href="https://wiki.gnome.org/ThreePointTwentyfive" target="_blank">σελίδα του προγράμματος κυκλοφορίας</a>, η κυκλοφορία της έκδοσης 3.26.0 θα είναι η 11η Σεπτεμβρίου. Επειδή είναι πολύ κοντά, καλό θα είναι να επικεντρωθούμε 3.26.1, που σύμφωνα με το πρόγραμμα, θα κυκλοφορήσει στις 4 Οκτωβρίου 2017.

Στη σελίδα <a href="https://l10n.gnome.org/teams/el/" target="_blank">https://l10n.gnome.org/teams/el/</a>, μεταφράζουμε την έκδοση **«GNOME 3.26 (σε ανάπτυξη)»** και για αρχή θα επικεντρωθούμε στα μηνύματα της **«Διεπαφής χρήστη»** και στη συνέχεια στην **«Τεκμηρίωση»**.

Καλοσωρίζουμε τους φίλους μας από την κοινότητα Ubuntu που θα θελήσουν να μεταφράσουν. Αν και η έκδοση του Ubuntu που θα κυκλοφορήσει τον επόμενη μήνα θα χρησιμοποιεί GNOME 3.24.2, καλό θα ήταν να βοηθήσουν, όσοι θέλουν, στη νέα έκδοση.

### Είμαι νέος στην κοινότητα, πως μπορώ να βοηθήσω στη μετάφραση του έργου;

Η ελληνική κοινότητα του GNOME, δέχεται με χαρά οποιαδήποτε βοήθεια στο έργο της. Αν θέλετε να βοηθήσετε με τη μετάφραση του GNOME και δεν γνωρίζετε πως, σας προσκαλούμε να επικοινωνήσετε μαζί μας στη <a href="https://mail.gnome.org/mailman/listinfo/gnome-el-list" target="_blank">λίστα αλληλογραφίας</a> ή στα [μέσα κοινωνικής δικτύωσης](https://www.gnome.gr/#social_media) για νας καθοδηγήσουμε.

Στη σελίδα [«Συμμετοχή»](https://www.gnome.gr/contribute/) θα βρείτε οδηγίες για το πως να εγγραφείτε στην πλατφόρμα μεταφράσεων του GNOME, ποια είναι τα βήματα που ακολουθούμε σε κάθε μετάφραση καθώς επίσης και χρήσιμες πληροφορίες σχετικά με τα εργαλεία που χρησιμοποιούμε στην ελληνική κοινότητα του GNOME.

**Ας ξεκινήσουμε λοιπόν τις μεταφράσεις!**
