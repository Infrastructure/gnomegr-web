---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2008-11-22 05:24:05
guid: http://gnome.gr/?p=34
id: 34
permalink: /2008/11/22/%ce%bf%ce%b4%ce%b7%ce%b3%cf%8c%cf%82-%cf%87%cf%81%ce%ae%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-%cf%83%cf%84%ce%b1-%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%ce%ac/
tags:
  - documentation
  - gnome
title: Οδηγός χρήσης του GNOME στα ελληνικά
url: /2008/11/22/%ce%bf%ce%b4%ce%b7%ce%b3%cf%8c%cf%82-%cf%87%cf%81%ce%ae%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-%cf%83%cf%84%ce%b1-%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%ce%ac/
---

Ο <a href="http://dtypaldos.wordpress.com/" target="_blank">Δημήτρης Τυπάλδος</a> μετέφρασε <a href="http://library.gnome.org/users/user-guide/stable/index.html.en" target="_blank">τον οδηγό χρήσης του GNOME</a> στα ελληνικά.

Ο οδηγός είναι διαθέσιμος σε μορφή HTML από το <a href="http://library.gnome.org/users/user-guide/stable/index.html.el" target="_blank">http://library.gnome.org/users/user-guide/stable/index.html.el</a>

Ακόμα, ο ίδιος οδηγός είναι <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2008/11/user-guide-el.pdf" target="_blank">διαθέσιμος και σε μορφή PDF (1,6MB, 163 σελίδες)</a>.

<img class="alignnone img-responsive " src="http://simos.info/pub/GNOME-User-guide-1.png" alt="Δείγμα της τεκμηρίωσης, μορφή PDF" />

Παρακαλούμε διαδώστε όσο πιο πολύ μπορείτε τα κείμενα αυτά.

<img class="alignnone img-responsive " src="http://simos.info/pub/GNOME-User-guide-2.png" alt="Δείγμα της τεκμηρίωσης, μορφή PDF" />

Αν υπάρχουν διορθώσεις/σχόλια στην μετάφραση, μπορείτε να τα κάνετε στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα team@gnome.gr</a>.