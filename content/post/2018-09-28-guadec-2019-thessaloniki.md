---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
  - Εκδηλώσεις
date: 2018-09-28 19:43:16
guid: https://www.gnome.gr/?p=3719
id: 3719
permalink: /2018/09/28/guadec-2019-thessaloniki/
tags:
  - events
  - gnome
  - GUADEC
  - GUADEC 2019
  - συνέδριο
title: Το συνέδριο GUADEC 2019 θα διεξαχθεί στη Θεσσαλονίκη
url: /2018/09/28/guadec-2019-thessaloniki/
---

Το Ίδρυμα GNOME με ενθουσιασμό σας ανακοινώνει ότι το GUADEC 2019 θα πραγματοποιηθεί από τις 23 έως τις 28 Αυγούστου στη Θεσσαλονίκη. Το GUADEC, είναι το ευρωπαϊκό συνέδριο χρηστών και προγραμματιστών του GNOME και συγκεντρώνει εκατοντάδες χρήστες και προγραμματιστές κάθε χρόνο για να προωθήσει το έργο GNOME. Είναι ένα από τα πιο μακροχρόνια και αξιοσημείωτα γεγονότα.

Η Θεσσαλονίκη είναι η δεύτερη μεγαλύτερη πόλη στην Ελλάδα και έχει πάνω της περισσότερα από 2300 χρόνια ιστορία. Βρίσκεται στη Βόρεια Ελλάδα και έχει υπέροχη προκυμαία και χώρους δίπλα στη θάλασσα όπου ο επισκέπτης μπορεί να απολαύσει την βόλτα του. Η Θεσσαλονίκη αποτελεί πολιτιστικό κέντρο γαστρονομίας αφού φιλοξενεί μια μεγάλη γκάμα από μοντέρνα μπαρ και εστιατόρια, βραβευμένα παγκοσμίως. Θα βρείτε τέτοια μέρη τόσο στην Άνω Πόλη όσο και κατά μήκος της παραλίας αλλά και στους πεζόδρομους.

Το Ίδρυμα GNOME αναμένει με χαρά την φιλοξενία του GUADEC 2019 στη Θεσσαλονίκη και έχει ξεκινήσει μια ιστοσελίδα όπου οι ενδιαφερόμενοι μπορούν να βρουν περισσότερες πληροφορίες: <a href="https://2019.guadec.org/" target="_blank" rel="noopener">guadec.org</a>. Για να είστε ενημερωμένοι με τις τελευταίες προθεσμίες και ημερομηνίες εγγραφής, νέα, δραστηριότητες και πολλά άλλα, ακολουθήστε στο Twitter <a href="https://twitter.com/guadec" target="_blank" rel="noopener">@GUADEC</a> και <a href="https://twitter.com/gnome" target="_blank" rel="noopener">@GNOME</a>.

Είτε επιθυμείτε να παρουσιάσετε την εργασία σας, να συμμετάσχετε στα εργαστήρια ή απλά να παρακολουθήσετε τις παρουσιάσεις, εμείς στο Ίδρυμα GNOME ελπίζουμε να σας δούμε εκεί!


<img class="size-full wp-image-3723 img-responsive img-responsive " src="./uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki.jpg" alt="Thessaloniki from Aristotle University CC-BY-3.0 Nikolaos Oikonomou" srcset="https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki.jpg 1024w, https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki-300x56.jpg 300w, https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki-768x143.jpg 768w, https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki-940x174.jpg 940w, https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki-210x39.jpg 210w, https://static.gnome.org/gnome-gr/uploads/sites/7/2018/09/1024px-Panorama_of_Thessaloniki-120x22.jpg 120w" sizes="(max-width: 1024px) 100vw, 1024px" /> 

Πηγή: <a href="https://www.gnome.org/" rel="noopener" target="_blank">https://www.gnome.org/</a>
