---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
  - Εικόνες/Στιγμιότυπα
  - Παρουσίαση
date: 2011-05-09 17:17:26
guid: http://gnome.gr/?p=887
id: 887
permalink: /2011/05/09/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-gnome-%cf%83%cf%84%ce%b7-fosscomm-2011-%cf%80%ce%ac%cf%84%cf%81%ce%b1/
title: Παρουσίαση GNOME στη FOSSCOMM 2011 (Πάτρα)
url: /2011/05/09/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-gnome-%cf%83%cf%84%ce%b7-fosscomm-2011-%cf%80%ce%ac%cf%84%cf%81%ce%b1/
---

![http://patras.fosscomm.gr/wp-content/themes/undedicated/images/logo3.png](http://patras.fosscomm.gr/wp-content/themes/undedicated/images/logo3.png)

Το Σαββατοκύριακο που πέρασε, έγινε <a title="FOSSCOMM 2011" href="http://patras.fosscomm.gr/" target="_blank">το συνέδριο κοινότητων ΕΛ/ΛΑΚ FOSSCOMM στην Πάτρα</a>.

Το GNOME, και <a title="Ελληνική κοινότητα GNOME" href="https://static.gnome.org/gnome-gr" target="_blank">η ελληνική κοινότητα GNOME.gr</a> ήταν εκεί με παρουσίαση και stand.

Και τα δύο τα οργάνωσε <a title="Προσωπικό ιστολόγιο του Στάθη Ιωσηφίδη" href="http://stathisuse.blogspot.com/" target="_blank">ο Στάθης Ιωσηφίδης</a> και έκανε εξαιρετική δουλειά.

<a title="Παρουσίαση GNOME στη FOSSCOMM 2011" href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/Gnome_FOSSCOMM2011.pdf" target="_blank"><img class="size-medium wp-image-879 img-responsive " title="FOSSCOMM ΠαρουσίασηGNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM-ΠαρουσίασηGNOME-300x225.png" alt="FOSSCOMM ΠαρουσίασηGNOME" /></a> 

[Λήψη της παρουσίασης GNOME στο FOSSCOMM2011](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/Gnome_FOSSCOMM2011.pdf) (PDF, 4.2MB)

Για τη δημιουργία της παρουσίασης βοήθησαν (κατά τις <a title="Google Docs παρουσίαση GNOME" href="https://docs.google.com/present/edit?id=0AYiYkQwi5jaZZGZzemhrODVfMzRkcnhwczVjYw&hl=en" target="_blank">καταγραφές στο Google Docs</a>) οι

  * Ευστάθιος Ιωσηφίδης
  * Εμμανουήλ Καπέρναρος
  * Γιάννης Κατσαμπίρης
  * Σίμος Ξενιτέλλης
  * Δημοσθένης Παπαδόπουλος
  * Στέργιος Προσινικλής

[<img class="size-medium wp-image-889  img-responsive " title="Στάθης Ιωσηφίδης στο stand του GNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM_GNOME_Stand_Stathis-224x300.jpg" alt="Στάθης Ιωσηφίδης στο stand του GNOME" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/FOSSCOMM_GNOME_Stand_Stathis.jpg) 

Ο Στάθης μοίρασε πενήντα DVD με το GNOME 3 και φυλλάδια για την ελληνική κοινότητα GNOME.gr.

[<img class="size-medium wp-image-890 img-responsive " title="Κονκάρδες GNOME" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/GNOME_buttons_photo-300x223.png" alt="Κονκάρδες GNOME" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/05/GNOME_buttons_photo.png) 

Από τις κονκάρδες του GNOME που έφτιαξε ο Στάθης. Ελπίζουμε να προλάβατε να πάρετε τη δική σας.

Θα θέλαμε να ευχαριστήσουμε το <a title="Βασίλειος Γεωργιτζίκης" href="http://blog.tzikis.com/" target="_blank"><em>Βασίλειο Γεωργιτζίκη</em></a> από πλευράς FOSSCOMM για τη βοήθεια που προσέφερε_.
  
_
