---
author: iosifidis
blogger_author:
  - Elias Psallidas
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/2117377032275353458
blogger_permalink:
  - /2014/11/gnome-foundation-groupon-trademark-gnome.html
categories:
  - Ειδήσεις
date: 2014-11-11 23:27:00
guid: http://gnome.gr/2014/11/11/gnome-foundation-%ce%b5%ce%bd%ce%b1%ce%bd%cf%84%ce%af%ce%bf%ce%bd-groupon-%cf%84%ce%bf-trademark-gnome/
id: 2891
image: /wp-content/uploads/sites/7/2014/11/GnomeLogoHorizontal.png
permalink: /2014/11/11/gnome-foundation-%ce%b5%ce%bd%ce%b1%ce%bd%cf%84%ce%af%ce%bf%ce%bd-groupon-%cf%84%ce%bf-trademark-gnome/
title: GNOME Foundation εναντίον Groupon – το trademark «GNOME»
url: /2014/11/11/gnome-foundation-%ce%b5%ce%bd%ce%b1%ce%bd%cf%84%ce%af%ce%bf%ce%bd-groupon-%cf%84%ce%bf-trademark-gnome/
---

<div dir="ltr" style="text-align: left">
  Αργά η γρήγορα όλοι όσοι έρχονται στον κόσμο του ελεύθερου λογισμικού θα συναντήσουν το όνομα <b>GNOME</b>. Το όνομα είναι γνωστό σε αυτόν τον κόσμο επειδή υπάρχει για 17 χρόνια στην κοινότητα του Ελεύθερου Λογισμικού / Λογισμικού Ανοιχτού Κώδικα. Πρόκειται για το GNOME Project, ένα πλήρες περιβάλλον εργασίας για διανομές GNU/Linux και συστήματα BSD. Ήταν κάποτε (και εν μέρει ακόμα είναι) το προεπιλεγμένο περιβάλλον εργασίας του Ubuntu, και συνεχίζει να είναι το προεπιλεγμένο περιβάλλον εργασίας για αρκετές διανομές &nbsp;GNU/Linux, με χαρακτηριστικά παραδείγματα αυτά των Debian και Fedora. Οι τεχνολογίες του GNOME συναντώνται επίσης σε τηλεοράσεις, tablets, τηλέφωνα και σε λογισμικό διάφορων συσκευών.</p> 
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/GnomeLogoHorizontal.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/GnomeLogoHorizontal-300x107.png"  class="img-responsive" /></a>
  </div>
  
  <div style="clear: both;text-align: center">
  </div>
  
  <h2 style="clear: both;text-align: center">
  </h2>
  
  <h2 style="clear: both;text-align: center">
    Η έκκληση του GNOME Foundation&nbsp;
  </h2>
  
  <p>
    Πρόσφατα, η εταιρία <a href="http://en.wikipedia.org/wiki/Groupon" target="_blank">Groupon</a> ανακοίνωσε την κυκλοφορία ενός προϊόντος με την ονομασία «Gnome» και πρόκειται για ένα <a href="http://gnome.groupon.com/" target="_blank">λειτουργικό σύστημα για tablets, κλειστού κώδικα</a>. Είναι δύσκολο για την κοινότητα του GNOME να πιστέψει πως μία εταιρία σαν την Groupon με ετήσιο εισόδημα που ξεπερνά τα 2,5 δισεκατομμύρια δολάρια και συνεπώς έχει ένα ολοκληρωμένο και ικανό νομικό τμήμα και ένα μεγάλο τμήμα μηχανικών, να μην έχει τύχει να ακούσουν ξανά το όνομα GNOME ή έστω μετά από έρευνα να μην ανακάλυψαν πως αυτό το εμπορικό σήμα υπάρχει ήδη, από το διαδίκτυο, την ιστοσελίδα και το λογισμικό του GNOME Project. Παρόλα αυτά, το GNOME Foundation επικοινώνησε με την Groupon και ζήτησε να βρουν άλλο όνομα για το προϊόν τους. Όχι μόνο η Groupon αρνήθηκε, αλλά έχει υποβάλει περισσότερες αιτήσεις για την κατοχύρωση του trademark (που μπορείτε να δείτε <a href="http://tsdr.uspto.gov/#caseNumber=86200190%0A86200193%0A86200194%0A86200196%0A86200657%0A86200661%0A86200759%0A86200763%0A86200765%0A86227618%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a>, <a href="http://tsdr.uspto.gov/#caseNumber=86287930%0A86287935%0A86287938%0A86287940%0A86287946%0A86287951%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a> και <a href="http://tsdr.uspto.gov/#caseNumber=86441913%0A86441922%0A86441923%0A86441925%0A86441926%0A86441930%0A86441933%0A86441934%0A86441937%0A86441941%0A86441945%0A86441951%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a>). Η χρήση του ονόματος GNOME για ιδιόκτητο λογισμικό έρχεται σε αντίθεση με τις θεμελιώδεις αρχές και ιδέες της κοινότητας του GNOME, της κοινότητας του ελεύθερου λογισμικού και του GNU Project και είναι απαράδεκτη.<br />Παρακαλούμε βοηθήστε το GNOME Foundation σε αυτή τη μάχη εναντίον αυτής της τεράστιας εταιρίας.
  </p>
  
  <p>
    Το GNOME Foundation θέλει να δείξει πως το σήμα του έχει σημασία. Από τις 28 αιτήσεις για το trademark που κατέθεσε η Groupon, το GNOME Foundation πρέπει να εκκινήσει την επίσημη διαδικασία για να αντιταχθεί σε 10 από αυτές, μέχρι τις <b>3 Δεκεμβρίου του 2014</b>. Ζητάει τη βοήθεια της κοινότητας για την συγκέντρωση κεφαλαίου ώστε να μπορέσει να αντισταθεί σε αυτή την πράξη της Groupon, καθώς και για την ενημέρωση του κόσμου. <b><i><u>Θέλει να εξασφαλίσει πως το όνομα GNOME συνδέεται με την ελευθερία και όχι με ιδιόκτητο λογισμικό.</u></i></b> Το GNOME Foundation μας ενημερώνει πως χρειάζονται <b>80.000 δολάρια</b> για να μπορέσει να αντιταχθεί στις πρώτες 10 αιτήσεις της Groupon για το όνομα GNOME και πως αν καταφέρει να υπερασπιστεί το σήμα του χωρίς να ξοδέψει όλο αυτό το ποσό, θα αξιοποιήσει ότι απομείνει για την βελτίωση του GNOME και ζητά τη βοήθειά μας για να υπερασπιστεί το όνομα GNOME που φέρει μεγάλη ιστορία στο ελεύθερο λογισμικό.
  </p>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/DefendGNOME.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/DefendGNOME-300x111.png"  class="img-responsive" /></a>
  </div>
  
  <p>
    Δωρεές μπορούν να γίνουν μέσω αυτού του <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LKMQSMA5WPZQL" target="_blank">συνδέσμου</a> (αν και καλό θα ήταν να γίνει χρήση του πράσινου κουμπιού σε <a href="https://gnome.org/groupon/" target="_blank">αυτή τη σελίδα</a>) και μπορείτε να χρησιμοποιήσετε τους παρακάτω συνδέσμους για να διαδοθεί αυτή η υπόθεση και στα μέσα κοινωνικής δικτύωσης:
  </p>
  
  <ul style="text-align: left">
    <li>
      <a href="https://www.facebook.com/sharer.php?u=https://gnome.org/groupon/&t=Help%20the%20GNOME%20Foundation%20defend%20the%20GNOME%20trademark%20against%20Groupon!%20Please%20donate%20at%20https://gnome.org/groupon/" target="_blank">Facebook</a>
    </li>
    <li>
      <a href="https://plus.google.com/share?url=https://gnome.org/groupon/" target="_blank">Google Plus</a>
    </li>
    <li>
      <a href="https://twitter.com/intent/tweet?text=Help%20the%20GNOME%20Foundation%20%28@gnome%29%20defend%20the%20GNOME%20trademark%20against%20Groupon!%20To%20%23defendGNOME%20please%20donate%20at&url=https://gnome.org/groupon/&related=gnome" target="_blank">Twitter</a>
    </li>
  </ul>
  
  <div>
    Χρησιμοποιήστε τα hashtags #DefendGNOME και #GNOME προκειμένου να κοινοποιήσετε αυτή την υπόθεση.
  </div>
  
  <h2 style="text-align: center">
  </h2>
  
  <h2 style="text-align: center">
    Η τρομερή ανταπόκριση της κοινότητας του ελεύθερου λογισμικού
  </h2>
  
  <div>
    Η ανακοίνωση του GNOME Foundation σχετικά με το θέμα και η έκκλησή του προς την κοινότητα έγιναν μόλις σήμερα (11/11/2014) και από τη στιγμή της δημοσίευσης της ανακοίνωσης, υπήρξε καταιγισμός δημοσιεύσεων από τα μέλη της κοινότητας του ελεύθερου λογισμικού στο διαδίκτυο.
  </div>
  
  <div>
  </div>
  
  <div style="text-align: left">
    Όχι μόνο το διαδίκτυο γέμισε με άρθρα που εκφράζουν την αντίδρασή τους προς την συμπεριφορά της Groupon, μέσω των μέσων κοινωνικής δικτύωσης αλλά και των ιστοσελίδων που αφορούν το ελεύθερο λογισμικό, αλλά μέσα σε αυτό το σύντομο χρονικό διάστημα, το GNOME Foundation κατάφερε να συγκεντρώσει ένα αρκετά υψηλό ποσοστό του κεφαλαίου που χρειάζεται. Πιο συγκεκριμένα, μέχρι τη στιγμή της σύνταξης του παρόντος άρθρου, οι δωρεές προς το GNOME Foundation ώστε να αντιταχθεί σε αυτή την πράξη της Groupon, αγγίζουν συνολικά τα: <b><a href="https://gnome.org/groupon/" target="_blank">68.627 $</a></b></p> 
    
    <div style="clear: both;text-align: center">
      <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/guadec.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/guadec-300x143.png"  class="img-responsive" /></a>
    </div>
    
    <p>
      Η ανταπόκριση της Groupon ήταν σχεδόν άμεση μετά τα σημερινά γεγονότα, <a href="https://www.groupon.com/blog/cities/gnome-update" target="_blank">ανακοινώνοντας</a>:
    </p>
    
    <blockquote>
      <p>
        <i><span style="color: #999999">Η Groupon υποστηρίζει ισχυρά και συνεχώς την κοινότητα του ελεύθερου λογισμικού και οι προγραμματιστές μας είναι ενεργά μέλη και συνεισφέρουν σε διάφορα έργα ανοιχτού κώδικα. Επικοινωνούμε με το GNOME Foundation μήνες τώρα ώστε να προσπαθήσουμε να βρούμε μία κοινή γραμμή πλεύσης, συμπεριλαμβάνοντας κάποια άλλα ονόματα και είμαστε χαρούμενοι να συνεχίσουμε αυτές τις συζητήσεις. Η σχέση μας με την κοινότητα του ελεύθερου λογισμικού είναι πιο σημαντική για εμάς από το όνομα ενός προϊόντος. Και αν δεν μπορέσουμε να βρούμε μία λύση που να μας ικανοποιεί όλους, θα ήμασταν χαρούμενοι να ψάξουμε να βρούμε άλλο όνομα.</span></i>
      </p>
    </blockquote>
    
    <blockquote>
      <p>
        <i><span style="color: #999999"><b style="background-color: white">Ενημέρωση: Μετά από συζητήσεις με την κοινότητα του ελεύθερου λογισμικού και το GNOME Foundation αποφασίσαμε να αποσύρουμε την αίτησή μας για το όνομα «Gnome». Η εταιρία μας θα προχωρήσει με διαφορετικό όνομα για το προϊόν μας.&nbsp;</b></span></i>
      </p>
    </blockquote>
    
    <blockquote>
    </blockquote>
    
    <p>
      Αρκετοί Leaders έργων ελεύθερου λογισμικού μεταξύ άλλων, πήραν θέση στο θέμα.
    </p>
    
    <p>
      <i>— Lucas Nussbaum, Debian Project Leader:</i>
    </p>
    
    <blockquote>
      <p>
        <span style="color: #999999"><i>Αυτή η δικαστική διαμάχη δεν αφορά την προστασία του σήματος του GNOME, αλλά πρόκειται για διευκρίνηση στον εταιρικό κόσμο πως τα εμπορικά σήματα του Ελεύθερου Λογισμικού μπορούν και θα πρέπει να φυλάσσονται. Όχι μόνο από το εν λόγω έργο, αλλά από ολόκληρη την κοινότητα. Ως αποτέλεσμα όλα τα εμπορικά σήματα ελεύθερου λογισμικού θα γίνουν πιο ισχυρά ταυτόχρονα.&nbsp;</i></span>
      </p>
    </blockquote>
    
    <p>
      <i>— Matthew Miller, Fedora Project Leader:</i>
    </p>
    
    <blockquote>
      <p>
        <span style="color: #999999"><i>Το GNOME είναι ένα αναπόσπαστο μέρος του σύμπαντος του Ελεύθερου Λογισμικού / Λογισμικού Ανοιχτού Κώδικα για πάνω από μιάμιση δεκαετία και είναι ένα ζωτικής σημασίας συστατικό του Fedora. Είμαι σοκαρισμένος και θλίβομαι που μία εταιρία η οποία περιγράφει τον εαυτό της ως «ενισχυμένη από ανοιχτό λογισμικό» θα σκεπτόταν να λερώσει τα νερά κυκλοφορώντας ιδιόκτητο λογισμικό με το ίδιο όνομα. Αυτό επιφέρει σύγχυση, καταστροφή και ασέβεια. Παρακαλώ ελάτε και εσείς να ζητήσετε από τη Groupon να αλλάξει τα σχέδιά της και να συνεισφέρουμε στην αμυντική γραμμή του GNOME Foundation.</i></span>
      </p>
    </blockquote>
    
    <p>
      <i>— Andrew Lee, Private Internet Access co-founder and Chief Content Officer:</i>
    </p>
    
    <blockquote>
      <p>
        <span style="color: #999999"><i>Το GNOME είναι ένα από τα ορόσημα και θεμέλια της κοινότητας του ανοιχτού λογισμικού για πάνω από 15 χρόνια. Είμαστε εξοργισμένοι που η Groupon επιδιώκει να το υπονομεύσει αυτό, καθώς και τα χρόνια εμπιστοσύνης και καλής θέλησης που έχει χτίσει το GNOME με τον καιρό, με ιδιόκτητο λογισμικό και προϊόντα που δεν έχουν καμία σχέση με την κοινότητα του GNOME. Στεκόμαστε δίπλα στο GNOME στην προστασία του σήματός του και παροτρύνουμε τους πάντες στην κοινότητα του ΕΛ/ΛΑΚ να κάνουν το ίδιο.</i></span>
      </p>
    </blockquote>
    
    <p>
    </p>
    
    <h2 style="text-align: center">
      Ενημέρωση&nbsp;
    </h2>
    
    <div>
      Επειδή και εμείς ως μέλη της κοινότητας του Ubuntu, του GNOME και του ελεύθερου λογισμικού γενικότερα, θεωρούμε πως αυτή η παραβίαση του trademark του GNOME έχει εξαιρετικά αρνητικές επιπτώσεις στην κοινότητα του ελεύθερου λογισμικού, θα παρακολουθούμε την πορεία της υπόθεσης και θα σας κρατάμε ενήμερους για περαιτέρω εξελίξεις .</p>
    </div>
    
    <p>
      Ελπίζουμε πως η Groupon θα τηρήσει όσα γράφει στην ανακοίνωσή της και πως θα προχωρήσει με διαφορετική ονομασία για το προϊόν της.
    </p>
    
    <h2 style="text-align: center">
      <b><span style="color: red">#DefendGNOME</span></b>
    </h2>
    
    <div>
      <b><span style="color: red"><br /></span></b>
    </div>
    
    <h2 style="text-align: center">
      <a href="http://www.gnome.org/news/2014/11/groupon-has-agreed-to-change-its-product-name/" target="_blank">Ανακοίνωση</a> από το GNOME Foundation
    </h2>
    
    <div>
      Ευχαριστούμε πολύ για <a href="http://gnome.org/groupon/" target="_blank">τις δωρεές και τη βοήθεια</a>&nbsp;στο να διαδοθεί η ιστορία. Είμαστε συγκλονισμένοι και αναζωογονημένοι από την υποστήριξη που λάβαμε από όλους, που είχε ως αποτέλεσμα την ακόλουθη ανακοίνωση από τη Groupon:
    </div>
    
    <blockquote>
      <p>
        <i style="background-color: white"><span style="color: #999999">«Η Groupon συμφώνησε να αλλάξει το όνομα του προϊόντος της για να επιλυθούν οι ανησυχίες του GNOME Foundation. Η Groupon εγκαταλείπει και τις 28 τρέχουσες αιτήσεις για το trademark. Οι εμπλεκόμενοι συνεργάζονται για να βρεθεί μία κοινώς αποδεκτή λύση, μία διαδικασία που έχει ήδη αρχίσει.»</span></i>
      </p>
    </blockquote>
    
    <div>
      Υπάρχει κάτι καταπληκτικό στο ελεύθερο λογισμικό – πρόκειται για τεχνολογία με ηθικές αξίες, αλλά δημιουργεί και μία φανταστική κοινότητα ανθρώπων που είναι διαθέσιμη να πολεμήσει για το σωστό. Χρειάστηκε μία μεγάλη ποσότητα χρόνου και προσπάθειας για να αντιμετωπίσουμε αυτό το πρόβλημα τους μήνες πριν την σημερινή ανακοίνωση και θα μας πάρει λίγο διάστημα μέχρι να αναδιοργανωθούμε. Θα σας κρατάμε ενήμερους καθώς το θέμα επιλυθεί εντελώς.
    </div>
  </div>
  
  <div style="text-align: left">
    <div style="text-align: center">
    </div>
  </div>
  
  <div>
  </div>
</div>
