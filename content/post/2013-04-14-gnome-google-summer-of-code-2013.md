---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2013-04-14 16:08:13
guid: http://gnome.gr/?p=2034
id: 2034
image: /wp-content/uploads/sites/7/2013/04/gsoc_banner_2013.jpg
permalink: /2013/04/14/gnome-google-summer-of-code-2013/
tags:
  - Google
  - Google Summer of Code
  - GSoC
  - GSoC 2013
title: Συμμετοχή GNOME στο Google Summer of Code 2013
url: /2013/04/14/gnome-google-summer-of-code-2013/
---

[<img class="aligncenter size-medium wp-image-2035 img-responsive " alt="Google Summer of Code 2013" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/04/gsoc_banner_2013-300x159.jpg" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/04/gsoc_banner_2013.jpg)

Το <a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2013" target="_blank">Google Summer of Code (GSoC)</a> είναι ένα πρόγραμμα της Google που απευθύνεται σε σπουδαστές-φοιτητές. Ο κάθε συμμετέχοντας, επιλέγει σε ποιο project θέλει να ασχοληθεί, επιλέγει θέμα (μαζί με μέντορα) και κάνει αίτηση. Εφόσον επιλεχθεί, έχει διάστημα 3 μηνών να φέρει σε πέρας το έργο που του ανατέθηκε. Βέβαια πάντα με βοήθεια από τον μέντορα, ο οποίος δίνει την τελική έγκριση για την πρόοδο του φοιτητή.

**Γιατί να συμμετέχει κάποιος φοιτητής αλλά και κάποιος οργανισμός;**
  
Η απάντηση είναι απλή. Καταρχήν ο φοιτητής δημιουργεί ανοικτό κώδικα για το project που μπορεί να αποτελέσει αρχή-ανάπτυξη για κάποιο άλλο project. Μέσα από αυτή τη διαδικασία, ωθεί νέους προγραμματιστές να συμμετέχουν στην ανάπτυξη projects ανοικτού κώδικα αλλά βοηθάει και τους οργανισμούς να προσελκύσουν νέους προγραμματιστές με φρέσκες ιδέες. Ο φοιτητής παίρνει μια ιδέα για τον τρόπο εργασίας μεγάλων οργανισμών αλλά και εκτίθεται σε «σενάρια» ανάπτυξης λογισμικού που δεν διδάσκονται στα πανεπιστήμια. Τέλος, το καλύτερο είναι η χορηγία _5500USD_ από την Google για κάθε φοιτητή, τα οποία _5000USD_ είναι του φοιτητή και τα _500USD_ είναι του οργανισμού.

Το GNOME συμμετέχει και φέτος στο πρόγραμμα GSoC. Δείτε την ιστοσελίδα με τις <a href="https://live.gnome.org/SummerOfCode2013/Ideas" target="_blank">ιδέες για το GSoC</a>.

Η αρχική ιστοσελίδα του GSoC είναι η

<a href="http://www.google-melange.com/gsoc/homepage/google/gsoc2013" target="_blank">http://www.google-melange.com/gsoc/homepage/google/gsoc2013</a>

όπου μπορείτε να κάνετε και τις εγγραφές σας ως φοιτητές.

Οι ημερομηνίες για όσους θέλετε να συμμετάσχετε βρίσκονται στον σύνδεσμο του <a href="http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2013/help_page#2._What_is_the_program_timeline" target="_blank">χρονοδιαγράμματος</a>

_Σημαντικές ημερομηνίες για τους φοιτητές είναι_:

**22 Απριλίου:** Έναρξη αιτήσεων φοιτητών
  
**3 Μαΐου:** Λήξη αιτήσεων φοιτητών
  
**27 Μαΐου:** Ανακοίνωση φοιτητών που έγιναν δεκτοί
  
**17 Ιουνίου:** Έναρξη εργασιών φοιτητών. Πρώτες πληρωμές από την Google
  
**1 – 8 Αυγούστου:** <a href="http://www.guadec.org/" target="_blank">GUADEC 2013</a>
  
**2 Αυγούστου:** Αξιολόγηση
  
**23 Σεπτεμβρίου:** Λήξη προγράμματος

Μην διστάσετε να επικοινωνήσετε μαζί μας στο κανάλι **#soc** στο _GIMPNet_ (irc.gnome.org)!
  
Εάν έχετε ερωτήσεις, δείτε πρώτα την ιστοσελίδα <a href="http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2013/help_page" target="_blank">Frequently Asked Questions</a>.
  
Εάν δεν σας λύθηκαν οι απορίες, στη συνέχεια επικοινωνήστε με τους διαχειριστές του GNOME Google Summer of Code
  
<a href="https://live.gnome.org/ZeeshanAli" target="_blank">ZeeshanAli</a>, <a href="https://live.gnome.org/AlexandreFranke" target="_blank">AlexandreFranke</a>, <a href="https://live.gnome.org/ChristopheFergeau" target="_blank">ChristopheFergeau</a>, <a href="https://live.gnome.org/FabianoFidencio" target="_blank">FabianoFidencio</a>, <a href="https://live.gnome.org/DanielSiegel" target="_blank">DanielSiegel</a> ή <a href="https://live.gnome.org/MarinaZhurakhinskaya" target="_blank">MarinaZhurakhinskaya</a>.
  
Μπορείτε να τους βρείτε, στέλνοντας mail στο soc-admins@gnome.org
