---
author: iosifidis
categories:
  - Ειδήσεις
date: 2014-11-11 16:48:00
guid: http://gnome.gr/?p=2871
id: 2871
permalink: /2014/11/11/gnome-foundation-enantion-groupon-gia-onoma-gnome/
tags:
  - DefendGNOME
  - gnome
  - Groupon
  - ShameOnGroupon
title: GNOME Foundation εναντίον Groupon – για το όνομα «GNOME»
url: /2014/11/11/gnome-foundation-enantion-groupon-gia-onoma-gnome/
---

[<img class="aligncenter size-medium wp-image-2872 img-responsive " src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/gnome-300x112.png" alt="GNOME Logo" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/11/gnome.png)

Αργά η γρήγορα όλοι όσοι έρχονται στον κόσμο του ελεύθερου λογισμικού θα συναντήσουν το όνομα **GNOME**. Το όνομα είναι γνωστό σε αυτόν τον κόσμο επειδή υπάρχει για 17 χρόνια στην κοινότητα του Ελεύθερου Λογισμικού / Λογισμικού Ανοιχτού Κώδικα. Πρόκειται για το _GNOME Project_, ένα πλήρες περιβάλλον εργασίας για διανομές GNU/Linux και συστήματα BSD. Είναι το προεπιλεγμένο περιβάλλον εργασίας του Red Hat Enterprise Linux, καθώς και των Debian και Fedora. Οι τεχνολογίες του GNOME συναντώνται επίσης σε τηλεοράσεις, tablets, τηλέφωνα και σε λογισμικό διάφορων συσκευών.

Πρόσφατα, η εταιρία <a href="http://en.wikipedia.org/wiki/Groupon" target="_blank">Groupon</a> ανακοίνωσε την κυκλοφορία ενός προϊόντος με την ονομασία Gnome και πρόκειται για ένα <a href="http://investor.groupon.com/releasedetail.cfm?releaseid=848707" target="_blank">λειτουργικό σύστημα για tablets</a>, κλειστού κώδικα. Είναι δύσκολο για την κοινότητα του GNOME να πιστέψει πως μία εταιρία σαν την Groupon με ετήσιο εισόδημα που ξεπερνά τα 2,5 δισεκατομμύρια δολάρια και συνεπώς έχει ένα ολοκληρωμένο και ικανό νομικό τμήμα και ένα μεγάλο τμήμα μηχανικών, να μην έχει τύχει να ακούσουν ξανά το όνομα GNOME ή έστω μετά από έρευνα να μην ανακάλυψαν πως αυτό το εμπορικό σήμα υπάρχει ήδη, από το διαδίκτυο, την ιστοσελίδα και το λογισμικό του GNOME Project. Παρόλα αυτά, το GNOME Foundation επικοινώνησε με την Groupon και ζήτησε να βρουν άλλο όνομα για το προϊόν τους. Όχι μόνο η Groupon αρνήθηκε, αλλά έχει υποβάλει περισσότερες αιτήσεις για την κατοχύρωση του trademark (που μπορείτε να δείτε <a href="http://tsdr.uspto.gov/#caseNumber=86287930%0A86287935%0A86287938%0A86287940%0A86287946%0A86287951%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a>, <a href="http://tsdr.uspto.gov/#caseNumber=86200190%0A86200193%0A86200194%0A86200196%0A86200657%0A86200661%0A86200759%0A86200763%0A86200765%0A86227618%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a> και <a href="http://tsdr.uspto.gov/#caseNumber=86441913%0A86441922%0A86441923%0A86441925%0A86441926%0A86441930%0A86441933%0A86441934%0A86441937%0A86441941%0A86441945%0A86441951%0A&caseType=SERIAL_NO&searchType=multiStatus" target="_blank">εδώ</a>). Η χρήση του ονόματος GNOME για ιδιόκτητο λογισμικό έρχεται σε αντίθεση με τις θεμελιώδεις αρχές και ιδέες της κοινότητας του GNOME, της κοινότητας του ελεύθερου λογισμικού και του GNU Project και είναι απαράδεκτη.
  
Παρακαλούμε βοηθήστε το GNOME Foundation σε αυτή τη μάχη εναντίον αυτής της τεράστιας εταιρίας.

Το GNOME Foundation θέλει να δείξει πως το σήμα του έχει σημασία. Από τις 28 αιτήσεις για το trademark που κατέθεσε η Groupon, το GNOME Foundation πρέπει να εκκινήσει την επίσημη διαδικασία για να αντιταχθεί σε 10 από αυτές, μέχρι τις 3 Δεκεμβρίου του 2014. Ζητάει τη βοήθεια της κοινότητας για την συγκέντρωση κεφαλαίου ώστε να μπορέσει να αντισταθεί σε αυτή την πράξη της Groupon, καθώς και για την ενημέρωση του κόσμου. Θέλει να εξασφαλίσει πως το όνομα GNOME συνδέεται με την ελευθερία και όχι με ιδιόκτητο λογισμικό. Το GNOME Foundation μας ενημερώνει πως χρειάζονται 80.000 δολάρια για να μπορέσει να αντιταχθεί στις πρώτες 10 αιτήσεις της Groupon για το όνομα GNOME και πως αν καταφέρει να υπερασπιστεί το σήμα του χωρίς να ξοδέψει όλο αυτό το ποσό, θα αξιοποιήσει ότι απομείνει για την βελτίωση του GNOME και ζητά τη βοήθειά μας για να υπερασπιστεί το όνομα GNOME που φέρει μεγάλη ιστορία στο ελεύθερο λογισμικό.

Δωρεές μπορούν να γίνουν μέσω αυτού του <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LKMQSMA5WPZQL" target="_blank">συνδέσμου</a> (αν και καλό θα ήταν να γίνει χρήση του πράσινου κουμπιού σε <a href="https://gnome.org/groupon/" target="_blank">αυτή τη σελίδα</a>) και μπορείτε να χρησιμοποιήσετε τους παρακάτω συνδέσμους για να διαδοθεί αυτή η υπόθεση και στα μέσα κοινωνικής δικτύωσης:

  * <a href="https://www.facebook.com/sharer.php?u=https://gnome.org/groupon/&t=Help%20the%20GNOME%20Foundation%20defend%20the%20GNOME%20trademark%20against%20Groupon!%20Please%20donate%20at%20https://gnome.org/groupon/" target="_blank">Facebook</a>
  * <a href="https://plus.google.com/share?url=https://gnome.org/groupon/" target="_blank">Google Plus</a>
  * <a href="https://twitter.com/intent/tweet?text=Help%20the%20GNOME%20Foundation%20%28@gnome%29%20defend%20the%20GNOME%20trademark%20against%20Groupon!%20Please%20donate%20at&url=https://gnome.org/groupon/&related=gnome" target="_blank">Twitter</a>

Tags: #DefendGNOME #ShameOnGroupon
