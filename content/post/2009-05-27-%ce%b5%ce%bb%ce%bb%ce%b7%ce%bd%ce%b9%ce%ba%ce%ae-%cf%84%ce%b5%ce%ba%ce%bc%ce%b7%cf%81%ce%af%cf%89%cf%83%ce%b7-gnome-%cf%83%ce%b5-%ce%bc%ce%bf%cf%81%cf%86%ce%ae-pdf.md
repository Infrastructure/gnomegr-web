---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2009-05-27 06:39:52
guid: http://gnome.gr/?p=168
id: 168
permalink: /2009/05/27/%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%ce%ae-%cf%84%ce%b5%ce%ba%ce%bc%ce%b7%cf%81%ce%af%cf%89%cf%83%ce%b7-gnome-%cf%83%ce%b5-%ce%bc%ce%bf%cf%81%cf%86%ce%ae-pdf/
title: Ελληνική τεκμηρίωση GNOME σε μορφή PDF
url: /2009/05/27/%ce%b5%ce%bb%ce%bb%ce%b7%ce%bd%ce%b9%ce%ba%ce%ae-%cf%84%ce%b5%ce%ba%ce%bc%ce%b7%cf%81%ce%af%cf%89%cf%83%ce%b7-gnome-%cf%83%ce%b5-%ce%bc%ce%bf%cf%81%cf%86%ce%ae-pdf/
---

**Ενημέρωση 27 Μαΐου 2009**: Ξαναδημιουργήσαμε τα αρχεία και τα παραθέτουμε παρακάτω.

[Τεκμηρίωση accessx-status (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/accessx-status.pdf)
  
[Τεκμηρίωση accerciser (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_accerciser.pdf)
  
[Τεκμηρίωση anjuta-manuals (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_anjuta-manuals.pdf)
  
[Τεκμηρίωση brasero (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_brasero.pdf)
  
[Τεκμηρίωση cheese (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_cheese.pdf)
  
[Τεκμηρίωση dasher-Data (PDF)](http://gnome.gr/wp-content/uploads/2009/05/documentation_dasher-Data.pdf)
  
[Τεκμηρίωση deskbar-applet (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_deskbar-applet.pdf)
  
[Τεκμηρίωση ekiga (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_ekiga.pdf)
  
[Τεκμηρίωση empathy (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_empathy.pdf)
  
[Τεκμηρίωση eog (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_eog.pdf)
  
[Τεκμηρίωση epiphany (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_epiphany.pdf)
  
[Τεκμηρίωση evince (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_evince.pdf)
  
[Τεκμηρίωση evolution (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_evolution.pdf)
  
[Τεκμηρίωση file-roller (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_file-roller.pdf)
  
[Τεκμηρίωση gcalctool (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gcalctool.pdf)
  
[Τεκμηρίωση gconf-editor-docs (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gconf-editor-docs.pdf)
  
[Τεκμηρίωση gdm-docs (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gdm-docs.pdf)
  
[Τεκμηρίωση gedit (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gedit.pdf)
  
[Τεκμηρίωση glade3 (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_glade3.pdf)
  
[Τεκμηρίωση gnome-applets-battstat (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-battstat.pdf)
  
[Τεκμηρίωση gnome-applets-charpick (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-charpick.pdf)
  
[Τεκμηρίωση gnome-applets-cpufreq (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-cpufreq.pdf)
  
[Τεκμηρίωση gnome-applets-drivemount (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-drivemount.pdf)
  
[Τεκμηρίωση gnome-applets-geyes (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-geyes.pdf)
  
[Τεκμηρίωση gnome-applets-gswitchit (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-gswitchit.pdf)
  
[Τεκμηρίωση gnome-applets-gweather (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-gweather.pdf)
  
[Τεκμηρίωση gnome-applets-invest-applet (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-invest-applet.pdf)
  
[Τεκμηρίωση gnome-applets-mini-commander (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-mini-commander.pdf)
  
[Τεκμηρίωση gnome-applets-mixer (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-mixer.pdf)
  
[Τεκμηρίωση gnome-applets-multiload (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-multiload.pdf)
  
[Τεκμηρίωση gnome-applets-stickynotes (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-stickynotes.pdf)
  
[Τεκμηρίωση gnome-applets-trashapplet (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-applets-trashapplet.pdf)
  
[Τεκμηρίωση gnome-control-center (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-control-center.pdf)
  
[Τεκμηρίωση gnome-desktop-desktop-docs (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-desktop-desktop-docs.pdf)
  
[Τεκμηρίωση gnome-devel-docs-platform-overview (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-devel-docs-platform-overview.pdf)
  
[Τεκμηρίωση gnome-games-aisleriot (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-aisleriot.pdf)
  
[Τεκμηρίωση gnome-games-blackjack (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-blackjack.pdf)
  
[Τεκμηρίωση gnome-games-glchess (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-glchess.pdf)
  
[Τεκμηρίωση gnome-games-glines (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-glines.pdf)
  
[Τεκμηρίωση gnome-games-gnect (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnect.pdf)
  
[Τεκμηρίωση gnome-games-gnibbles (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnibbles.pdf)
  
[Τεκμηρίωση gnome-games-gnobots2 (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnobots2.pdf)
  
[Τεκμηρίωση gnome-games-gnome-sudoku (PDF)](http://gnome.gr/wp-content/uploads/2009/05/documentation_gnome-games-gnome-sudoku.pdf)
  
[Τεκμηρίωση gnome-games-gnometris (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnometris.pdf)
  
[Τεκμηρίωση gnome-games-gnomine (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnomine.pdf)
  
[Τεκμηρίωση gnome-games-gnotravex (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnotravex.pdf)
  
[Τεκμηρίωση gnome-games-gnotski (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gnotski.pdf)
  
[Τεκμηρίωση gnome-games-gtali (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-gtali.pdf)
  
[Τεκμηρίωση gnome-games-iagno (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-iagno.pdf)
  
[Τεκμηρίωση gnome-games-mahjongg (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-mahjongg.pdf)
  
[Τεκμηρίωση gnome-games-same-gnome (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-games-same-gnome.pdf)
  
[Τεκμηρίωση gnome-media-gnome-cd (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-media-gnome-cd.pdf)
  
[Τεκμηρίωση gnome-media-grecord (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-media-grecord.pdf)
  
[Τεκμηρίωση gnome-media-gst-mixer (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-media-gst-mixer.pdf)
  
[Τεκμηρίωση gnome-media-gstreamer-properties (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-media-gstreamer-properties.pdf)
  
[Τεκμηρίωση gnome-netstatus (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-netstatus.pdf)
  
[Τεκμηρίωση gnome-nettool (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-nettool.pdf)
  
[Τεκμηρίωση gnome-panel-help (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-panel-help.pdf)
  
[Τεκμηρίωση gnome-power-manager (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-power-manager.pdf)
  
[Τεκμηρίωση gnome-system-monitor (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-system-monitor.pdf)
  
[Τεκμηρίωση gnome-system-tools-doc (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-system-tools-doc.pdf)
  
[Τεκμηρίωση gnome-terminal (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-terminal.pdf)
  
[Τεκμηρίωση gnome-user-docs-gnome2-system-admin-guide (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-user-docs-gnome2-system-admin-guide.pdf)
  
[Τεκμηρίωση gnome-user-docs-gnome2-user-guide (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-user-docs-gnome2-user-guide.pdf)
  
[Τεκμηρίωση gnome-user-share (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-user-share.pdf)
  
[Τεκμηρίωση gnome-utils-baobab (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-utils-baobab.pdf)
  
[Τεκμηρίωση gnome-utils-gfloppy (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-utils-gfloppy.pdf)
  
[Τεκμηρίωση gnome-utils-gnome-dictionary (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-utils-gnome-dictionary.pdf)
  
[Τεκμηρίωση gnome-utils-gsearchtool (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-utils-gsearchtool.pdf)
  
[Τεκμηρίωση gnome-utils-logview (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnome-utils-logview.pdf)
  
[Τεκμηρίωση gnote (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gnote.pdf)
  
[Τεκμηρίωση gtk-doc-help (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gtk-doc-help.pdf)
  
[Τεκμηρίωση gtranslator (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gtranslator.pdf)
  
[Τεκμηρίωση gucharmap (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_gucharmap.pdf)
  
[Τεκμηρίωση library-web-data (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_library-web-data.pdf)
  
[Τεκμηρίωση mousetweaks (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_mousetweaks.pdf)
  
[Τεκμηρίωση seahorse (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_seahorse.pdf)
  
[Τεκμηρίωση seahorse-plugins (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_seahorse-plugins.pdf)
  
[Τεκμηρίωση sound-juicer-help (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_sound-juicer-help.pdf)
  
[Τεκμηρίωση tomboy (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_tomboy.pdf)
  
[Τεκμηρίωση vinagre (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_vinagre.pdf)
  
[Τεκμηρίωση zenity (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/documentation_zenity.pdf)
  
[Τεκμηρίωση gnome-access-guide (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/gnome-access-guide.pdf)
  
[Τεκμηρίωση integration-guide (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/integration-guide.pdf)
  
[Τεκμηρίωση totem (PDF)](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/05/totem.pdf)