---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2016-04-28 19:01:16
guid: https://static.gnome.org/gnome-gr/?p=3277
id: 3277
permalink: /2016/04/28/guadec2016-august-karlsruhe/
tags:
  - events
  - gnome
  - GUADEC
  - Karlsruhe
  - συνέδριο
title: Το συνέδριο GUADEC 2016 θα πραγματοποιηθεί 12–14 Αυγούστου στην Καρλσρούη της Γερμανίας
url: /2016/04/28/guadec2016-august-karlsruhe/
---

<figure style="width: 640px" class="wp-caption aligncenter">[<img class=" img-responsive " src="https://www.gnome.org/wp-content/uploads/2016/02/karlsruhe-castle.jpg" />](https://goo.gl/bkJGFx)<figcaption class="wp-caption-text">Photo: “Nacht in Karlsruhe” by Heiko S., CC-BY-NC 2.0</figcaption></figure> 

Είμαστε στην ευχάριστη θέση να σας ανακοινώσουμε την διεξαγωγή του <a href="https://2016.guadec.org/" target="_blank">συνεδρίου GUADEC 2016</a>, στην πόλη Καρλσρούη της Γερμανίας. Το συνέδριο θα διεξαχθεί 12-14 Αυγούστου στο <a href="https://kit.edu/english/" target="_blank">Karlsruhe Institute of Technology</a>.

Η Καρλσρούη βρίσκεται στα νότιοδυτικά της Γερμανίας, κοντά στα Γαλλο-Γερμανικά σύνορα. Σε αυτή την όμορφη και ιστορική πόλη βρίσκονται δυο ανώτερα δικαστήρια της Γερμανίας και αρκετά εκπαιδευτικά ιδρύματα.

> «Είμαστε περήφανοι που θα διοργανώσουμε το συνέδριο GUADEC στην Καρλσρούη φέτος και ανυπομονούμε να καλωσορίσουμε την κοινότητα του GNOME στην όμορφη πόλη μας. Με την Gulaschprogrammiernacht (GPN) έχουμε ένα μεγάλο συνέδριο χάκερ στην πόλη και έχοντας το GUADEC εδώ θα ενισχύσει περαιτέρω την τοπική κοινότητα ανοικτού κώδικα. »
  
> – Benjamin Berg, τοπικός διοργανωτής
> 
> Σας περιμένουμε!

Εάν η εταιρεία σας ή ο οργανισμός σας θα ήθελε να είναι χορηγός στο συνέδριο GUADEC, μπορείτε να βρείτε πληροφορίες στην ιστοσελίδα των χορηγών στο <a href="https://2016.guadec.org/sponsors/" target="_blank">GUADEC.or</a>g, ή να επικοινωνήσετε στην ιστοσελίδα sponsors@guadec.org ώστε να συζητήσουμε μαζί σας.

Εάν θέλετε να συμμετάσχετε στην ομάδα των εθελοντών ώστε να βοηθήσετε στην οργάνωση του συνεδρίου, μπορείτε να εγγραφείτε στην <a href="https://mail.gnome.org/mailman/listinfo/guadec-list" target="_blank">λίστα αλληλογραφίας GUADEC</a>. Θα θέλαμε να σας έχουμε μαζί μας στην οργάνωση!

Πηγές: <a href="https://www.gnome.org/" target="_blank">GNOME</a>, <a href="https://2016.guadec.org/" target="_blank">GUADEC</a>