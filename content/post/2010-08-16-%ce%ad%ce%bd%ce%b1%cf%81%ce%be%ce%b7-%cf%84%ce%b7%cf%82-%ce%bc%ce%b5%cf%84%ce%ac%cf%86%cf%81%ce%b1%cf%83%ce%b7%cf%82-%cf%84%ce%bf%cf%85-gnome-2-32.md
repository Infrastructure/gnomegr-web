---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2010-08-16 20:59:05
guid: http://gnome.gr/?p=385
id: 385
permalink: /2010/08/16/%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7-%cf%84%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-2-32/
title: Έναρξη της μετάφρασης του GNOME 2.32
url: /2010/08/16/%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7-%cf%84%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-2-32/
---

Αυτή είναι γενική ανακοίνωση για την έναρξη της μετάφρασης του GNOME 2.32.
  
Περιλαμβάνει και πληροφορίες περί GNOME 3 και GNOME Shell.

  1. Ο στόχος της κοινότητας GNOME ήταν για την έκδοση που θα προκύψει τον επόμενο μήνα να ονομαστεί GNOME 3.0 και να έχει ως βασικό συστατικό το λεγόμενο GNOME Shell. Θα μπορούσαμε να πούμε ότι το GNOME Shell έχει ως στόχο την αντικατάσταση της πάνω και κάτω μπάρας με κάτι άλλο, πιο καλό. Νομίζω ότι η καλύτερη σελίδα που εξηγεί το νέο σύστημα είναι <a href="http://live.gnome.org/GnomeShell/CheatSheet" target="_blank">http://live.gnome.org/GnomeShell/CheatSheet</a>
  2. Στο πρόσφατο συνέδριο Guadec (Ιούλιος) αποφασίστηκε ότι θα ήταν καλύτερο να αναβληθεί το GNOME 3.0 για έξι μήνες, για την Άνοιξη του 2011.
  3. Είναι στο χέρι της κάθε διανομής για το αν θα χρησιμοποιήσει τα πακέτα για το GNOME Shell ή θα βάλει το παραδοσιακό GNOME. Για τη διανομή Ubuntu της έκδοσης 10.10 αποφασίστηκε ότι έχει το παραδοσιακό GNOME που ξέρουμε.
  4. Ωστόσο τα πακέτα για να ενεργοποιήσει κάποιος το GNOME Shell θα είναι διαθέσιμα στα αποθετήρια, οπότε ο καθένας θα μπορεί να δοκιμάσει εκείνο το GNOME Shell, με το ίδιο τρόπο που π.χ. δοκιμάζουμε τώρα εύκολα και ανώδυνα το Cairo Dock, <a href="http://www.glx-dock.org/" target="_blank">http://www.glx-dock.org/</a> (κάνει το φθηνό υπολογιστή μας να αισθάνεται σα Mac).

Ξεκινούμε τώρα την ενημέρωση της μετάφραση του GNOME 2.32, κατά το <a href="http://l10n.gnome.org/languages/el/" target="_blank">http://l10n.gnome.org/languages/el/</a> που περιλαμβάνει τα πακέτα για το παραδοσιακό GNOME αλλά και το GNOME Shell.

Υπάρχουν τρία νέα πακέτα για το GNOME 2.32, που είναι και πλήρως αμετάφραστα,
  
<a href="http://l10n.gnome.org/languages/el/gnome-2-32/ui/" target="_blank">http://l10n.gnome.org/languages/el/gnome-2-32/ui/</a>

  * Caribu, <a href="http://live.gnome.org/Caribou" target="_blank">http://live.gnome.org/Caribou</a> Είναι πρόγραμμα γραφής μέσω οθόνης (on-screen keyboard), και είναι χρήσιμο σε εκείνα τα tables με οθόνες αφής.
  * GNOME Colour Manager, <a href="http://projects.gnome.org/gnome-color-manager/" target="_blank">http://projects.gnome.org/gnome-color-manager/</a> Επιτρέπει προφίλ χρωμάτων, κάτι που έλειπε.
  * Rygel, <a href="http://live.gnome.org/Rygel" target="_blank">http://live.gnome.org/Rygel</a> Επιτρέπει τον υπολογιστή σας να συνδέεται σε υπολογιστής του τοπικού σας δικτύου με ευκολία. Για παράδειγμα, αν έχετε κάποια από τις τηλεοράσεις που έχουν σύνδεση ethernet ή wifi, τότε μπορείτε να παίζεται εύκολα ταινίες και μουσική που βρίσκονται στο Linux σας πάνω στην τηλεόραση. Αν η συσκευή σας υποστηρίζει αυτό το DLNA, τότε το Rygel θα σας βοηθήσει.

Αν θέλετε να μεταφράσετε κάτι από αυτά, ρωτήστε στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα team@gnome.gr</a> για το πως θα χρησιμοποιήσετε τη μεταφραστική μνήμη ή διαβάστε τις οδηγίες στο <a href="http://wiki.gnome.gr/" target="_blank">http://wiki.gnome.gr/</a> (δείτε τις σελίδες για τα εργαλεία poEdit, Lokalize και gtranslator).