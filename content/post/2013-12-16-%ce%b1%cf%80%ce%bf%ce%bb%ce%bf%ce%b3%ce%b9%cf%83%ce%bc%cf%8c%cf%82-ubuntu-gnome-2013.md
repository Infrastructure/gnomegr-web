---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/2127057809460174150
blogger_permalink:
  - /2013/12/ubuntu-gnome-2013.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-12-16 16:36:00
guid: http://gnome.gr/2013/12/16/%ce%b1%cf%80%ce%bf%ce%bb%ce%bf%ce%b3%ce%b9%cf%83%ce%bc%cf%8c%cf%82-ubuntu-gnome-2013/
id: 2907
permalink: /2013/12/16/%ce%b1%cf%80%ce%bf%ce%bb%ce%bf%ce%b3%ce%b9%cf%83%ce%bc%cf%8c%cf%82-ubuntu-gnome-2013/
title: Απολογισμός Ubuntu GNOME – 2013
url: /2013/12/16/%ce%b1%cf%80%ce%bf%ce%bb%ce%bf%ce%b3%ce%b9%cf%83%ce%bc%cf%8c%cf%82-ubuntu-gnome-2013/
---

<div dir="ltr" style="text-align: left">
  Αν και το <b>Ubuntu GNOME Project </b>είναι σχετικά νέο, αποδείχθηκε πως είναι μία επιτυχημένη διανομή και συνεχώς προχωράει μπροστά με σταθερά βήματα. Κατά τη διάρκεια του έτους 2013, η ομάδα του Ubuntu GNOME κατάφερε να επιτύχει αρκετούς στόχους και είμαστε όλοι υπερήφανοι για επιτυχή εκπλήρωση αυτών των στόχων. Παρακάτω υπάρχει μία λίστα με τις επιτυχίες της ομάδας το 2013:</p> 
  
  <ul style="text-align: left">
    <li>
      Το Ubuntu GNOME έγινε <a href="https://lists.launchpad.net/ubuntu-gnome/msg00274.html">επίσημο παράγωγο</a> του Ubuntu.
    </li>
    <li>
      Η έκδοση <a href="http://ubuntugnome.org/ubuntu-gnome-13-04/">Ubuntu GNOME 13.04</a> είναι η πρώτη επίσημη έκδοση.
    </li>
    <li>
      Η δημιουργία του πρώτου <a href="https://wiki.ubuntu.com/UbuntuGNOME/OneStopPage">συστήματος</a> τεκμηρίωσης και wiki.
    </li>
    <li>
      H ταχύτατη ανάπτυξη του <a href="https://wiki.ubuntu.com/UbuntuGNOME/CommunicationsTeam/November2013">Ubuntu GNOME στα Social Media</a>.
    </li>
    <li>
      Η κυκλοφορία της σταθερότατης έκδοσης <a href="https://wiki.ubuntu.com/SaucySalamander/ReleaseNotes/UbuntuGNOME">Ubuntu GNOME 13.10</a>.
    </li>
    <li>
      H οργάνωση και η δημιουργία της ομάδας σε <a href="http://gnome.gr/2013/10/20/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%b3%ce%b9%ce%b1-%cf%85%cf%80%ce%bf-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b5%cf%82-sub-teams/">υπο-ομάδες</a>.
    </li>
    <li>
      H δημιουργία ξεχωριστής ομάδας για <a href="http://ubuntugnome.org/brainstorming-team/">Brainstorming και διαχείριση</a>.
    </li>
    <li>
      Η συμμετοχή <a href="http://gnome.gr/2013/09/22/%ce%b6%ce%b7%cf%84%ce%bf%cf%8d%ce%bd%cf%84%ce%b1%ce%b9-%ce%b5%ce%b8%ce%b5%ce%bb%ce%bf%ce%bd%cf%84%ce%ad%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b1-ubuntu-gnome/">νέων εθελοντών</a> στο Ubuntu GNOME.
    </li>
    <li>
      H δημιουργία νέας ομάδας για τους νέους εθελοντές (<a href="http://ubuntugnome.org/announcement-ubuntu-gnome-packaging-team/">Packaging Team</a>).
    </li>
    <li>
      H συμμετοχή του Ubuntu GNOME στο project <a href="https://wiki.ubuntu.com/StartUbuntu">StartUbuntu</a>.
    </li>
  </ul>
  
  <div>
  </div>
  
  <div>
  </div>
  
  <div>
    Είμαστε πολύ χαρούμενοι για τις επιτυχίες της ομάδας στο έτος 2013. Είναι μεγάλα κατορθώματα και σίγουρα αποτελούν κίνητρο και για περισσότερα στο μέλλον, νέα και καλύτερα. Επιστευόμαστε τα μέλη της ομάδας και είμαστε αισιόδοξοι ότι το 2014 θα είναι ακόμα καλύτερο για το Ubuntu GNOME.
  </div>
  
  <div>
  </div>
  
  <div>
    Ιδιαίτερες ευχαριστίες σε όλους όσους έκαναν το αδύνατο να φαίνεται εύκολο και δυνατο. Χωρίς την βοήθειά τους, την υποστήριξη και την συνεισφορά τους, δεν θα είχαμε κατορθώσει να επιτύχουμε όλους αυτούς τους στόχους.
  </div>
  
  <div>
  </div>
  
  <div>
    Ανυπομονούμε να γεμίσουμε ακόμα μία σελίδα, αυτή τη φορά με τα κατορθώματα του 2014 όλοι μαζί, και σας ζητάμε να μην χάσετε την ευκαιρία να γίνετε κομμάτι αυτής της καταπληκτικής εμπειρίας. Οπότε <a href="https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved">ξεκινήστε</a> σήμερα, σχεδιάστε για το αύριο και χαρείτε τις επιτυχίες και τα κατορθώματα της ομάδας την επόμενη μέρα.
  </div>
  
  <div>
  </div>
  
  <blockquote>
    <p>
      <i>Όλοι μαζί, είμαστε πιο έξυπνοι από ότι ο καθένας από εμάς!</i>
    </p>
  </blockquote>
  
  <p>
    Ευχαριστούμε που επιλέξατε το Ubuntu GNOME!<br />Να περνάτε καλά και να έχετε ένα ευτυχισμένο νέο έτος!</div>