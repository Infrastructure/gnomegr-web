---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
  - Παρουσίαση
date: 2013-10-26 16:57:37
guid: http://gnome.gr/?p=2177
id: 2177
image: /wp-content/uploads/sites/7/2013/10/αφίσα.jpg
permalink: /2013/10/26/linux-team-pamak-no1/
tags:
  - gnome
  - Linux Team
  - ΠΑΜΑΚ
  - Πανεπιστήμιο Μακεδονίας
title: 1η εκδήλωση της Κοινότητας Linux και Ελεύθερου Λογισμικού Πανεπιστημίου Μακεδονίας
url: /2013/10/26/linux-team-pamak-no1/
---

[<img src="http://gnome.gr/wp-content/uploads/2013/10/ΠΑΜΑΚ_αφίσα-194x300.jpg" alt="1η εκδήλωση της Κοινότητας Linux και Ελεύθερου Λογισμικού Πανεπιστημίου Μακεδονίας" class="aligncenter size-medium wp-image-2171 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/αφίσα.jpg)

Την Τρίτη 22 Οκτωβρίου στις 20.00 πραγματοποιήθηκε η 1η εκδήλωση της Κοινότητας Linux και Ελεύθερου Λογισμικού Πανεπιστημίου Μακεδονίας.

Η κοινότητα GNOME εκπροσωπήθηκε από τον <a href="https://wiki.gnome.org/EfstathiosIosifidis" target="_blank">Ευστάθιο Ιωσηφίδη</a> με ομιλία σχετικά με την χρήση του GNOME για νέους χρήστες που προέρχονται από το λειτουργικό Windows, καθώς και μερικά στοιχεία για την κοινότητα GNOME.

Μπορείτε να δείτε πληροφορίες σχετικά με την εκδήλωση και τα αρχεία της παρουσίασης, στον <a href="http://gnome.gr/projects/linux-team-pamak-no1/" target="_blank">σύνδεσμο των συνεδρίων</a>.

Φωτογραφίες:
  
[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/IMG_4063-300x168.jpg" alt="Ομιλία ΠΑΜΑΚ" class="aligncenter size-medium wp-image-2181 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/IMG_4063.jpg)

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/PAMAK1-300x225.jpg" alt="Ομιλία ΠΑΜΑΚ κοινό" class="aligncenter size-medium wp-image-2182 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/PAMAK1.jpg)

Περισσότερες φωτογραφίες υπάρχουν στο <a href="https://www.facebook.com/media/set/?set=a.538549212887873.1073741830.530424673700327&type=1" target="_blank">Facebook της Κοινότητας</a>

Μπορείτε να δείτε το σχετικό βίντεο της παρουσίασης:
