---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/7070851184750896013
blogger_permalink:
  - /2014/04/1404-gnome-310-310.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-04-08 16:39:00
guid: http://gnome.gr/2014/04/08/%ce%b3%ce%b9%ce%b1%cf%84%ce%af-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-14-04-%ce%b8%ce%b1-%cf%80%ce%b5%cf%81%ce%b9%ce%ad%cf%87%ce%b5%ce%b9-%cf%84%ce%b7%ce%bd-%ce%b5%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-g/
id: 2902
permalink: /2014/04/08/%ce%b3%ce%b9%ce%b1%cf%84%ce%af-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-14-04-%ce%b8%ce%b1-%cf%80%ce%b5%cf%81%ce%b9%ce%ad%cf%87%ce%b5%ce%b9-%cf%84%ce%b7%ce%bd-%ce%b5%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-g/
title: Γιατί η έκδοση 14.04 θα περιέχει την εκδοση GNOME 3.10 και όχι την 3.12
url: /2014/04/08/%ce%b3%ce%b9%ce%b1%cf%84%ce%af-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-14-04-%ce%b8%ce%b1-%cf%80%ce%b5%cf%81%ce%b9%ce%ad%cf%87%ce%b5%ce%b9-%cf%84%ce%b7%ce%bd-%ce%b5%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-g/
---

Πολλοί χρήστες ρωτάνε την ομάδα Ubuntu GNOME την ίδια ερώτηση ξανά και ξανα. Η απάντηση πάντα είναι ίδια εδώ και 5 μήνες που ξεκίνησε η δοκιμή της Trusty Tahr.

**ΕΡ: Γιατί το Ubuntu GNOME 14.04 LTS στην περιέχει την έκδοση GNOME 3.12?**

ΑΠ: Απλά, επειδή το Ubuntu GNOME 14.04 LTS θα είναι διαθέσιμο στις <a href="https://wiki.ubuntu.com/TrustyTahr/ReleaseSchedule" target="1">17 Απριλίου 2014</a> ενώ η έκδοση GNOME 3.12 εκδόθηκε στις 26 Μαρτίου. <a href="http://www.gnome.org/press/2014/03/gnome-3-12-released-with-new-features-for-users-and-developers/" target="1">Δείτε την ανακοίνωση για την έκδοση GNOME 3.12</a>.

**ΕΡ: Υπάρχει περίπτωση να δούμε την έκδοση GNOME 3.12 στο Ubuntu GNOME 14.04 LTS;**

ΑΠ: Όχι. Επειδή στις 20 Φεβρουαρίου 2014 είχαμε την διακοπή εισροής νέων πακέτων. Εκείνη την εποχή δεν είχε εκδοθεί η GNOME 3.12.

**ΕΡ: Μου αρέσει το Ubuntu GNOME και θα ήθελα να το χρησιμοποιήσω με την τελευταία έκδοση του GNOME!!!**

ΑΠ: Δεν υπάρχει πρόβλημα. Γι” αυτό υπάρχουν τα <a href="https://wiki.ubuntu.com/UbuntuGNOME/Developers" target="1">PPA</a>.

**ΕΡ: Με τα <a href="https://wiki.ubuntu.com/UbuntuGNOME/Developers" target="1">PPA</a>, μπορώ να έχω την GNOME 3.12 με το Ubuntu GNOME Trusty Tahr;**

ΑΠ: Μόλις θα έχουμε έτοιμα τα PPA για την GNOME 3.12, θα ανακοινωθεί πως μπορείτε να τα εισάγετε και να αναβαθμίσετε την έκδοση.

Ελπίζουμε να σας λύθηκαν όλες οι απορίες.  
Μπορείτε να επικοινωνήσετε μαζί μας.  
Ευχαριστούμε που επιλέξατε το Ubuntu GNOME.