---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2012-09-29 12:33:42
guid: http://gnome.gr/?p=1886
id: 1886
permalink: /2012/09/29/release-party-thessaloniki-gnome-3-6/
tags:
  - events
  - gnome
  - gnome 3.6
  - release party
  - θεσσαλονίκη
title: Release party στη Θεσσαλονίκη για το GNOME 3.6
url: /2012/09/29/release-party-thessaloniki-gnome-3-6/
---

Όπως γνωρίζετε, ανακοινώθηκε η [έκδοση 3.6](http://gnome.gr/2012/09/26/%ce%ba%cf%85%ce%ba%ce%bb%ce%bf%cf%86%cf%8c%cf%81%ce%b7%cf%83%ce%b5-%cf%84%ce%bf-gnome-3-6/ "Ανακοίνωση έκδοσης 3.6") του GNOME και όπως συνηθίζεται το γιορτάζουμε!! Οπότε στις **7 Οκτωβρίου** θα μαζευτούμε στο **Cafe Alfa** στη Θεσσαλονίκη,6 το απόγευμα στο κέντρο της πόλης ([Σβώλου με Αμύνης](http://gnome.gr/events/15/gnome-3-6-release-party/)).

Χαλαρή κουβεντούλα, ποτό κλπ για να χαλαρώσουμε και να γνωριστούμε.
  
Ελπίζω να έχουν τελειώσει οι εξετάσεις όλων και να έχετε επιστρέψει από τις διακοπές σας ;-)

Θα σας περιμένουμε να γνωριστούμε από κοντά.

ΥΓ1:Όσοι μπορούν να βοηθήσουν με αφίσες, ανακοινώσεις κλπ, ας το οργανώσουν.
  
ΥΓ2: Μπορείτε να οργανώσετε και στις δικές σας πόλεις μια εκδήλωση. Αν ενδιαφέρεστε, επικοινωνήστε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα ταχυδρομείου</a> για να σας βοηθήσουμε.

&nbsp;

<p style="text-align: center">
  <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/09/blue_black_letters.png"><img class="aligncenter  wp-image-1915 img-responsive " title="Αφίσα εκδήλωσης" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/09/blue_black_letters.png" alt="αφίσα εκδήλωσης" /></a>
</p>
