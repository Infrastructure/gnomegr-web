---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/6301305019694053988
blogger_permalink:
  - /2014/08/wallpaper-ubuntu-gnome-1410-utopic.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-08-27 15:56:00
guid: http://gnome.gr/2014/08/27/%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%cf%8c%cf%82-wallpaper-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-ubuntu-gnome-14-10-utopic-unicorn/
id: 2897
image: /wp-content/uploads/sites/7/2014/08/14876512215_454763b9f2_b.jpg
permalink: /2014/08/27/%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%cf%8c%cf%82-wallpaper-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-ubuntu-gnome-14-10-utopic-unicorn/
title: Διαγωνισμός wallpaper για το Ubuntu GNOME 14.10 (Utopic Unicorn)
url: /2014/08/27/%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%cf%8c%cf%82-wallpaper-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-ubuntu-gnome-14-10-utopic-unicorn/
---

Όπως γνωρίζετε, αυτή τη στιγμή η ανάπτυξη του Ubuntu GNOME 14.10 βρίσκεται στο στάδιο beta1. Παράλληλα γίνεται ένας διαγωνισμός για τις 10 καλύτερες φωτογραφίες που θα συμπεριληφθούν στις ταπετσαρίες του Ubuntu GNOME 14.10.

Μπορείτε να τις δείτε όλες και να ψηφίσετε στην διεύθυνση:  
<a href="http://picompete.com/contest/1419/ubuntu-gnome-14-10-wallpaper-contest/" target="1">http://picompete.com/contest/1419/ubuntu-gnome-14-10-wallpaper-contest/</a>

Δώστε προσοχή σε μια. Το όνομά της είναι:  
**Ubuntu14.10\_ChIossif\_Aug14_cc-by-sa-4.0**

Κάντε αναζήτηση με αυτό το όνομα και ψηφήστε την φωτογραφία, ώστε να συμπεριληφθεί στην επίσημη έκδοση. Ο λόγος για να το κάνετε, είναι διότι η φωτογραφία έχει υποβληθεί από τον Χρήστο Ιωσηφίδη.

Η φωτογραφία είναι:  
<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/08/14876512215_454763b9f2_b-300x188.jpg" width="70%" height="70%"  class="img-responsive" />