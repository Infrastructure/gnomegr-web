---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2012-09-04 15:21:27
guid: http://gnome.gr/?p=1687
id: 1687
permalink: /2012/09/04/%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7-%cf%84%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-3-6/
tags:
  - documentation
  - gnome
  - gnome 3.6
  - l10n
  - μετάφραση
title: Έναρξη της μετάφρασης του GNOME 3.6
url: /2012/09/04/%ce%ad%ce%bd%ce%b1%cf%81%ce%be%ce%b7-%cf%84%ce%b7%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-3-6/
---

&nbsp;

<img class="aligncenter  wp-image-1710 img-responsive " title="equation" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/09/equation.png" alt="" />

Σύμφωνα με το <a href="https://live.gnome.org/ThreePointFive" target="_blank">πρόγραμμα ανάπτυξης</a> του GNOME 3.5.x από τις 3 Σεπτεμβρίου βρισκόμαστε στη φάση **«String Freeze»**.Αυτό σημαίνει πως για οποιαδήποτε αλλαγή στα μηνύματα των εφαρμογών απαιτείται έγκριση από την ομάδα μετάφρασης του GNOME.

Βρισκόμαστε στην τελική ευθεία καθώς η προθεσμία υποβολής των μεταφράσεων είναι μέχρι **26 Σεπτεμβρίου**,όπου και θα κυκλοφορήσει η τελική έκδοση του GNOME 3.6

### Μετάφραση

Αυτή τη στιγμή είμαστε σε αρκετά καλό επίπεδο μετάφρασης της έκδοσης 3.6,στο **99%** για το <a href="http://l10n.gnome.org/languages/el/gnome-3-6/ui/" target="_blank">γραφικό περιβάλλον</a> ενώ στο **82%** για την <a href="http://l10n.gnome.org/languages/el/gnome-3-6/doc/" target="_blank">τεκμηρίωση</a> (με αρκετές από τις μεγάλες μεταφράσεις να είναι σε εξέλιξη) !!

Προταρχικός μας στόχος τι άλλο&#8230; να φτάσουμε το 100% τόσο στο γραφικό περιβάλλον όσο και στην τεκμηρίωση της έκδοσης 3.6,όπως επίσης και να αυξήσουμε,όσο μπορούμε,τη μετάφραση γραφικού περιβάλλοντος των <a href="http://l10n.gnome.org/languages/el/gnome-extras/ui/" target="_blank">λοιπών εφαρμογών του GNOME</a>.

### Συμμετοχή

Οδηγίες για τη συμμετοχή σας στο έργο της μετάφρασης,θα βρείτε στη σελίδα «[Θέλω να μεταφράσω!](http://gnome.gr/%CF%83%CF%85%CE%BC%CE%BC%CE%B5%CF%84%CE%BF%CF%87%CE%AE/%CE%B8%CE%AD%CE%BB%CF%89-%CE%BD%CE%B1-%CE%BC%CE%B5%CF%84%CE%B1%CF%86%CF%81%CE%AC%CF%83%CF%89/)» όπως επίσης και στο wiki της κοινότητας μας στο <a href="http://wiki.gnome.gr" target="_blank">http://wiki.gnome.gr</a>.Για οποιαδήποτε ερώτηση μη διστάσετε να επικοινωνήσετε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα ταχυδρομείου</a>.

&nbsp;

Ας ξεκινήσουμε λοιπόν τη μετάφραση !!

&nbsp;