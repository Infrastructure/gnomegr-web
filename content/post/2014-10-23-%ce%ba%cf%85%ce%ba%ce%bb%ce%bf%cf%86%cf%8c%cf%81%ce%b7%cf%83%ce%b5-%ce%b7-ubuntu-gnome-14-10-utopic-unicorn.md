---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/5879106488162786546
blogger_permalink:
  - /2014/10/ubuntu-gnome-1410-utopic-unicorn.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-10-23 05:00:00
guid: http://gnome.gr/2014/10/23/%ce%ba%cf%85%ce%ba%ce%bb%ce%bf%cf%86%cf%8c%cf%81%ce%b7%cf%83%ce%b5-%ce%b7-ubuntu-gnome-14-10-utopic-unicorn/
id: 2893
image: /wp-content/uploads/sites/7/2014/10/maps.png
permalink: /2014/10/23/%ce%ba%cf%85%ce%ba%ce%bb%ce%bf%cf%86%cf%8c%cf%81%ce%b7%cf%83%ce%b5-%ce%b7-ubuntu-gnome-14-10-utopic-unicorn/
title: Κυκλοφόρησε η Ubuntu GNOME 14.10 Utopic Unicorn
url: /2014/10/23/%ce%ba%cf%85%ce%ba%ce%bb%ce%bf%cf%86%cf%8c%cf%81%ce%b7%cf%83%ce%b5-%ce%b7-ubuntu-gnome-14-10-utopic-unicorn/
---

Η ομάδα <a href="https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved/WhoWeAre" target="1">Ubuntu GNOME</a> με χαρά σας ανακοινώνει την κυκλοφορία της έκδοσης **Ubuntu GNOME 14.10 Utopic Unicorn**.

**ΤΙ ΝΕΟ ΘΑ ΔΟΥΜΕ;**  
* Gnome-shell 3.12  
* Περιλαμβάνονται ως προεπιλογή το gnome-weather και gnome-maps  
* Ανανεωμένη εφαρμογή Βίντεο  
* Νέα εμφάνιση gedit  
* Βελτιωμένη υποστήριξη οθόνης υψηλής ανάλυσης  
* Δημιουργία δικών σας φακέλων εφαρμογών  
* Ενημερώσεις στην εμπειρία χρήσης του GNOME 3  
* Υπάρχει το gnome-classic-session που θυμίζει το Gnome 2  
* 10 νέα wallpapers που βγήκαν <a href="http://gnome.gr/2014/10/05/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%ce%bf%cf%8d-wallpapers-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%ad/" target="1">μέσα από διαγωνισμό</a>

**Ελάχιστες απαιτήσεις συστήματος**  
* Επεξεργαστής 1 GHz  
* Μνήμη συστήματος 1.5 GB RAM  
* Χώρος στον δίσκο 7 GB  
* CD/DVD ή USB για το μέστο εγκατάστασης  
* Πρόσβαση στο Internet για ενημέρωση κατά την εγκατάσταση

Η έκδοση 14.10 θα έχει **<a href="https://wiki.ubuntu.com/Releases" target="1">κύκλο ζωής 9 μήνες</a>**. Πριν εγκαταστήσετε τη νέα έκδοση, διαβάστε τις <a href="https://wiki.ubuntu.com/UtopicUnicorn/ReleaseNotes/UbuntuGNOME" target="1">σημειώσεις έκδοσης</a>.

Κατεβάστε την 14.10 από την <a href="https://wiki.ubuntu.com/UbuntuGNOME/GetUbuntuGNOME#Ubuntu_GNOME_14.10" target="1">ιστοσελίδα λήψεων</a>.

Δημιουργήστε το μέσο εγκάστασης όπως <a href="https://wiki.ubuntu.com/UbuntuGNOME/Installation#Creating_Installation_Media" target="1">περιγράφεται εδώ</a>.   
Προτιμήστε 64bit εάν έχετε UEFI και υπολογιστή με RAM από 2GB και άνω.

Εάν δεν γνωρίζετε πως να εγκαταστήσετε το Ubuntu GNOME, δείτε [τον οδηγό εγκατάστασης](http://gnome.gr/p/ubuntu-gnome.html).

Εάν θέλετε να αναβαθμίσετε από την 14.04 σε 14.10, ακολουθήστε την εντολή στο τερματικό:

<div>
  sudo update-manager -d
</div>

Δείτε μερικές φωτογραφίες:

<a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/maps.png" target="1"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/maps.png" width="70%" height="70%"  class="img-responsive" /></a>  
<a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/weather.png" target="1"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/weather.png" width="70%" height="70%"  class="img-responsive" /></a>  
Ένα μεγάλο ευχαριστώ σε όσους βοήθησαν στις δοκιμές ώστε να είναι μια σταθερή έκδοση.  
Για οποιαδήποτε απορία, μπορείτε να επικοινωνήσετε είτε στο <a href="http://forum.ubuntu-gr.org/viewtopic.php?f=70&t=24820" target="1">forum</a>, είτε να μας στείλετε mail (info ΠΑΠΑΚΙ ubuntugnome ΤΕΛΕΙΑ gr.

Δείτε και την ανακοίνωση στο <a href="http://ubuntugnome.org/ubuntu-gnome-14-10-is-released/" target="1">http://www.ubuntugnome.org</a>
