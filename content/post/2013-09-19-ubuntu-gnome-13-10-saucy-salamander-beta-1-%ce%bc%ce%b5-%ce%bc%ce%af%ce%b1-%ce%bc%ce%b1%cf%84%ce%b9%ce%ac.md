---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3929697404496441430
blogger_permalink:
  - /2013/09/ubuntu-gnome-1310-saucy-salamander-beta.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-09-19 15:58:00
guid: http://gnome.gr/2013/09/19/ubuntu-gnome-13-10-saucy-salamander-beta-1-%ce%bc%ce%b5-%ce%bc%ce%af%ce%b1-%ce%bc%ce%b1%cf%84%ce%b9%ce%ac/
id: 2913
image: /wp-content/uploads/sites/7/2013/09/logo_medium.png
permalink: /2013/09/19/ubuntu-gnome-13-10-saucy-salamander-beta-1-%ce%bc%ce%b5-%ce%bc%ce%af%ce%b1-%ce%bc%ce%b1%cf%84%ce%b9%ce%ac/
title: Ubuntu GNOME 13.10 «Saucy Salamander» (Beta 1) – Με μία ματιά
url: /2013/09/19/ubuntu-gnome-13-10-saucy-salamander-beta-1-%ce%bc%ce%b5-%ce%bc%ce%af%ce%b1-%ce%bc%ce%b1%cf%84%ce%b9%ce%ac/
---

<div dir="ltr" style="text-align: left">
  Με την κυκλοφορία της έκδοσης Ubuntu 12.10 (Quantal Quetzal) γνωρίσαμε την πρώτη συγκροτημένη ανεπίσημη προσπάθεια δημιουργίας μίας Ubuntu-based διανομής με περιβάλλον εργασίας το ολοκληρωμένο GNOME 3.<br />Υπήρξαν προηγουμένως αξιόλογα projects προκειμένου να εφαρμοστεί κάτι ανάλογο, όπως το <a href="http://ubuntu-gs-remix.sourceforge.net/p/home/">Ubuntu GNOME Shell Remix</a>, αλλά δεν είχαν το μέγεθος και την οργάνωση του Ubuntu GNOME.</p> 
  
  <div>
  </div>
  
  <div>
    H διανομή με την τότε ονομασία Ubuntu GNOME Remix 12.10, ήταν αυτό που ήθελαν πολλοί χρήστες, ένα remix του Ubuntu με προεγκατεστημένο περιβάλλον εργασίας το αγαπημένο GNOME Shell.
  </div>
  
  <div>
  </div>
  
  <div>
    Με την έκδοση 13.04 το Ubuntu GNOME Remix, πλέον επίσημα με την ονομασία <b><a href="https://wiki.ubuntu.com/UbuntuGNOMEOneStopPage">Ubuntu GNOME</a></b>, ήρθε να ολοκληρώσει την οικογένεια των παραγώγων του Ubuntu, η οποία αποτελείται από διανομές Ubuntu με όλα τα μεγάλα περιβάλλοντα εργασίας.
  </div>
  
  <div>
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/logo_medium.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/logo_medium-300x35.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Ubuntu GNOME – Νέο λογότυπο
      </td>
    </tr>
  </table>
  
  <div>
  </div>
  
  <div>
    Σήμερα, ένα μήνα πριν την κυκλοφορία της έκδοσης 13.10 (Saucy Salamander) το Ubuntu GNOME 13.10 βρίσκεται σε στάδιο ανάπτυξης Beta 1. Η δουλειά που έχει γίνει είναι εμφανής, έχουν διορθωθεί πολλά bugs, η διανομή είναι σταθερή, βελτιώνεται συνεχώς και βλέπουμε πολλά νέα στοιχεία που διακρίνουν το Ubuntu GNOME 13.10 από τις προηγούμενες εκδόσεις.
  </div>
  
  <div>
  </div>
  
  <p>
    <a name='more'></a>
  </p>
  
  <h2 style="text-align: left">
    <b>Με την πρώτη ματιά&#8230;</b>
  </h2>
  
  <div>
    Το πρώτο πράγμα που προσέχει ένας χρήστης του Ubuntu GNOME κατά τη διαδικασία της εγκατάστασης της 13.10, είναι η νέα plymouth screen.
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/ubuntu_gnome_13_10_plymouth_theme__official__by_aldomann-d6k2mof.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/ubuntu_gnome_13_10_plymouth_theme__official__by_aldomann-d6k2mof-300x188.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Ubuntu GNOME 13.10 Plymouth Screen
      </td>
    </tr>
  </table>
  
  <div>
    Λιτή, με το λογότυπο του GNOME και το χαρακτηριστικό γκρί χρώμα που έχουμε δει στην οθόνη εισόδου του GDM, σίγουρα μία ευχάριστη προσθήκη που δεν υπήρχε στις προηγούμενες εκδόσεις.
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/grub.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/grub-300x225.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Ubuntu GNOME 13.10 GRUB
      </td>
    </tr>
  </table>
  
  <div>
    Βλέπουμε και άλλες μικρές αλλαγές στην εμφάνιση, το γκρι είναι το χρώμα και στο φόντο του grub, που αντικατέστησε το μπλε που υπήρχε στις προηγούμενες εκδόσεις. Στην οθόνη εισόδου του GDM υπάρχει πλέον το νέο λογότυπο του Ubuntu GNOME που αντικατέστησε αυτό του Ubuntu που υπάρχει στην 13.04.
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gdm.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gdm-300x232.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        GDM
      </td>
    </tr>
  </table>
  
  <div>
  </div>
  
  <div>
    Στην έκδοση Ubuntu GNOME 13.10, έχουν συμπεριληφθεί τα περισσότερα πακέτα του GNOME 3.8. Το περιβάλλον εργασίας δεν έχει πολλές διαφορές από αυτό στην έκδοση 13.04, οικείο, με το χαρακτηριστικό wallpaper του GNOME και το GNOME Shell 3.8. Δεν έχει γίνει ιδιαίτερη παραμετροποίηση στην εμφάνιση και τα θέματα έχουν παραμείνει όπως τα γνωρίζουμε, με default το Adwaita και τα εικονίδια του GNOME. Ο λόγος είναι ότι οι developers του Ubuntu GNOME προτιμούν να φέρουν στην επιφάνεια εργασίας μας την «αγνή GNOME εμπειρία», όπως την έχουν οραματιστεί οι developers του GNOME. Η παραμετροποίηση είναι στα χέρια του χρήστη, με πολλά εργαλεία στη διάθεσή του, όπως νέα <a href="http://gnome-look.org/">θέματα</a>, <a href="https://extensions.gnome.org/">extensions</a>&nbsp;και άλλα.
  </div>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gs2.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gs2-300x223.png"  class="img-responsive" /></a><a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gs3.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gs3-300x219.png"  class="img-responsive" /></a>
  </div>
  
  <div style="clear: both;text-align: center">
  </div>
  
  <div style="clear: both;text-align: left">
    Αυτές οι αλλαγές που βλέπουμε, μικρές μεν, αλλά δίνουν μία ξεχωριστή νότα στο Ubuntu GNOME 13.10 που έλειπε από τις προηγούμενες εκδόσεις. Είναι ξεκάθαρο πως τα νέα στοιχεία βάζουν την διανομή σε δικό της μονοπάτι.
  </div>
  
  <div style="clear: both;text-align: left">
  </div>
  
  <div style="clear: both;text-align: left">
    Στην έκδοση 3.10 του GNOME θα δούμε και παραλλαγές του κλασσικού wallpaper που ίσως συμπεριληφθούν στην τελική έκδοση Ubuntu GNOME 13.10:
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/bright_day_3.10.jpg" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/bright_day_3.10-300x169.jpg"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Bright Day
      </td>
    </tr>
  </table>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/good_night_3.10.jpg" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/good_night_3.10-300x169.jpg"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Goog Night
      </td>
    </tr>
  </table>
  
  <div style="clear: both;text-align: left">
  </div>
  
  <h2 style="text-align: left">
    <b>Οι αναβαθμίσεις&#8230;</b>
  </h2>
  
  <div>
    Όταν το Ubuntu GNOME βρισκόταν ακόμα στα πρώτα του βήματα και πριν γίνει καν επίσημο παράγωγο του Ubuntu, μάθαμε πως οι εκδόσεις του Ubuntu GNOME δεν θα περιέχουν τις τελευταίες εκδόσεις των πακέτων του περιβάλλοντος εργασίας GNOME. Αυτό γιατί οι κύκλοι των εκδόσεων του Ubuntu και των αναβαθμίσεων του GNOME είναι πανομοιότυποι και δεν υπάρχει ο απαραίτητος χρόνος για δοκιμές, προκειμένου να υπάρχει στο Ubuntu η τελευταία έκδοση του GNOME σε σταθερή κατάσταση.
  </div>
  
  <div>
  </div>
  
  <div>
    Το κεντρικό αποθετήριο της Gnome3 team περιέχει όλα τα σταθερά πακέτα που θα περνάνε στην έκδοση και είναι ενεργοποιημένο στο Ubuntu GNOME ώστε να μας παρέχει την αγνή GNOME εμπειρία όσο το δυνατόν πιο αποτελεσματικά.
  </div>
  
  <div>
  </div>
  
  <div>
    Στα πλαίσια αυτής της λογικής και για να ενημερώνεται το σύστημα με τις εξελίξεις στο GNOME <a href="http://gnome.gr/2013/09/15/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-gnome3-next-ppa/">δημιουργήθηκε το αποθετήριο Gnome3-next</a>. Σε αυτό το αποθετήριο θα υπάρχουν τα πακέτα από την επόμενη μεγάλη έκδοση του GNOME (τώρα την 3.10) και θα λειτουργεί για την έκδοση 13.10 και μετά. Προσθέτοντας το αποθετήριο στις πηγές λογισμικού, θα έχουμε την δυνατότητα να εγκαθιστούμε στο σύστημά μας τα πακέτα των επόμενων εκδόσεων, που θεωρούνται σταθερά, διατηρώντας έτσι up-to-date την εμπειρία χρήσης του GNOME.
  </div>
  
  <div>
  </div>
  
  <div>
    Για όσους πειραματίζονται και θέλουν να έχουν τις τελευταίες εκδόσεις και για άλλα πακέτα, υπάρχει το αποθετήριο&nbsp;<a href="https://launchpad.net/~gnome3-team/+archive/gnome3-staging">gnome3-staging</a>&nbsp;που περιέχει τα πακέτα που ελέγχονται και δοκιμάζονται πριν περάσουν στο κεντρικό αποθετήριο του Gnome3. Tα πακέτα στο gnome3-staging έχουν εξαρτήσεις που βρίσκονται στο αποθετήριο gnome3-next, οπότε για καλύτερες δοκιμές, κάποιος θα πρέπει να προσθέσει και τα δύο αποθετήρια στο Ubuntu GNOME 13.10.
  </div>
  
  <div>
    To gnome3-staging υπάρχει μόνο για λόγους δοκιμών και δεν προτείνεται για κάποιον που επιδιώκει να έχει ένα σταθερό σύστημα.
  </div>
  
  <div>
  </div>
  
  <h2 style="text-align: left">
    <b>Σαν να μην άλλαξε τίποτα&#8230;</b>
  </h2>
  
  <div>
    Με την έκδοση Ubuntu 11.04 αποχαιρετίσαμε το αγαπημένο μας GNOME Panel (GNOME 2) και γνωρίσαμε το Unity που δεν είχε την μορφή και την λειτουργικότητα που έχει σήμερα. Την ίδια περίοδο το GNOME πέρασε στην έκδοση 3 όπου το GNOME Shell αντικατέστησε το GNOME Panel ως default γραφικό περιβάλλον του περιβάλλοντος εργασίας.
  </div>
  
  <div>
    Tότε ως μέσο μετάβασης υπήρχε το «GNOME Classic» (gnome-session-fallback), ένα γραφικό περιβάλλον για όσους δεν ήθελαν να συνεχίσουν με το Unity ή το GNOME Shell.
  </div>
  
  <div>
    Aργότερα ανακοινώθηκε πως το project για το GNOME Classic θα καταργηθεί, για διάφορους λόγους αλλά κυρίως επειδή η συντήρησή του ήταν δύσκολη και χρονοβόρα.
  </div>
  
  <div>
  </div>
  
  <div>
    Η αντικατάσταση του άκρως παραμετροποιήσιμου GNOME Panel από το Unity και το GNOME Shell &nbsp;καθώς και η κατάργηση του GNOME Classic, δυσαρέστησε πολλούς χρήστες, ενώ ακόμα και σήμερα βλέπουμε αναφορές στο GNOME Panel και μία ευραίως αποδεκτή άποψη πως το περιβάλλον αυτό δεν έπρεπε να αντικατασταθεί. Πολλοί χρήστες σήμερα φαίνεται να χρησιμοποιούν παλιές εκδόσεις διανομών Linux λόγω της ύπαρξης του GNOME Panel.
  </div>
  
  <div>
  </div>
  
  <div>
    H ομάδα ανάπτυξης του Ubuntu GNOME σε μία προσπάθεια να αναστήσει την εμπειρία χρήσης του GNOME Panel, εισήγαγε στην 13.10 μία συνεδρία με όνομα «GNOME Classic» (ξανά). Πρόκειται για το GNOME Shell κατάλληλα παραμετροποιημένο ώστε να θυμίζει το GNOME Panel.
  </div>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic1.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic1-300x224.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        Ubuntu GNOME 13.10 – GNOME Classic Session
      </td>
    </tr>
  </table>
  
  <div>
  </div>
  
  <div>
    Mε ενεργοποιημένα κάποια extensions, όπως το gnomenu, το places, το frippery bottom panel, το ρολόι στα δεξιά και διάφορες άλλες αλλαγές, αυτή η συνεδρία φέρνει αυτό που ζητούν όσοι νοσταλγούν το «κλασσικό» GNOME Panel.
  </div>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gdm2.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/gdm2-300x221.png"  class="img-responsive" /></a><a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic2.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic2-300x224.png"  class="img-responsive" /></a>&nbsp;
  </div>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic3.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic3-300x223.png"  class="img-responsive" /></a><a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic4.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/classic4-300x222.png"  class="img-responsive" /></a>
  </div>
  
  <p>
    &nbsp;Eνώ παράλληλα το hot corner είναι ενεργοποιημένο για το Overview του GNOME Shell, το οποίο φυσικά είναι στα χέρια του χρήστη να το απενεργοποιήσει.
  </p>
  
  <table align="center" cellpadding="0" cellspacing="0" style="margin-left: auto;margin-right: auto;text-align: center">
    <tr>
      <td style="text-align: center">
        <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/overview.png" style="margin-left: auto;margin-right: auto"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/overview-300x224.png"  class="img-responsive" /></a>
      </td>
    </tr>
    
    <tr>
      <td style="text-align: center">
        GNOME Classic Session – Overview
      </td>
    </tr>
  </table>
  
  <p>
  </p>
  
  <h2 style="text-align: left">
    <b>Τεχνικά&#8230;</b>
  </h2>
  
  <p>
    &nbsp;Όπως είδαμε, προστέθηκε η νέα plymouth screen, τα περισσότερα πακέτα από το GNOME 3.8 συμπεριλαμβάνονται στην έκδοση 13.10 (Beta1), καθώς και το ανανεωμένο GNOME Classic Session. Η έκδοση βασίζεται στον πυρήνα 3.11.
  </p>
  
  <p>
    Όπως και στην 13.04 υπάρχει προεγκατεστημένο το Ubuntu Software Center και όχι το αντίστοιχο λογισμικό που παρέχει το GNOME. Το Ubuntu Software Center μας παρέχει περισσότερες επιλογές και για αυτό προτιμήθηκε.<br />Το Ubuntu Online Accounts δεν θα είναι προεγκατεστημένο στην έκδοση 13.10 (<a href="https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/1040193">1040193</a>), ενώ οι default εφαρμογές θα είναι όπως τις γνωρίζουμε: Firefox, Evolution, LibreOffice.
  </p>
  
  <p>
    Στις <a href="https://wiki.ubuntu.com/UbuntuGNOME#System_Requirements">απαιτήσεις συστήματος</a>&nbsp;τονίζεται πως το Ubuntu GNOME δεν είναι η κατάλληλη διανομή για παλαιούς υπολογιστές, καθώς ορίζεται ως ελάχιστο μέγεθος μνήμης RAM αυτό του 1GB. Για υπολογιστές που δεν ικανοποιούν αυτή τη συνθήκη, προτιμούνται άλλα παράγωγα της οικογενείας του Ubuntu, όπως το Lubuntu και το Xubuntu.
  </p>
  
  <h2 style="text-align: left">
    <b>Λήψη και δοκιμές&#8230;</b>
  </h2>
  
  <p>
    Mπορείτε να κατεβάσετε την έκδοση Ubuntu GNOME 13.10 Beta 1 από&nbsp;<a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/saucy/beta-1/">εδώ</a>. Ενώ αν υπάρχει ενδιαφέρον για συμμετοχή σε δοκιμές, η ομάδα ανάπτυξης του Ubuntu GNOME έχει ανακοινώσει πως <a href="http://gnome.gr/2013/09/15/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%b7%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7%cf%82-ubuntu-gnome-13-10/">υπάρχει ανάγκη για περισσότερους δοκιμαστές</a>.
  </p>
  
  <p>
    Yπάρχουν ακόμα κάποια <a href="https://wiki.ubuntu.com/SaucySalamander/Beta1/UbuntuGNOME#Known_Problems">προβλήματα</a> που πρέπει να διορθωθούν, για αυτό και η έκδοση Beta 1 δεν προτείνεται για όσους χρειάζονται ένα αξιόπιστο περιβάλλον. Οι χρήστες που δεν θέλουν να ρισκάρουν τη σταθερότητα του συστήματος, καλό θα είναι να περιμένουν την κυκλοφορία της σταθερής έκδοσης 13.10.
  </p>
  
  <p>
    Oι εκδόσεις Alpha και Beta είναι δοκιμαστικές για τους προγραμματιστές και όσους συμμετέχουν στην διαδικασία με διορθώσεις, μεταφράσεις κλπ.
  </p>
  
  <p>
    Η ημερομηνία κυκλοφορίας του Ubuntu GNOME 13.10 (Final Release) δεν έχει ανακοινωθεί ακόμα και μέχρι τότε, αναμένουμε και άλλες αλλαγές.
  </p>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/ubuntu_gnome_roundel.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/ubuntu_gnome_roundel.png"  class="img-responsive" /></a>
  </div>
  
  <p>
  </p>
  
  <div>
  </div>
  
  <div>
  </div>
  
  <div>
  </div>
  
  <div style="clear: both;text-align: left">
  </div>
  
  <div style="clear: both;text-align: left">
  </div>
  
  <div>
  </div>
</div>
