---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
date: 2012-05-21 12:40:48
guid: http://gnome.gr/?p=1411
id: 1411
image: /wp-content/uploads/sites/7/2012/05/banner-gsoc2012.png
permalink: /2012/05/21/google-summer-of-code-2012/
tags:
  - Google
  - Google Summer of Code
  - GSoC
title: 'Google Summer of Code 2012: Συμμετοχή Gnome'
url: /2012/05/21/google-summer-of-code-2012/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/banner-gsoc2012.png" alt="Google Summer of Code 2012" title="GSoC 2012" class="aligncenter size-medium wp-image-1412 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/05/banner-gsoc2012.png)

Με χαρά σας ανακοινώνουμε ότι έγιναν δεκτοί 29 φοιτητές στην συμμετοχή του Gnome στο Google Summer of Code 2012. Οι φοιτητές θα εργαστούν σε πολλά projects βελτιώνοντας το γραφικό περιβάλλον GNOME 3, την τεχνολογία που κρύβεται στο GNOME και πολλές δημοφιλείς εφαρμογές. Μερικές εφαρμογές που θα βελτιώσουν οι φοιτητές είναι Documents, Web, Boxes, Calculator, Banshee, Getting Things GNOME!, Activity Journal και GCompris.

Το GNOME ευχαριστεί την Google για την γενναιόδωρη υποστήριξη στα έργα ελεύθερου λογισμικού καθώς και για την αποδοχή του GNOME για όγδοη συνεχόμενη χρονιά! Επίσης, ένα μεγάλο ευχαριστώ στους μέντορες που θα βοηθήσουν τους φοιτητές στο έργο τους!

Για περισσότερες πληροφορίες σχετικά με τα projects επισκεφθείτε την ιστοσελίδα του [GNOME στο Google Summer of Code](http://www.google-melange.com/gsoc/org/google/gsoc2012/gnome). Οι φοιτητές θα γράφουν στα προσωπικά τους ιστολόγια για την πρόοδό τους και αυτά θα εμφανίζονται στο Planet GNOME όλο το καλοκαίρι.

Πηγή: <a href="http://www.gnome.org/" target="_blank">GNOME.org</a>
