---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/8681420701167455102
blogger_permalink:
  - /2014/08/utopic-unicorn-beta-1.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-08-29 11:00:00
guid: http://gnome.gr/2014/08/29/utopic-unicorn-beta-1-%cf%80%cf%81%ce%bf%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae/
id: 2896
permalink: /2014/08/29/utopic-unicorn-beta-1-%cf%80%cf%81%ce%bf%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae/
title: Utopic Unicorn Beta 1 προς δοκιμή
url: /2014/08/29/utopic-unicorn-beta-1-%cf%80%cf%81%ce%bf%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae/
---

Η ομάδα <a href="http://distrowatch.com/table.php?distribution=ubuntugnome" target="1">Ubuntu GNOME</a> σας παρουσιάζει την δοκιμαστική έκδοση Ubuntu GNOME Utopic Unicorn Beta 1.

Παρακαλούμε, για περισσότερες πληροφορίες διαβάστε τις [σημειώσεις της έκδοσης beta 1](https://wiki.ubuntu.com/UtopicUnicorn/Beta1/UbuntuGNOME) της 14.10.

Ιστοσελίδα <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/14.10/beta-1/" target="1">ΛΗΨΕΩΝ</a>

ΣΗΜΕΙΩΣΗ:

Αυτή είναι η έκδοση Beta 1. Οι εκδόσεις Ubuntu GNOME Beta **ΔΕΝ** συνίσταται για:

* Τελικούς χρήστες που δεν γνωρίζουν για τα προβλήματα των προ-εκδόσεων   
* Όποιον χρειάζεται ένα σταθερό σύστημα  
* Όποιος δεν αισθάνεται άνετα να καταλήγει με ένα κατεστραμμένο σύστημα  
* Παραγωγικό περιβάλλον με δεδομένα και έργα που χρειάζεται αξιοπιστία

Οι εκδόσεις **Ubuntu GNOME Beta** συνίσταται για:

* Χρήστες που θέλουν να βοηθήσουν στην ανάπτυξη, βρίσκοντας σφάλματα και υποβάλοντάς τα προς διόρθωση  
* Προγραμματιστές του Ubuntu GNOME

**Βοηθήσετε την δοκιμή του Ubuntu GNOME:**  
Διαβάστε στο <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">Ubuntu GNOME Wiki Page</a>.

Ευχαριστούμε που βοηθάτε στην ανάπτυξη του <a href="http://distrowatch.com/table.php?distribution=ubuntugnome" target="1">Ubuntu GNOME</a>.

Επίσης, μην ξεχνάτε να ψηφίσετε την Ελληνική συμμετοχή στον διαγωνισμό για τα wallpapers. Για περισσότερες πληροφορίες, δείτε στην ανάρτηση [Διαγωνισμός wallpaper για το Ubuntu GNOME 14.10 (Utopic Unicorn)](http://gnome.gr/2014/08/27/%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%cf%8c%cf%82-wallpaper-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-ubuntu-gnome-14-10-utopic-unicorn/)