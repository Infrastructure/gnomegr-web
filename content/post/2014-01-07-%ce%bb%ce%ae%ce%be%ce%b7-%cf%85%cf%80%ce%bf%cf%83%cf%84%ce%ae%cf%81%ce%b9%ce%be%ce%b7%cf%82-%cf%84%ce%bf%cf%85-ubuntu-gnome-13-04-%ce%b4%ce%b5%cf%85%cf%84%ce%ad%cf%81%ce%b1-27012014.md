---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/179720416146519505
blogger_permalink:
  - /2014/01/ubuntu-gnome-1304-27012014.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-01-07 10:43:00
guid: http://gnome.gr/2014/01/07/%ce%bb%ce%ae%ce%be%ce%b7-%cf%85%cf%80%ce%bf%cf%83%cf%84%ce%ae%cf%81%ce%b9%ce%be%ce%b7%cf%82-%cf%84%ce%bf%cf%85-ubuntu-gnome-13-04-%ce%b4%ce%b5%cf%85%cf%84%ce%ad%cf%81%ce%b1-27012014/
id: 2906
image: /wp-content/uploads/sites/7/2014/01/raring-ringtail-logo.png
permalink: /2014/01/07/%ce%bb%ce%ae%ce%be%ce%b7-%cf%85%cf%80%ce%bf%cf%83%cf%84%ce%ae%cf%81%ce%b9%ce%be%ce%b7%cf%82-%cf%84%ce%bf%cf%85-ubuntu-gnome-13-04-%ce%b4%ce%b5%cf%85%cf%84%ce%ad%cf%81%ce%b1-27012014/
title: Λήξη υποστήριξης του Ubuntu GNOME 13.04 – Δευτέρα 27/01/2014
url: /2014/01/07/%ce%bb%ce%ae%ce%be%ce%b7-%cf%85%cf%80%ce%bf%cf%83%cf%84%ce%ae%cf%81%ce%b9%ce%be%ce%b7%cf%82-%cf%84%ce%bf%cf%85-ubuntu-gnome-13-04-%ce%b4%ce%b5%cf%85%cf%84%ce%ad%cf%81%ce%b1-27012014/
---

<div dir="ltr" style="text-align: left">
  Η έκδοση Ubuntu GNOME 13.04 (Raring Ringtail) είναι η πρώτη έκδοση του Ubuntu με διάρκεια υποστήριξης 9 μηνών. Κυκλοφόρησε στις 25 Απριλίου του 2013 και αυτόν τον μήνα (Ιανουάριο 2014) συμπληρώνεται το διάστημα των 9 μηνών και έτσι, <b>η έκδοση Ubuntu GNOME 13.04 θα σταματήσει να υποστηρίζεται (End Of Life) την Δευτέρα, 27/01/2014</b>.</p> 
  
  <p>
    Αυτό σημαίνει πως από τις 27/01/2014 η εν λόγω έκδοση δεν θα βελτιώνεται με ενημερώσεις ασφαλείας και αναβαθμίσεις των πακέτων της, καθώς τα αποθετήρια του λογισμικού της θα μεταφερθούν στο αρχείο και θα παγώσουν.
  </p>
  
  <p>
    Για όσους λοιπόν χρησιμοποιούν ακόμα την έκδοση 13.04, συνίσταται ισχυρά να αναβαθμίσουν στην νεότερη και τρέχουσα έκδοση του Ubuntu GNOME, την 13.10 (Saucy Salamander) η οποία θα υποστηρίζεται μέχρι τον Ιούλιο του 2014, ενώ τον Απρίλιο του 2014 περιμένουμε την κυκλοφορία της νέας <i>και πρώτης για το Ubuntu GNOME</i> LTS έκδοσης, Ubuntu GNOME 14.04 (Trusty Tahr).
  </p>
  
  <p>
    Η έκδοση Ubuntu GNOME 13.04 (Raring Ringtail) είναι ένα σημαντικό σημείο αναφοράς για το αγαπημένο μας παράγωγο του Ubuntu, διότι αποτελεί την πρώτη επίσημη έκδοση του Ubuntu GNOME ως μέλος της οικογενείας των παραγώγων του Ubuntu.
  </p>
  
  <p>
    Αποχαιρετούμε το Ubuntu GNOME 13.04 κοιτώντας μπροστά για νεότερες και βελτιωμένες εκδόσεις γεμάτες επιτυχίες για το Ubuntu GNOME!
  </p>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/01/raring-ringtail-logo.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/01/raring-ringtail-logo.png"  class="img-responsive" /></a>
  </div>
  
  <p>
    </div>
