---
author: iosifidis
categories:
  - Ειδήσεις
date: 2013-10-07 09:00:15
guid: http://gnome.gr/?p=2131
id: 2131
permalink: /2013/10/07/gnome-women/
tags:
  - gnome
  - GNOME Women
  - Women
title: GNOME Women
url: /2013/10/07/gnome-women/
---

## [<img class="aligncenter size-medium wp-image-2147 img-responsive " alt="GNOME Women" src="http://gnome.gr/wp-content/uploads/2013/10/gnome-women-outreach-logo-no-background-237x300.png" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/gnome-women-outreach-logo-no-background-e1410961068554.png)

## **ΤΙ ΕΙΝΑΙ**

Η <a href="https://wiki.gnome.org/GnomeWomen" target="_blank"><strong>GNOME Women</strong></a> είναι μια ομάδα γυναικών, που σκοπό έχει να ενθαρρύνει τη συνεισφορά των γυναικών στο GNOME αλλά και γενικότερα στο ελεύθερο λογισμικό / λογισμικό ανοικτού κώδικα.

## **ΓΙΑΤΙ ΧΡΕΙΑΖΕΤΑΙ ΤΕΤΟΙΑ ΟΜΑΔΑ;**

Γενικά είναι δύσκολο να γίνει κάποιος μέλος μιας κοινότητας, να μάθει την κουλτούρα της και τις διαδικασίες. Οι γυναίκες (μειονότητα στις κοινότητες ανοικτού λογισμικού) αντιμετωπίζουν πολλές προκλήσεις και συναντούν διαφορετικά εμπόδια από αυτά που συναντούν οι άντρες, όταν προσπαθήσουν να συνεισφέρουν. Επειδή οι γυναίκες είναι μειονότητα, είναι δυσκολότερο να βρουν άλλες γυναίκες που έχουν περάσει τα ίδια ή κάποιες γυναίκες που μπορούν να μοιραστούν τις εμπειρίες τους ή και ναι τις συμβουλευτούν. Σκοπός της ομάδας GNOME Women είναι να διευθετηθεί αυτή η ανάγκη.

## **ΠΟΥ ΜΠΟΡΕΙΤΕ ΝΑ ΣΥΝΕΙΣΦΕΡΕΤΕ;**

  1. Συγγραφή άρθρων σχετικά με το λογισμικό, την κοινότητα και την ανάπτυξη του GNOME.
  2. Εάν είστε καλές στην επεξεργασία εικόνας ή μπορείτε να φτιάξετε κάτι όμορφο όπως wallpapers κλπ, μπορείτε να συμμετάσχετε στην ομάδα artwork (<a href="http://art.gnome.org/" target="_blank">http://art.gnome.org/</a>).
  3. Αν και στα <a href="https://l10n.gnome.org/teams/el/" target="_blank">Ελληνικά το GNOME</a> βρίσκεται σε πολύ καλό επίπεδο, μπορείτε να βοηθήσετε σε ολοκλήρωση μεταφράσεων καθώς και να έχουμε ποιοτικότερες μεταφράσεις. Δείτε ένα φιλικό άρθρο σχετικά με την μετάφραση που αφορά την ομάδα των γυναικών <a href="https://wiki.gnome.org/GnomeWomenTrans" target="_blank">https://wiki.gnome.org/GnomeWomenTrans</a>
  4. Η ομάδα <a href="https://wiki.gnome.org/Engagement" target="_blank">GNOME Engagement</a> (πρώην Marketing) αυξάνεται και σίγουρα θα χρειαστεί περισσότερα χέρια ώστε να κατασκευαστούν υλικά όπως φυλλάδια κ.α. αλλά και οργάνωση και συμμετοχή σε εκδηλώσεις.
  5. Πιθανό να γνωρίζετε ότι οι περισσότεροι προγραμματιστές δεν δίνουν σημασία στην τεκμηρίωση, οπότε αυτό είναι μια δουλειά που πρέπει να γίνει από άλλους. Η ομάδα <a href="https://wiki.gnome.org/DocumentationProject/" target="_blank">τεκμηρίωσης GNOME</a> θέλει άτομα που θέλουν να γράψουν τεκμηρίωση για το GNOME. Μέχρι στιγμής, το αποτέλεσμα είναι εκπληκτικό για νέους αλλά και έμπειρους χρήστες με το <a href="https://library.gnome.org/" target="_blank">GNOME Library</a>.
  6. Σε περίπτωση που γνωρίζετε, μπορείτε να συμμετέχετε στην ομάδα GNOME System Administration. Υπάρχουν πολλές δραστηριότητες που μπορείτε να βοηθήσετε ώστε το <a href="http://www.gnome.org" target="_blank">gnome.org</a> αλλά και το <a href="https://static.gnome.org/gnome-gr" target="_blank">gnome.gr</a> να δουλεύουν απρόσκοπτα.
  7. Το <a href="http://live.gnome.org/GnomeLove" target="_blank">GNOME Love</a> είναι το σημείο που μπορείτε να ξεκινήσετε την συνεισφορά στο GNOME (όχι μόνο γυναίκες). Στις σελίδες αυτές θα βρείτε πληροφορίες πως να γίνετε μέλος της κοινότητας GNOME. Μπορείτε να βοηθήσετε ήδη υπάρχοντα projects του GNOME.

&nbsp;

## **ΠΩΣ ΝΑ ΞΕΚΙΝΗΣΕΤΕ**

1. Εγγραφή στην λίστα:
  
<a href="http://mail.gnome.org/mailman/listinfo/gnome-women-list" target="_blank">http://mail.gnome.org/mailman/listinfo/gnome-women-list</a>
  
2. Αποστολή ενημερωτικού mail που να αναφέρει ποια είστε, τι γνωρίζετε, που θέλετε να ασχοληθείτε (να ζητήσετε βοήθεια από αυτές που γνωρίζουν που μπορείτε να συνεισφέρετε). Αυτό είναι προαιρετικό αλλά το κάνουν στα περισσότερα projects, οπότε είναι καλό να σας γνωρίσουν.
  
3. Να μπείτε στο κανάλι:
  
**#gnome-women**, στον _irc.gnome.org_
  
και να μιλήσετε με άλλες γυναίκες.
  
4. Facebook: <a href="https://www.facebook.com/groups/118943919669/" target="_blank">https://www.facebook.com/groups/118943919669/</a>
  
5. Εγγραφή στην Ελληνική λίστα (εάν δεν έχετε γραφτεί ήδη):
  
<a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">http://lists.gnome.gr/listinfo.cgi/team-gnome.gr</a>

## **ΠΕΡΙΣΣΟΤΕΡΕΣ ΠΛΗΡΟΦΟΡΙΕΣ**

<a href="https://wiki.gnome.org/GnomeWomen" target="_blank">https://wiki.gnome.org/GnomeWomen</a>