---
author: nikosx
categories:
  - Ειδήσεις
date: 2008-06-11 14:39:41
guid: http://gnome.gr/gnome2gr/?p=21
id: 21
permalink: /2008/06/11/%cf%84%ce%b9-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%cf%84%ce%bf-gnome/
title: Τι είναι το GNOME;
url: /2008/06/11/%cf%84%ce%b9-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%cf%84%ce%bf-gnome/
---

 <span style="font-size: small">Τι είναι το GNOME; </span>

  * <span style="font-size: small">To GNOME είναι ένα γραφικό περιβάλλον επιφάνειας εργασίας για το Linux και άλλα λειτουργικά συστήματα. Η ευκολία χρήσης αλλά και η πολυγλωσσική υποστήριξη αποτελούν βασικά του χαρακτηριστικα.</span>
  *  <span style="font-size: small">Το GNOME Προσφέρει ένα μοντέρνο περιβάλλον επιφάνειας εργασίας και είναι κατάλληλο για χρήση στο σπίτι, στο γραφείο αλλά και σε επιχειρήσεις<br /> </span>
  * <span style="font-size: small">Το GNOME παρέχει μια ισχυρή πλατφόρμα ανάπτυξης εφαρμογών για προγραμματιστές<br /> </span>
  * <span style="font-size: small">Το GNOME είναι ελεύθερο/ανοικτού κώδικα λογισμικό </span>