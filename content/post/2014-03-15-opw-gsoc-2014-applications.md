---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-03-15 20:03:26
guid: http://gnome.gr/?p=2226
id: 2226
permalink: /2014/03/15/opw-gsoc-2014-applications/
tags:
  - Google Summer of Code
  - GSoC
  - OPW
  - Outreach Program for Women
title: Ήρθε η ώρα να κάνετε την αίτησή σας στο OPW και στο GSoC
url: /2014/03/15/opw-gsoc-2014-applications/
---

Με το καλοκαίρι να πλησιάζει, το GNOME βρίσκεται στην ευχάριστη θέση να σας ανακοινώσει ότι μπορείτε να κάνετε αίτηση για το **Google Summer of Code (GSoC)** και το **Outreach Program for Women**.

## Outreach Program for Women

Το _Outreach Program for Women_ προσφέρει $5500 για απασχόληση 4 μηνών σε κάποιο οργανισμό Ελεύθερου Λογισμικού / Λογισμικού Ανοικτού Κώδικα. Το πρόγραμμα δέχεται προτάσεις, όχι μόνο για projects κώδικα, αλλά και τεκμηρίωσης, γραφικών και μάρκετινγκ. Κάθε οργανισμός έχει mentors που καθοδηγούν τους συμμετέχοντες κατά τη διάρκεια της εργασίας και στην διαδικασία της υποβολής της αίτησης.

Ως μέρος της διαδικασίας υποβολής, οι αιτούσες πρέπει να συνεισφέρουν στο project που έχουν την πρόθεση να εργαστούν.

Η κοινότητα GNOME ενθαρρύνει τις ενδιαφερόμενες να επισκεφθούν την ιστοσελίδα <a href="https://gnome.org/opw/" target="_blank">Outreach Program for Women</a>, και να διαβάσει εκεί τις <a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">ιδέες-εργασίες</a>. Η ημερομηνία λήξης <a href="https://wiki.gnome.org/OutreachProgramForWomen#Application_Process" target="_blank">υποβολής αίτησης</a> αυτού του γύρου είναι η **19η Μαρτίου**.

## Google Summer of Code

Το _Google Summer of Code (GSoC)_ είναι ένα πρόγραμμα που χρηματοδοτείται από την Google. Ακριβώς όπως και το Outreach Program for Women, το GSoC προσφέρει $5500 για φοιτητές – σπουδαστές που επιθυμούν να εργαστούν για έναν οργανισμό ή project Ελεύθερου Λογισμικού / Λογισμικού Ανοικτού Κώδικα μαζί με κάποιον mentor από την κοινότητα.

Το 2014 είναι η <a href="http://google-opensource.blogspot.com/2014/03/get-with-program-open-source-coding.html" target="_blank">10η χρονιά αυτού του προγράμματος</a>, και το GNOME έχει συμμετάσχει όλα τα χρόνια. Το πρόγραμμα είναι μεγάλη ευκαιρία για τους φοιτητές να συνεισφέρουν στην κοινότητά μας, και να χρησιμοποιήσουν τις γνώσεις προγραμματισμού τους, ώστε να αναπτυχθεί το project. Υπάρχουν πολλές <a href="https://wiki.gnome.org/Outreach/SummerOfCode/2014/Ideas" target="_blank">διαθέσιμες ιδέες</a> και οι φοιτητές είναι ευπρόσδεκτοι να συζητήσουν τις δικές τους ιδέες με πιθανούς mentors!

Οι <a href="https://wiki.gnome.org/Outreach/SummerOfCode/Students" target="_blank">αιτήσεις</a> θα γίνονται δεκτές μέχρι τις **21 Μαρτίου**.

## Συμμετέχετε μαζί μας

Θα θέλαμε να σας έχουμε μαζί μας είτε στο Google Summer of Code είτε στο Outreach Program for Women!

Εάν ενδιαφέρεστε να κάνετε αίτηση για να εργαστείτε με το GNOME, επικοινωνήστε μαζί μας! Όσο πιο σύντομα κάνετε την αίτησή σας, έχετε περισσότερες πιθανότητες να επιλεγείτε και στα δυο προγράμματα.