---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
  - Παρουσίαση
date: 2013-11-11 15:14:32
guid: http://gnome.gr/?p=2200
id: 2200
permalink: /2013/11/11/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-%cf%83%cf%84%ce%b7%ce%bd-%ce%ad%ce%bd%cf%89%cf%83%ce%b7-%ce%b5%ce%bb%ce%bb%ce%ae%ce%bd%cf%89%ce%bd-%cf%87%cf%81%ce%b7%cf%83%cf%84%cf%8e/
tags:
  - gnome
  - gnome 3.10
  - gnome shell
title: Παρουσίαση στην  Ένωση Ελλήνων Χρηστών και Φίλων ΕΛ/ΛΑΚ – GreekLUG
url: /2013/11/11/%cf%80%ce%b1%cf%81%ce%bf%cf%85%cf%83%ce%af%ce%b1%cf%83%ce%b7-%cf%83%cf%84%ce%b7%ce%bd-%ce%ad%ce%bd%cf%89%cf%83%ce%b7-%ce%b5%ce%bb%ce%bb%ce%ae%ce%bd%cf%89%ce%bd-%cf%87%cf%81%ce%b7%cf%83%cf%84%cf%8e/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/11/gnome-greeklug.jpg" alt="Poster GNOME-Shell @ GreekLUG" class="aligncenter size-full wp-image-2191 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/11/gnome-greeklug.jpg)

Την Κυριακή 10 Νοεμβρίου 2013, πραγματοποιήθηκε η παρουσίαση στην έδρα της <a href="http://www.greeklug.gr" target="_blank">Ένωσης Ελλήνων Χρηστών και Φίλων ΕΛ/ΛΑΚ – GreekLUG</a>, με θέμα **«Περιβάλλον εργασίας και δράσεις κοινότητας GNOME»**. 

Τα θέματα που αναλύθηκαν ήταν σχετικά με:
  
1. Χρήση του GNOME-Shell και ειδικότερα διαφορές με την έκδοση GNOME 3.10
  
2. Παραμετροποίηση του GNOME-Shell
  
3. Δράσεις κοινότητας GNOME

Η συμμετοχή ήταν μεγάλη και υπήρξαν ερωτήσεις σχετικά με τα νέα χαρακτηριστικά που φέρνει το GNOME 3.10.

[<img src="https://lh5.googleusercontent.com/-BY_C_5O5Kfw/Uoi4rq9JH1I/AAAAAAAAJUA/UPXxa8lZ7HE/w909-h608-no/DSC_0853_.JPG" alt="Παρουσίαση GNOME-Shell" class="aligncenter img-responsive " />](https://lh5.googleusercontent.com/-BY_C_5O5Kfw/Uoi4rq9JH1I/AAAAAAAAJUA/UPXxa8lZ7HE/w909-h608-no/DSC_0853_.JPG)

Για περισσότερες φωτογραφίες, μπορείτε να δείτε είτε στην σελίδα στο <a href="https://www.facebook.com/media/set/?set=a.729770017051910.1073741826.228023513893232&type=3" target="_blank">facebook</a> ή στο <a href="https://plus.google.com/u/0/b/117283807360585416333/photos/117283807360585416333/albums/5943598910432360049" target="_blank">Google Plus</a>.

Όλες οι πληροφορίες σχετικά με την εκδήλωση βρίσκονται στην σελίδα των συνεδρίων και συγκεκριμένα στην <a href="http://gnome.gr/projects/gnome-shell-greeklug/" target="_blank">http://gnome.gr/projects/gnome-shell-greeklug/</a>.
