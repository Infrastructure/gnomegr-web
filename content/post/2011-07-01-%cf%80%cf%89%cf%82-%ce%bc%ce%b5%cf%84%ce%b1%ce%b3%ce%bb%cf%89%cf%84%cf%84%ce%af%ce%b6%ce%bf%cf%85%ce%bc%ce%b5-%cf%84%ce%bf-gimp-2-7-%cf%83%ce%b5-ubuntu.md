---
author: simosx
categories:
  - Παρουσίαση
date: 2011-07-01 19:30:37
guid: http://gnome.gr/?p=966
id: 966
permalink: /2011/07/01/%cf%80%cf%89%cf%82-%ce%bc%ce%b5%cf%84%ce%b1%ce%b3%ce%bb%cf%89%cf%84%cf%84%ce%af%ce%b6%ce%bf%cf%85%ce%bc%ce%b5-%cf%84%ce%bf-gimp-2-7-%cf%83%ce%b5-ubuntu/
tags:
  - compile
  - gimp
  - git
  - μεταγλώττιση
title: Πως μεταγλωττίζουμε το GIMP 2.7 σε Ubuntu
url: /2011/07/01/%cf%80%cf%89%cf%82-%ce%bc%ce%b5%cf%84%ce%b1%ce%b3%ce%bb%cf%89%cf%84%cf%84%ce%af%ce%b6%ce%bf%cf%85%ce%bc%ce%b5-%cf%84%ce%bf-gimp-2-7-%cf%83%ce%b5-ubuntu/
---

> Για όσους θέλουν να δοκιμάσουν το GIMP 2.7 δίχως να μπουν στη διαδικασία της μεταγλώττισης: αρκεί να προσθέσετε το PPA [https://launchpad.net/~matthaeus123/+archive/mrw-gimp-svn](https://launchpad.net/~matthaeus123/+archive/mrw-gimp-svn "https://launchpad.net/~matthaeus123/+archive/mrw-gimp-svn") και να εγκαταστήσετε το GIMP. Το παρόν άρθρο στοχεύει στους αναγνώστες που θα ήθελαν να δοκιμάσουν τη μεταγλώττιση ώστε να βοηθήσουν στην ανάπτυξη επιλύοντας σφάλματα στο <a title="https://bugzilla.gnome.org/browse.cgi?product=gimp" href="https://bugzilla.gnome.org/browse.cgi?product=gimp" target="_blank">https://bugzilla.gnome.org/browse.cgi?product=gimp</a>

Θα δούμε πως μπορούμε να μεταγλωττίσουμε την πιο πρόσφατη έκδοση του GIMP σε Ubuntu.

Δοκιμάζουμε τις οδηγίες από το [Tutorial: Compiling GIMP 2.7.2 for Ubuntu 11.04](http://www.gimpusers.com/tutorials/compiling-gimp-for-ubuntu "Μεταγλώττιση του GIMP 2.7 σε Ubuntu 11.04")

1. Κάνουμε τη λήψη του πηγαίου κώδικα του gimp από το αποθετήριο πηγαίου κώδικα git.

<pre style="padding-left: 30px">sudo apt-get install git     # εγκαθιστούμε το git, μια φορά χρειάζεται μόνο.</pre>

<pre style="padding-left: 30px">cd                           # πάμε στον γονικό κατάλογο</pre>

<pre style="padding-left: 30px">git clone git://git.gnome.org/gimp gimp-source   # θα βάλει το αποθετήριο στον κατάλογο gimp-source</pre>

Η εντολή git clone θα πάρει λίγο χρόνο για να ολοκληρωθεί. Θα γίνει λήψη γύρω στα 200ΜΒ δεδομένων.

2. Από το φλοιό που κάνουμε τη μεταγλώττιση, εκτελούμε τις εντολές (για φλοιό bash):

<pre style="padding-left: 30px">export PATH=/opt/gimp-2.7/bin:$PATH
export PKG_CONFIG_PATH=/opt/gimp-2.7/lib/pkgconfig
export LD_LIBRARY_PATH=/opt/gimp-2.7/lib
<kbd>export CFLAGS="-march=native"</kbd></pre>

Αν ανοίξουμε νέο τερματικό για να συνεχίσουμε τη μεταγλώττιση, πρέπει να τρέξουμε τις παραπάνω εντολές ξανά.

3. Μπαίνουμε στον κατάλογο με τον κώδικα του gimp και τρέξουμε

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7</pre>

Εδώ θα πει ότι λείπουν πακέτα, όπως intltool-update. Ψάχνουμε με  apt-cache search intltool για να επιβεβαιώσουμε το όνομα του εργαλείου, και το εγκαθιστούμε με

<pre style="padding-left: 30px">sudo apt-get install intltool</pre>

Πολύ πιθανό να αναφέρει για «libtool», οπότε κάνουμε την ίδια διαδικασία και προσπαθούμε ξανά. Στον υπολογιστή μου έχω εγκαταστήσει αρκετές από τις βιβλιοθήκες που χρειάζονται οπότε δεν έχω την πλήρη λίστα που χρειάζεστε.

4. Σε κάποιο σημείο, όταν τρέχουμε

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7</pre>

μέσα στο αποθετήριο του gimp, θα αναφέρει ότι λείπουν τα _**babl**_ και _**gegl**_.

Εδώ κάνουμε μια αναδρομή και εγκαθιστούμε τα δύο πακέτα, πριν συνεχίσουμε την προσπάθεια με την εγκατάσταση του gimp.

Για το babl,

<pre style="padding-left: 30px">cd</pre>

<pre style="padding-left: 30px">git clone git://git.gnome.org/babl</pre>

<pre style="padding-left: 30px">cd babl</pre>

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7
make</pre>

<pre style="padding-left: 30px">sudo make install</pre>

και για το gegl,

<pre style="padding-left: 30px">cd</pre>

<pre style="padding-left: 30px">git clone git://git.gnome.org/babl</pre>

<pre style="padding-left: 30px">cd babl</pre>

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7
make</pre>

<pre style="padding-left: 30px">sudo make install</pre>

Αυτό ήταν. Τώρα έχουμε τις πιο πρόσφατες εκδόσεις των babl, gegl, εγκατεστημένες στο /opt/gimp-2.7/, όπως θέλει το gimp.

5. Γυρίζουμε πάλι στον υποκατάλογο του gimp (gimp-source) και τρέχουμε μια ακόμα φορά την εντολή ρυθμίσεων

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7</pre>

και για να συνεχίσουμε με τις υπόλοιπες εξαρτήσεις. Για παράδειγμα, σε μένα ανέφερε ότι

<pre style="padding-left: 30px">checking for PYGTK... no
configure: error:
*** Could not find PyGTK 2.10.4 or newer.
*** Please install it, or skip building the python scripting extension by
*** passing --disable-python to configure (but then you will not be able
*** to use scripts for GIMP that are written in Python).</pre>

Κανένα πρόβλημα· τρέχουμε

<pre style="padding-left: 30px">$ apt-cache search pygtk | grep dev</pre>

<pre style="padding-left: 30px">libgtk-vnc-1.0-dev - A VNC viewer widget for GTK+ (development files)
python-gtk2-dev - GTK+ bindings: devel files</pre>

<pre style="padding-left: 30px">$ sudo apt-get install python-gtk2-dev</pre>

και τώρα έχουμε υποστήριξη και για Python στο Gimp.

Ξανά

<pre style="padding-left: 30px">./autogen.sh --prefix=/opt/gimp-2.7</pre>

και τώρα η ρύθμιση ολοκληρώνεται με επιτυχία:

<pre style="padding-left: 30px">Building GIMP with prefix=/opt/gimp-2.7, datarootdir=${prefix}/share
Desktop files install into ${datarootdir}

Extra Binaries:
 gimp-console:        yes
 gimp-remote:         no (not enabled)

Optional Features:
 D-Bus service:       yes
 Language selection:  yes

Optional Plug-Ins:
 Ascii Art:           no (AA library not found)
 Help Browser:        no (WebKit not found)
 LCMS:                no (lcms not found or unusable)
 JPEG:                yes
 JPEG 2000:           yes
 MNG:                 no (MNG header file not found)
 PDF (import):        Using PostScript plug-in (libpoppler not found)
 PDF (export):        yes
 PNG:                 yes
 Print:               yes
 PSP:                 yes
 Python:              yes
 Script-Fu:           yes
 SVG:                 yes
 TIFF:                yes
 TWAIN (MacOS X):     no
 TWAIN (Win32):       no
 URI:                 yes (using GIO/GVfs)
 Webpage:             no (WebKit not found)
 Windows ICO:         yes
 WMF:                 no (libwmf not found)
 XJT:                 yes
 X11 Mouse Cursor:    yes
 XPM:                 no (XPM library not found)

Plug-In Features:
 EXIF support:        yes

Optional Modules:
 ALSA (MIDI Input):   no (libasound not found or unusable)
 Linux Input:         yes (GUdev support: yes)
 DirectInput (Win32): no
 Color Correction:    no (lcms not found or unusable)
 Soft Proof:          no (lcms not found or unusable)

Tests:
 Use xvfb-run         no (not found)</pre>

<pre style="padding-left: 30px"><strong>Now type 'make' to compile the GNU Image Manipulation Program.</strong></pre>

Βλέπουμε ότι υπάρχουν κάποια πράγματα που λείπουν· μπορούμε να εγκαταστήσουμε τα αντίστοιχα πακέτα ανάπτυξης (development packages) και να τρέξουμε autogen.sh ξανά. Για παράδειγμα, αυτό [το LCMS σχετίζεται με τα χρώματα και τη διαχείριση χρωμάτων](http://www.littlecms.com/ "Δικτυακός τόπος LCMS"). Προσωπικά δεν το χρειάζομαι για τώρα (μπορεί να μπει μετά με πλήρη επαναμεταγλώττιση) οπότε συνεχίζουμε.

6. Τώρα τρέξουμε

<pre style="padding-left: 30px">make</pre>

Αν έχετε καλό επεξεργαστή, με πολλούς πυρήνες, μπορείτε να δοκιμάσετε π.χ.

<pre style="padding-left: 30px">make -j5</pre>

όπου τρέχουν 5 διεργασίες παράλληλα (για τετραπύρηνο επεξεργαστή).

Σε αυτό το σημείο περιμένετε, και χρονομετράτε τη μεταγλώττιση.

Μετά από 13 λεπτά (για μένα!), η μεταγλώττιση ολοκληρώθηκε και εγκαθιστούμε με

<pre style="padding-left: 30px">sudo make install</pre>

και αυτό ήταν!

7. Τώρα τρέχουμε το φρεσκο-μεταγλωττισμένο νέο Gimp 2.7 με

<pre style="padding-left: 30px">/opt/gimp-2.7/bin/gimp-2.7</pre>

Αμέσως παρατηρούμε ότι υπάρχει επιλογή στο gimp για λειτουργία ενός παραθύρου. Η προεπιλογή (για τώρα) είναι ο παραδοσιακός τρόπος με τα πολλαπλά παράθυρα.

[<img class="size-medium wp-image-967 img-responsive " title="GIMP 2.7.3 στα ελληνικά, φρεσκο-μεταγλωττισμένο από το αποθετήριο κώδικα" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/gimp273-300x235.png" alt="GIMP 2.7.3 στα ελληνικά, φρεσκο-μεταγλωττισμένο από το αποθετήριο κώδικα" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/07/gimp273.png) 

Πατήστε παραπάνω για να δείτε την εικόνα σε πλήρες μέγεθος.

<div id="_mcePaste" class="mcePaste" style="width: 1px;height: 1px;overflow: hidden">
  <span class="Yd"><span class="ze">rolling αναβαθμίσεις</span></span>
</div>
