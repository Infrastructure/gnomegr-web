---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
date: 2015-02-17 13:02:09
guid: http://gnome.gr/?p=2991
id: 2991
permalink: /2015/02/17/guadec-2015-august-7-9-gothenburg-sweden/
tags:
  - Gothenburg
  - GUADEC
  - Sweden
  - Γκέτενμποργκ
  - Σουηδία
title: Το συνέδριο GUADEC θα διεξαχθεί στις 7 – 9 Αυγούστου στο Γκέτενμποργκ
url: /2015/02/17/guadec-2015-august-7-9-gothenburg-sweden/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/02/Gothenburg-By-Night.jpg" alt="Γκέτενμποργκ τη νύχτα" title="Γκέτενμποργκ τη νύχτα" class="size-full wp-image-2992 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/02/Gothenburg-By-Night.jpg) 

Με χαρά σας ανακοινώνουμε ότι το συνέδριο GUADEC 2015 θα διεξαχθεί στο Γκετενμποργκ της Σουηδίας από 7 έως 9 Αυγούστου, στο συνεδριακό κέντρο <a href="http://folketshusgoteborg.se/" target="_blank">Folkets Hus</a>.

> «Καλωσορίζουμε την κοινότητα του GNOME στο Γκέτενμποργκ. Το GNOME ήταν μέρος του τοπικού γυνεδρίου Ανοικτού Λογισμικού, FSCONS, γι’αυτό είναι καταπληκτικό να διοργανώσουμε το συνέδριο GUADEC αυτό το χρόνο.»
> 
> – Oliver Propst, τοπικός οργανωτής 

Το Γκέτενμποργκ είναι η δεύτερη μεγαλύτερη πόλη της Σουηδίας, βρίσκεται δε στην δυτική Σουηδική ακτή. Η πόλη διαθέτει μια πλούσια ιστορία στη ναυτική και αυτοκινητοβιομηχανία, και αυτές τις μέρες είναι το κέντρο της τεχνολογίας με την ακμάζουσα κοινότητα ανοικτού λογισμικού.

**Συμμετοχή!**
  
Έχετε κάποια ιδέα ή κάποιο έργο που θα θέλατε να μοιραστείτε με την κοινότητα στην διάρκεια του συνεδρίου GUADEC; Μείνετε συντονισμένοι. Θα ανακοινώσουμε σύντομα το κάλεσμα για συμμετοχή.

Εάν η εταιρία ή ο οργανισμός σας θα ήθελε να είναι χορηγός στο GUADEC, μπορείτε να βρείτε πληροφορίες σχετικά με τις <a href="https://www.guadec.org/sponsors/" target="_blank">χορηγίες στην ιστοσελίδα GUADEC.org</a>.

Εάν θέλετε να συμμετάσχετε στην ομάδα μας και να βοηθήσετε στην οργάνωση του συνεδρίου GUADEC, <a href="https://2015.guadec.org/volunteer/" target="_blank">επισκεφθείτε την ιστοσελίδα</a>. Θα σας θέλαμε μαζί μας!

Φωτογραφία: “Gothenburg by Night” του Rob Sinclair, CC-BY-SA 2.0
  
Πηγή: <a href="http://www.gnome.org/news/2015/02/guadec-2015-to-happen-from-august-7-9-in-gothenburg-sweden/" target="_blank">GNOME.org</a>
