---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/1381489754579891878
blogger_permalink:
  - /2014/10/wallpaper-1410-results.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-10-05 12:09:00
guid: http://gnome.gr/2014/10/05/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%ce%bf%cf%8d-wallpapers-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%ad/
id: 2894
image: /wp-content/uploads/sites/7/2014/10/littleball.jpg
permalink: /2014/10/05/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%ce%bf%cf%8d-wallpapers-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%ad/
title: Αποτελέσματα διαγωνισμού wallpapers για την έκδοση 14.10
url: /2014/10/05/%ce%b1%cf%80%ce%bf%cf%84%ce%b5%ce%bb%ce%ad%cf%83%ce%bc%ce%b1%cf%84%ce%b1-%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%ce%bf%cf%8d-wallpapers-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%ad/
---

Όπως είχαμε αναφέρει σε <a href="http://gnome.gr/2014/08/27/%ce%b4%ce%b9%ce%b1%ce%b3%cf%89%ce%bd%ce%b9%cf%83%ce%bc%cf%8c%cf%82-wallpaper-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-ubuntu-gnome-14-10-utopic-unicorn/" target="1">προηγούμενη ανάρτηση</a>, διεξήχθη διαγωνισμός για wallpaper για την 14.10 με Ελληνική συμμετοχή.

Τα αποτελέσματα του διαγωνισμού βρίσκονται όλα συγκεντρωμένα στο <a href="https://dl.dropboxusercontent.com/u/20413076/ubuntu/gnome/14.10/ubuntu_gnome_wallpapers.zip" target="1">αρχείο zip (περίπου 70ΜΒ)</a>.  
Δυστηχώς η Ελληνική συμμετοχή δεν βρίσκεται μέσα στα wallpapers της έκδοσης 14.10.

Θα θέλαμε να συγχαρούμε τον Χρήστο Ιωσηφίδη για την συμμετοχή του και να τον ενθαρύνουμε να συμμετάσχει και σε επόμενους διαγωνισμούς. Επίσης θα θέλαμε να συγχαρούμε τους νικητές του διαγωνισμού.

Δείτε ένα δείγμα των wallpapers:  
[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/littleball.jpg"  class="img-responsive" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/littleball.jpg)
