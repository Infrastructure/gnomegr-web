---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/4256129090926532248
blogger_permalink:
  - /2014/08/utopic-unicorn-alpha2.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-08-01 19:00:00
guid: http://gnome.gr/2014/08/01/utopic-unicorn-alpha2/
id: 2898
permalink: /2014/08/01/utopic-unicorn-alpha2/
title: Utopic Unicorn Alpha2
url: /2014/08/01/utopic-unicorn-alpha2/
---

Η <a href="http://distrowatch.com/table.php?distribution=ubuntugnome" target="1">Ομάδα Ubuntu GNOME</a> σας ανακοινώνει την κυκλοφορία της έκδοσης **Ubuntu GNOME Utopic Unicorn Alpha 2**.

Παρακαλούμε διαβάστε τις <a href="https://wiki.ubuntu.com/UtopicUnicorn/Alpha2/UbuntuGNOME" target="1">σημειώσεις της έκδοσης</a>.

**ΣΗΜΕΙΩΣΗ:**

Αυτή είναι η έκδοση Alpha 2 Release. Οι εκδόσεις _Ubuntu GNOME Alpha_ **ΔΕΝ** συνιστούνται για:

* Καθημερινούς χρήστες που δεν γνωρίζουν για τα πιθανά προβλήματα των προ-εκδόσεων.  
* Οποιονδήποτε χρειάζεται σταθερό σύστημα.  
* Οποιονδήποτε δεν θέλει να έχει πιθανώς συχνά χαλασμένο σύστημα.  
* Οποιονδήποτε σε παραγωγικό περιβάλον που χειρίζεται δεδομένα ή έχει διάφορα projects και θέλει σταθερότητα

Οι εκδόσεις Ubuntu GNOME Alpha συνιστούνται για:

* Χρήστες που θέλουν να βοηθήσουν δοκιμάζοντας την έκδοση, υποβάλλοντας πιθανά σφάλματα ώστε να διορθωθούν.  
* Προγραμματιστές Ubuntu GNOME

Δείτε περισσότερες πληροφορίες στο <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">Ubuntu GNOME Wiki Page</a>.

Εάν θέλετε να επικοινωνήσετε μαζί μας, μπορείτε να το κάνετε στα <a href="https://wiki.ubuntu.com/UbuntuGNOME/ContactUs" target="1">κανάλια επικοινωνίας</a>.