---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3755223057415440946
blogger_permalink:
  - /2014/01/ubuntu-gnome-trusty-tahr-1404-alpha-2.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-01-26 15:32:00
guid: http://gnome.gr/2014/01/26/ubuntu-gnome-trusty-tahr-14-04-alpha-2/
id: 2905
image: /wp-content/uploads/sites/7/2014/01/ubuntu-14-04-trusty_tahr.jpg
permalink: /2014/01/26/ubuntu-gnome-trusty-tahr-14-04-alpha-2/
title: Ubuntu GNOME Trusty Tahr (14.04) Alpha 2
url: /2014/01/26/ubuntu-gnome-trusty-tahr-14-04-alpha-2/
---

<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/01/ubuntu-14-04-trusty_tahr-300x169.jpg"  class="img-responsive" />  
Με χαρά σας ανακοινώνουμε την έλευση της έκδοσης Ubuntu GNOME Trusty Tahr (14.04) Alpha 2.

Διαβάστε περισσότερα για το τι περικλείει η έκδοση Alpha 2, στην ιστοσελίδα <a href="https://wiki.ubuntu.com/TrustyTahr/Alpha2/UbuntuGNOME" target="1">wiki για το Ubuntu GNOME</a>.

Μπορείτε να κατεβάσετε το ISO από την <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/trusty/alpha-2/" target="1">ιστοσελίδα των λήψεων</a>, να το γράψετε σε ένα δισκάκι (<a href="https://help.ubuntu.com/community/BurningIsoHowto" target="1">δείτε πως μπορείτε να το κάνετε</a>) και να το δοκιμάσετε (<a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">δείτε τι δοκιμές μπορείτε να κάνετε</a>).

Στείλτε τα αποτελέσματα των δοκιμών σας (τι σας άρεσε περισσότερο, τι είναι αυτό που θέλει διόρθωση, <a href="https://help.ubuntu.com/community/ReportingBugs" target="1">σφάλματα</a> κλπ) στους τρόπους επικοινωνίας που μπορείτε να <a href="https://wiki.ubuntu.com/UbuntuGNOME/ContactUs" target="1">βρείτε εδώ</a>.