---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/4530641751054188903
blogger_permalink:
  - /2014/10/upgrade-1404-to-1410-or-not.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-10-24 11:01:00
guid: http://gnome.gr/2014/10/24/%ce%bd%ce%b1-%ce%b1%ce%bd%ce%b1%ce%b2%ce%b1%ce%b8%ce%bc%ce%af%cf%83%cf%89-%ce%b1%cf%80%cf%8c-14-04-%cf%83%ce%b5-14-10-%ce%ae-%cf%8c%cf%87%ce%b9/
id: 2892
image: /wp-content/uploads/sites/7/2014/10/utopic.jpg
permalink: /2014/10/24/%ce%bd%ce%b1-%ce%b1%ce%bd%ce%b1%ce%b2%ce%b1%ce%b8%ce%bc%ce%af%cf%83%cf%89-%ce%b1%cf%80%cf%8c-14-04-%cf%83%ce%b5-14-10-%ce%ae-%cf%8c%cf%87%ce%b9/
title: Να αναβαθμίσω από 14.04 σε 14.10 ή όχι;
url: /2014/10/24/%ce%bd%ce%b1-%ce%b1%ce%bd%ce%b1%ce%b2%ce%b1%ce%b8%ce%bc%ce%af%cf%83%cf%89-%ce%b1%cf%80%cf%8c-14-04-%cf%83%ce%b5-14-10-%ce%ae-%cf%8c%cf%87%ce%b9/
---

<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/10/utopic-300x200.jpg" height="40%" width="40%"  class="img-responsive" />  
Το καίριο ερώτημα σε κάθε νέα έκδοση. Να αναβαθμίσω ή όχι;

Με το Ubuntu GNOME δεν χρειάζεται ανησυχία. Η ζωή σας γίνεται ευκολότερη. Διαβάστε το παρόν, και θα καταλάβετε γιατί.

Αυτή την στιγμή, το Ubuntu GNOME έχει 2 κύριες εκδόσεις:

Ubuntu GNOME **14.04 (Trusty Tahr) LTS**.  
Ubuntu GNOME **14.10 (Utopic Unicorn)**.

Η έκδοση <a href="http://gnome.gr/2014/04/17/%ce%b7-ubuntu-gnome-14-04-lts-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b4%ce%b9%ce%b1%ce%b8%ce%ad%cf%83%ce%b9%ce%bc%ce%b7-%cf%80%cf%81%ce%bf%cf%82-%ce%bb%ce%ae%cf%88%ce%b7/" target="1">Ubuntu 14.04 LTS</a> ήταν διαθέσιμη από τον Απρίλιο 2014, ενώ η <a href="http://gnome.gr/2014/10/23/%ce%ba%cf%85%ce%ba%ce%bb%ce%bf%cf%86%cf%8c%cf%81%ce%b7%cf%83%ce%b5-%ce%b7-ubuntu-gnome-14-10-utopic-unicorn/" target="1">Ubuntu 14.10</a> κυκλοφόρησε μόλις χθες.

Η απάντηση στο ερώτημα είναι πολύ απλή:

* Εάν θέλετε να χρησιμοποιείτε τα τελευταία πακέτα/λογισμικό που προσφέρεται με την τελευταία έκδοση του **Ubuntu GNOME** και δεν σας πειράζει το «μικρό» διάστημα υποστήριξης (<a href="https://wiki.ubuntu.com/UtopicUnicorn/ReleaseNotes/UbuntuGNOME#Support" target="1">9 μήνες</a>), **ΤΟΤΕ** αναβαθμίστε. <a href="https://wiki.ubuntu.com/UtopicUnicorn/ReleaseNotes/UbuntuGNOME#Upgrading_from_last_release" target="1">Διαβάστε πως</a> (βασικά με την εντολή **sudo update-manager -d**).

* Εάν θέλετε να έχετε ένα σταθερό σύστημα με μακρά υποστήριξη λογισμικού (<a href="https://wiki.ubuntu.com/TrustyTahr/ReleaseNotes/UbuntuGNOME#Support" target="1">3 χρόνια</a>) και δεν σας ενδιαφέρουν τα νέα πακέτα/λογισμικό, **ΤΟΤΕ** μην αναβαθμίσετε. Μείνετε στην **Ubuntu GNOME 14.04 LTS**.

_ΣΗΜΕΙΩΣΗ:_ Στη λίστα με το νέο λογισμικό, συμπεριλαμβάνεται και η έκδοση GNOME. Η 14.04 έχει την έκδοση GNOME 3.10 (μπορείτε βέβαια να προσθέσετε ppa και να έχετε την 3.12). Η έκδοση 14.10, περιέχει το GNOME 3.12.

Λύθηκε το μυστήριο. Πρέπει να ρωτήσετε τον εαυτό σας:

**Τι χρειάζομαι;**

Τελευταία έκδοση ή έκδοση με μακράς διάρκειας υποστήριξη; Μόνο εσείς μπορείτε να απαντήσετε σε αυτό το ερώτημα.  
Δεν υπάρχει «λάθος» απάντηση στο ερώτημα.

_ΣΗΜΕΙΩΣΗ:_ Σε περίπτωση που αποφασίσετε να αναβαθμίσετε, να έχετε υπόψιν ότι κάτι μπορεί να πάει στραβά. Εάν δεν έχετε δημιουργήσει κατατμήσεις όπως αναφέρεται <a href="http://gnome.gr/p/ubuntu-gnome.html" target="1">στις οδηγίες</a> (διαφορετική κατάτμηση το /home) τότε πιθανό να χάσετε αρχεία.

Σας ευχαριστούμε που επιλέξατε το Ubuntu GNOME.