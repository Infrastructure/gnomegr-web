---
author: iosifidis
categories:
  - Ειδήσεις
  - Εκδηλώσεις
  - Παρουσίαση
date: 2013-11-08 00:44:14
guid: http://gnome.gr/?p=2193
id: 2193
image: /wp-content/uploads/sites/7/2013/11/gnome-greeklug.jpg
permalink: /2013/11/08/gnome-shell-greeklug/
tags:
  - gnome
  - gnome 3
  - gnome shell
title: Παρουσίαση του περιβάλλοντος εργασίας και κοινότητας GNOME στον σύλλογο GreekLUG
url: /2013/11/08/gnome-shell-greeklug/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/11/gnome-greeklug.jpg" alt="Poster GNOME-Shell @ GreekLUG" class="aligncenter size-full wp-image-2191 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/11/gnome-greeklug.jpg)

Την Κυριακή 10 Νοεμβρίου 2013 στις 13.00, θα πραγματοποιηθεί παρουσίαση στην έδρα της <a href="http://www.greeklug.gr" target="_blank">Ένωσης Ελλήνων Χρηστών και Φίλων ΕΛ/ΛΑΚ – GreekLUG</a>, με θέμα **«Περιβάλλον εργασίας και δράσεις κοινότητας GNOME»**. Ομιλητής θα είναι ο <a href="https://live.gnome.org/EfstathiosIosifidis" target="_blank">Ευστάθιος Ιωσηφίδης</a>. 

Τα θέματα που θα αναλυθούν είναι:
  
1. Χρήση του GNOME-Shell
  
2. Παραμετροποίηση του GNOME-Shell
  
3. Δράσεις κοινότητας GNOME

Όσοι ενδιαφέρονται να παρευρεθούν στην παρουσίαση, η έδρα του συλλόγου είναι στην οδό Εγνατίας 96, Πυλαία (<a href="http://www.greeklug.gr/index.php?option=com_content&view=article&id=50&Itemid=69&lang=el" target="_blank">δείτε τον χάρτη εδώ</a>).

Μπορείτε να δηλώσετε συμμετοχή στην εκδήλωση στο <a href="https://www.facebook.com/events/176622965877301/" target="_blank">facebook</a> ή στο <a href="https://plus.google.com/events/cont5v07c1emmfijkocll7usors" target="_blank">Google +</a> (απλά για να γνωρίζουν οι διοργανωτές πόσα περίπου άτομα θα φιλοξενήσουν).
