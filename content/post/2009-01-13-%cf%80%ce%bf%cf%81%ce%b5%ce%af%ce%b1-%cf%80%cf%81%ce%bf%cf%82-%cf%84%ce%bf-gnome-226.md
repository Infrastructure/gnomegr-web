---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2009-01-13 16:35:37
guid: http://gnome.gr/?p=145
id: 145
permalink: /2009/01/13/%cf%80%ce%bf%cf%81%ce%b5%ce%af%ce%b1-%cf%80%cf%81%ce%bf%cf%82-%cf%84%ce%bf-gnome-226/
title: Πορεία προς το GNOME 2.26
url: /2009/01/13/%cf%80%ce%bf%cf%81%ce%b5%ce%af%ce%b1-%cf%80%cf%81%ce%bf%cf%82-%cf%84%ce%bf-gnome-226/
---

Ο <a href="http://www.andreasn.se/blog/?p=93" target="_blank">Andreas Nilsson έφτιαξε μια εικόνα που δείχνει τα στάδια</a> από τα οποία θα περάσει το GNOME για να παραχθεί το GNOME 2.26.

<a href="http://www.andreasn.se/blog/images/gnome-timeline.png" target="_blank"><img src="http://www.andreasn.se/blog/images/gnome-timeline-thumb.png" alt="GNOME release timeline"  class="img-responsive" /></a>

Μπορούμε να δούμε το διάστημα που έχει να κάνει με το λεγόμενο string freeze, όπου οι προγραμματιστές δε θα κάνουν αλλαγές στα μυνήματα και οι μεταφραστές είναι σε πλήρη μεταφραστική ταχύτητα. Ωστόσο, ακόμα και τώρα είναι εντάξει να παραδίδουμε μεταφράσεις.

(στη σελίδα του Andreas μπορείτε να βρείτε το πηγαίο αρχείο .svg όπου με το Inkscape μπορείτε να κάνετε επεξεργασία και να φτιάξετε παρόμοια έργα).