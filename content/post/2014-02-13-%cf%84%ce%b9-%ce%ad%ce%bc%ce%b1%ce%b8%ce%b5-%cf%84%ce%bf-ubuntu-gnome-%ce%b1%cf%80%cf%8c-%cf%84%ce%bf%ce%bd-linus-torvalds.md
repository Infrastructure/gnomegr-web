---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/1763316998015638328
blogger_permalink:
  - /2014/02/ubuntu-gnome-linus-torvalds.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-02-13 09:53:00
guid: http://gnome.gr/2014/02/13/%cf%84%ce%b9-%ce%ad%ce%bc%ce%b1%ce%b8%ce%b5-%cf%84%ce%bf-ubuntu-gnome-%ce%b1%cf%80%cf%8c-%cf%84%ce%bf%ce%bd-linus-torvalds/
id: 2904
permalink: /2014/02/13/%cf%84%ce%b9-%ce%ad%ce%bc%ce%b1%ce%b8%ce%b5-%cf%84%ce%bf-ubuntu-gnome-%ce%b1%cf%80%cf%8c-%cf%84%ce%bf%ce%bd-linus-torvalds/
title: Τι έμαθε το Ubuntu GNOME από τον Linus Torvalds
url: /2014/02/13/%cf%84%ce%b9-%ce%ad%ce%bc%ce%b1%ce%b8%ce%b5-%cf%84%ce%bf-ubuntu-gnome-%ce%b1%cf%80%cf%8c-%cf%84%ce%bf%ce%bd-linus-torvalds/
---

<div dir="ltr" style="text-align: left">
  <div style="clear: both;text-align: center">
  </div>
  
  <p>
    Τι έμαθε το Ubuntu GNOME από τον Linus Torvalds:
  </p>
  
  <ul style="text-align: left">
    <li>
      Μην κάνουμε μεγάλα όνειρα.
    </li>
    <li>
      Να μοιραζόμαστε.
    </li>
    <li>
      Να μην έχουμε σχέδιο.
    </li>
    <li>
      Να μην στοχεύουμε στην επιτυχία, αλλά να κάνουμε αυτό που πιστεύουμε και αγαπάμε και η επιτυχία θα έρθει από μόνη της.
    </li>
    <li>
      Όλοι μαζί, είμαστε πιο έξυπνοι από ότι ο καθένας από εμάς.
    </li>
  </ul>
  
  <div>
  </div>
  
  <h2 style="text-align: left">
    Αλλά τι σχέση έχουν αυτά με το Ubuntu GNOME;
  </h2>
  
  <div>
    Απλούστατα, αυτά τα μαθήματα είναι αυτά που πιστεύουμε και δουλέυουμε για αυτά καθημερινά. Για να γίνεις ο καλύτερος, πρέπει να μάθεις πως να το κάνεις. Αφού το μάθεις, πρέπει να το ακουθήσεις και να το πραγματοποιήσεις. Πρέπει να συνεχίσεις να κάνεις αυτό που πιστεύεις και αγαπάς και η επιτυχία θα ακολουθήσει. Γιατί; Επειδή όλοι μαζί, είμαστε εξυπνότεροι από ότι ο καθένας ξεχωριστά και έτσι θα πετύχουμε τους στόχους μας.
  </div>
  
  <div>
  </div>
  
  <div>
    Το <b>Ubuntu GNOME </b>πιστεύει σε αυτά τα μαθήματα και δεν θα εγκαταλήψει το όνειρο του να γίνει η καλύτερη διανομή Linux που υπάρχει.
  </div>
  
  <div>
  </div>
  
  <div>
    Αυτό έμαθε η κοινότητα από τον Linus Torvalds.
  </div>
  
  <div>
  </div>
  
  <div>
    Αν πιστεύετε και εσείς σε αυτά τα μαθήματα, ελάτε να συμμετάσχετε σε μία από τις πιο ενδιαφέρουσες, καταπληκτικές και ταχύτατα ανεπτυσσόμενες κοινότητες, την κοινότητα του Ubuntu GNOME.
  </div>
  
  <div>
  </div>
  
  <div>
    Aκόμα και αν δεν θέλετε να γίνετε μέλος της κοινότητας του Ubuntu GNOME, βοηθήστε στην προώθηση του ελεύθερου λογισμικού κοινοποιώντας αυτό το Video προς τιμην του <a href="http://el.wikipedia.org/wiki/%CE%9B%CE%AF%CE%BD%CE%BF%CF%85%CF%82_%CE%A4%CF%8C%CF%81%CE%B2%CE%B1%CE%BB%CE%BD%CF%84%CF%82">Linus Torvalds</a>, του δημιουργού ενός από τα πιο προηγμένα λογισμικά στον κόσμο σήμερα, του πυρήνα <a href="http://el.wikipedia.org/wiki/Linux">Linux</a>.
  </div>
</div>