---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/856700873125487529
blogger_permalink:
  - /2013/10/ubuntu-gnome-1310-saucy-salamander.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-10-17 19:53:00
guid: http://gnome.gr/2013/10/17/%cf%84%ce%bf-ubuntu-gnome-13-10-saucy-salamander-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b5%ce%b4%cf%8e/
id: 2909
permalink: /2013/10/17/%cf%84%ce%bf-ubuntu-gnome-13-10-saucy-salamander-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b5%ce%b4%cf%8e/
title: Το Ubuntu GNOME 13.10 (Saucy Salamander) είναι εδώ!
url: /2013/10/17/%cf%84%ce%bf-ubuntu-gnome-13-10-saucy-salamander-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b5%ce%b4%cf%8e/
---

<div dir="ltr" style="text-align: left">
  Η ομάδα του Ubuntu GNOME με χαρά ανακοίνωσε σήμερα την κυκλοφορία της νέας έκδοσης Ubuntu GNOME 13.10 (Saucy Salamander)!<br />Η ομάδα έχει ως στόχο να φέρει στην επιφάνεια εργασίας μας το αυθεντικό GNOME, ενώ σε συνεργασία με την ομάδα του Ubuntu Desktop, αποφασίστηκε να μείνει το GNOME 3.8 στην 13.10.<br />Δείτε τις <a href="https://help.gnome.org/misc/release-notes/3.8/">σημειώσεις της έκδοσης</a> για τα χαρακτηριστικά του GNOME 3.8.</p> 
  
  <h2 style="text-align: left">
    Ανακοίνωση και σημειώσεις έκδοσης
  </h2>
  
  <div style="text-align: left">
    Το μήνυμα που ανακοινώνει την κυκλοφορία της νέας έκδοσης είναι <a href="https://lists.ubuntu.com/archives/ubuntu-announce/2013-October/000177.html" target="_blank">αυτό</a>, ενώ οι σημειώσεις για την έκδοση βρίσκονται <a href="https://wiki.ubuntu.com/SaucySalamander/ReleaseNotes/UbuntuGNOME" target="_blank">εδώ</a>.
  </div>
  
  <div style="text-align: left">
  </div>
  
  <div>
    <div>
      <h2 style="text-align: left">
        Λήψη Ubuntu GNOME 13.10
      </h2>
      
      <div style="text-align: left">
        Για να εγκαταστήσετε το Ubuntu GNOME 13.10 (Saucy Salamander) επιλέξτε:<b>&nbsp;</b>
      </div>
      
      <div style="text-align: left">
        <b>&nbsp;&nbsp;&nbsp; </b>
      </div>
      
      <p>
        <b>Απευθείας λήψη:</b>&nbsp;&nbsp;
      </p>
      
      <ul style="text-align: left">
        <li>
          <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/release/ubuntu-gnome-13.10-desktop-amd64.iso">Ubuntu GNOME 13.10 64bit (EFI Support)</a>
        </li>
        <li>
          <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/release/ubuntu-gnome-13.10-desktop-i386.iso">Ubuntu GNOME 13.10 32bit</a>
        </li>
      </ul>
      
      <div style="text-align: left">
        <b>Αρχείο torrent:</b>
      </div>
      
      <ul style="text-align: left">
        <li>
          <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/release/ubuntu-gnome-13.10-desktop-amd64.iso.torrent">Ubuntu GNOME 13.10 64bit (EFI Support)</a>
        </li>
        <li>
          <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/13.10/release/ubuntu-gnome-13.10-desktop-i386.iso.torrent">Ubuntu GNOME 13.10 32bit</a>
        </li>
      </ul>
      
      <h2 style="text-align: left">
        Επικοινωνία με την ομάδα
      </h2>
      
      <div style="text-align: left">
        Μπορείτε να δείτε <a href="https://wiki.ubuntu.com/UbuntuGNOME/ContactUs">εδώ</a> την πλήρη λίστα με τα κανάλια επικοινωνίας.
      </div>
      
      <h2 style="text-align: left">
        Σας ευχαριστούμε
      </h2>
      
      <div style="text-align: left">
        Ευχαριστούμε όλους όσους συμμετείχαν στην ανάπτυξη και στις δοκιμές αυτής της έκδοσης.
      </div>
      
      <div style="text-align: left">
        Ειδικότερες ευχαριστίες στα άτομα που δοκιμάσανε την υπό ανάπτυξη έκδοση και κάνανε καταπληκτική δουλειά, ως αποτέλεσμα των προσπαθειών, έχουμε μια έκδοση που είμαστε περήφανοι.
      </div>
      
      <div style="text-align: left">
      </div>
      
      <div style="text-align: left">
        Σας ευχαριστούμε που επιλέξατε και χρησιμοποιείτε το Ubuntu GNOME.
      </div>
      
      <div style="text-align: left">
      </div>
      
      <div style="text-align: left">
        Σε επόμενο άρθρο θα ακολουθήσει παρουσίαση του Ubuntu GNOME 13.10, οδηγός εγκατάστασης και συμβουλές για τις πρώτες ρυθμίσεις μετά την εγκατάσταση.
      </div>
    </div>
  </div>
  
  <div style="text-align: left">
  </div>
</div>