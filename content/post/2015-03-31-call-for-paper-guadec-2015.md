---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2015-03-31 15:09:44
guid: https://static.gnome.org/gnome-gr/?p=3160
id: 3160
permalink: /2015/03/31/call-for-paper-guadec-2015/
tags:
  - events
  - gnome
  - gnome shell
  - GUADEC
  - Γκέτενμποργκ
  - συνέδριο
title: Έναρξη υποβολής προτάσεων για παρουσιάσεις στο συνέδριο GUADEC 2015
url: /2015/03/31/call-for-paper-guadec-2015/
---

[<img class="size-full wp-image-3163 aligncenter img-responsive " src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/03/guadec_2015.png" alt="guadec_2015" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2015/03/guadec_2015.png)

Το συνέδριο GUADEC είναι το ετήσιο συνέδριο της κοινότητας GNOME, που διεξάγεται στην Ευρώπη από το 2000. Φέτος το συνέδριο <a href="https://www.guadec.org/" target="_blank">GUADEC 2015</a> θα διεξαχθεί για πρώτη φορά στο Γκέτενμποργκ της Σουηδίας <a href="https://static.gnome.org/gnome-gr/2015/02/17/guadec-2015-august-7-9-gothenburg-sweden/" target="_blank">από 7 έως 9 Αυγούστου</a>, με επιπρόσθετες ημέρες μετά το συνέδριο για συναντήσεις και hackfests. Παλιότερα είχαμε εκπροσώπηση της ελληνικής κοινότητας του GNOME στα συνέδρια GUADEC, το <a href="http://simos.info/blog/archives/563" target="_blank">2006</a> στην Βαρκελώνη, το <a href="https://mail.gnome.org/archives/gnome-el-list/2007-July/msg00017.html" target="_blank">2007</a> στο Μπέρμιγχαμ και το <a href="https://static.gnome.org/gnome-gr/2008/07/10/9%CF%84%CE%BF-%CF%83%CF%85%CE%BD%CE%AD%CE%B4%CF%81%CE%B9%CE%BF-gnome-guadec-%CF%83%CF%84%CE%B7%CE%BD-%CE%BA%CF%89%CE%BD%CF%83%CF%84%CE%B1%CE%BD%CF%84%CE%B9%CE%BD%CE%BF%CF%8D%CF%80%CE%BF%CE%BB%CE%B7/" target="_blank">2008</a> στην Κωνσταντινούπολη, οπότε αν ενδιαφέρεστε παρακάτω θα βρείτε όλες τις απαραίτητες πληροφορίες για να δηλώσετε συμμετοχή ως ομιλητής (εθελοντής ή και ως απλός επισκέπτης).
  
&nbsp;
  
Το πρόγραμμα αντικατοπτρίζει την ανάπτυξη που συμβαίνει στο έργο GNOME:

  * Σχεδιασμός της βασικής εμπειρίας χρήσης (βασικές εφαρμογές, κέλυφος GNOME)
  * Σχεδιασμός της εμπειρίας ανάπτυξης του GNOME (εργαλεία ανάπτυξης, τεκμηρίωση)
  * Ενσωμάτωση των επεκτάσεων του συστήματος στην βασική εμπειρία χρήσης
  * Ανάπτυξη εφαρμογών
  * Ενσωμάτωση τεχνολογιών διαδικτύου στο GNOME
  * Εύρεση νέων εθελοντών που θέλουν να συνεισφέρουν στο έργο
  * Οργάνωση και διαχείριση του έργου

&nbsp;
  
Τα θέματα που δεν ταιριάζουν στις παραπάνω κατηγορίες είναι επίσης καλοδεχούμενα, αρκεί να είναι σχετικά με την κοινότητα του GNOME. Το πρόγραμμα σχετικά με την υποβολή των παρουσιάσεων είναι:

  1. Κυριακή, 3 Μαΐου: Προσθεσμία υποβολής των παρουσιάσεων
  2. Κυριακή, 17 Μαΐου: Ενημέρωση των συμμετεχόντων ομιλητών
  3. 7 έως 9 Αυγούστου: Το συνέδριο λαμβάνει μέρος στο Γκέτενμποργκ της Σουηδίας

Παρακαλούμε υποβάλετε τις προτάσεις σας πριν τις 3 Μαΐου μέσω του συστήματος στην ιστοσελίδα <a href="https://www.guadec.org/submit-your-talk/" target="_top" rel="nofollow">https://www.guadec.org/submit-your-talk/</a>. Οι παρουσιάσεις θα ελεχθούν από την επιτροπή προγράμματος μεταξύ 4 και 17 Μαΐου. 

Η επιτροπή προγράμματος περιμένει τις προτάσεις σας για συμμετοχή σας στο συνέδριο GUADEC 2015 ώστε να έχουμε ένα επιτυχημένο συνέδριο. Αν έχετε κάποια απορία σχετικά με το συνέδριο ή τη διαδικασία υποβολής, παρακαλούμε επικοινωνήστε μαζί μας στην διεύθυνση guadec-papers [ατ] gnome [τελεία] org
  
&nbsp;
  
—
  
**Πηγή: <a href="https://www.guadec.org/" title="GUADEC website" target="_blank">https://www.guadec.org/</a>**
