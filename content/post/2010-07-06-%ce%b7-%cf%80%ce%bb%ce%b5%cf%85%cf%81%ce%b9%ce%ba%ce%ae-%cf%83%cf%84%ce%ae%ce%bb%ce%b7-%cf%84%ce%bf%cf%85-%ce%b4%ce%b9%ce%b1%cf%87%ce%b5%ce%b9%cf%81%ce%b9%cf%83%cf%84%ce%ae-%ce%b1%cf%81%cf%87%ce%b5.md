---
author: simosx
categories:
  - Παρουσίαση
date: 2010-07-06 21:00:31
guid: http://gnome.gr/?p=382
id: 382
permalink: /2010/07/06/%ce%b7-%cf%80%ce%bb%ce%b5%cf%85%cf%81%ce%b9%ce%ba%ce%ae-%cf%83%cf%84%ce%ae%ce%bb%ce%b7-%cf%84%ce%bf%cf%85-%ce%b4%ce%b9%ce%b1%cf%87%ce%b5%ce%b9%cf%81%ce%b9%cf%83%cf%84%ce%ae-%ce%b1%cf%81%cf%87%ce%b5/
title: Η πλευρική στήλη του διαχειριστή αρχείων του GNOME (nautilus)
url: /2010/07/06/%ce%b7-%cf%80%ce%bb%ce%b5%cf%85%cf%81%ce%b9%ce%ba%ce%ae-%cf%83%cf%84%ce%ae%ce%bb%ce%b7-%cf%84%ce%bf%cf%85-%ce%b4%ce%b9%ce%b1%cf%87%ce%b5%ce%b9%cf%81%ce%b9%cf%83%cf%84%ce%ae-%ce%b1%cf%81%cf%87%ce%b5/
---

Η [ομάδα του edutorials.gr](http://www.edutorials.gr/node/286) έφτιαξε έναν όμορφο οδηγό βασισμένο σε Flash που εξηγεί πως <a href="http://www.edutorials.gr/node/369" target="_blank">μπορεί να αξιοποιήσει κάποιος καλύτερα την πλευρική στήλη του διαχειριστή παραθύρων του GNOME</a>.