---
author: iosifidis
categories:
  - Ειδήσεις
  - Εικόνες/Στιγμιότυπα
date: 2012-10-08 12:24:27
guid: http://gnome.gr/?p=1931
id: 1931
permalink: /2012/10/08/thessaloniki-3-6-release-party-report/
tags:
  - events
  - facebook
  - gnome
  - gnome 3.6
  - Google
  - linkedin
  - release party
  - twitter
  - θεσσαλονίκη
title: Τι έγινε στο Release Party της 3.6 στη Θεσσαλονίκη
url: /2012/10/08/thessaloniki-3-6-release-party-report/
---

Την Κυριακή 7 Οκτωβρίου, πραγματοποιήθηκε στη Θεσσαλονίκη το release party για την έκδοση 3.6 του GNOME. Λίγο αργοπορημένα θα πείτε αφού η έκδοση <a href="http://gnome.gr/2012/09/26/%CE%BA%CF%85%CE%BA%CE%BB%CE%BF%CF%86%CF%8C%CF%81%CE%B7%CF%83%CE%B5-%CF%84%CE%BF-gnome-3-6/" target="_blank">κυκλοφόρησε στις 26 Σεπτεμβρίου</a>.

Αφού έχουμε τελειώσει όλοι τις υποχρεώσεις μας και γυρίσαμε πίσω στην καθημερινότητά μας, καιρός ήταν να βρεθούμε και να τα πούμε από κοντά.

Ξεκινήσαμε δειλά δειλά να μαζευόμαστε από τις 6 το απογευματάκι. Ποτά, συζητήσεις περί ανοικτού λογισμικού, κοινοτήτων και όχι μόνο, ήταν ότι περιείχε το πάρτι.

Δείτε Φωτογραφίες:

 <img class="size-thumbnail wp-image-1968 alignleft img-responsive " title="Say Cheese!!" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2058-150x150.jpg" alt="" /> <img class=" wp-image-1956 alignleft img-responsive " title="Η παρέα" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2062-150x150.jpg" alt="" /><img class="alignleft size-thumbnail wp-image-1955 img-responsive " title="Κουβεντούλα.." src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2061-150x150.jpg" alt="" />
  
 <img class="alignleft size-thumbnail wp-image-1954 img-responsive " title="Τρελοπαρέα!!" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2060-150x150.jpg" alt="" /> <img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2058-150x150.jpg" alt="" title="Say Cheese!!" class="alignleft size-thumbnail wp-image-1968 img-responsive " /><img class="alignleft size-thumbnail wp-image-1951 img-responsive " title="Έπεσε μελέτη.." src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/10/DSCN2057-150x150.jpg" alt="" />
