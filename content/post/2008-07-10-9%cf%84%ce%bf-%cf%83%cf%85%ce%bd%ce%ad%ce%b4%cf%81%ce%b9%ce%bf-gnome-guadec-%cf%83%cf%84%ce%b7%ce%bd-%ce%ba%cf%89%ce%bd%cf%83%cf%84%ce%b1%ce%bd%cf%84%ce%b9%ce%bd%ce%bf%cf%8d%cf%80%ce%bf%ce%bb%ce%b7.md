---
author: simosx
categories:
  - Ειδήσεις
date: 2008-07-10 12:44:39
guid: http://gnome.gr/?p=28
id: 28
permalink: /2008/07/10/9%cf%84%ce%bf-%cf%83%cf%85%ce%bd%ce%ad%ce%b4%cf%81%ce%b9%ce%bf-gnome-guadec-%cf%83%cf%84%ce%b7%ce%bd-%ce%ba%cf%89%ce%bd%cf%83%cf%84%ce%b1%ce%bd%cf%84%ce%b9%ce%bd%ce%bf%cf%8d%cf%80%ce%bf%ce%bb%ce%b7/
title: 9το Συνέδριο GNOME (GUADEC) στην Κωνσταντινούπολη
url: /2008/07/10/9%cf%84%ce%bf-%cf%83%cf%85%ce%bd%ce%ad%ce%b4%cf%81%ce%b9%ce%bf-gnome-guadec-%cf%83%cf%84%ce%b7%ce%bd-%ce%ba%cf%89%ce%bd%cf%83%cf%84%ce%b1%ce%bd%cf%84%ce%b9%ce%bd%ce%bf%cf%8d%cf%80%ce%bf%ce%bb%ce%b7/
---

[<img class="alignnone size-medium wp-image-29 img-responsive " title="p7100002" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2008/07/p7100002.jpg" alt="" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2008/07/p7100002.jpg)

Αυτή την εβδομάδα γίνεται το <a href="http://www.guadec.org/" target="_blank">συνέδριο GUADEC</a> (Χρήστες και Προγραμματιστές του γραφικού περιβάλλοντος GNOME) στην Κωνσταντινούπολη.

Από Ελλάδα έχουν έρθει πέντε άτομα, ο Δημήτρης Γ, ο Γιώργος Λ., ο Μπάμπης Μ, ο Μιχάλης Κ και Σίμος Ξ.

Αυτά για τώρα. <span style="text-decoration: line-through">Πάμε στο παλάτι Ντολμά Μπαχτσέ (πραγματικό όνομα τοπικού παλατιού!) </span>(κλειστό τις Πέμπτες)
