---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2015-02-27 16:25:06
guid: http://gnome.gr/?p=3015
id: 3015
permalink: /2015/02/27/proodos-metafrasis-3-16/
tags:
  - '3.16'
  - gnome
  - μεταφράσεις
  - μετάφραση
title: Εβδομαδιαία πρόοδος μετάφρασης της έκδοσης 3.16
url: /2015/02/27/proodos-metafrasis-3-16/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n.png" alt="" class="aligncenter size-medium wp-image-2734 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n.png)

Αν και δεν σταματήσαμε να μεταφράζουμε από την κυκλοφορία της έκδοσης 3.14, την <a href="http://gnome.gr/2015/02/20/enarksi-metafrasis-tou-gnome-3-16/" target="_blank">προηγούμενη εβδομάδα</a> ξεκινήσαμε την μετάφραση της έκδοσης 3.16 η οποία θα είναι έτοιμη στις 25 Μαρτίου.

Το αποτέλεσμα αυτής της εβδομάδας είναι εκπληκτικό, καθώς την στιγμή που γράφεται το παρόν, τα στατιστικά έχουν ως εξής:

**Γραφική διεπαφή** 99% μεταφρασμένη, 5 strings που θέλουν αναθεώρηση και 5 strings που θέλουν μετάφραση.

**Τεκμηρίωση** 98% μεταφρασμένη, 106 strings που θέλουν αναθεώρηση και 277 strings που θέλουν μετάφραση.

Σαν ποσοστά δεν άλλαξαν ιδιαίτερα διότι τα string που είναι προς μετάφραση και αναθεώρηση είναι απειροελάχιστα σε σχέση με το σύνολο των strings. Σαν απόλυτος αριθμός όμως είναι ιδιαίτερα ικανοποιητικά για μια εβδομάδα. Επίσης μην ξεχνάμε ότι υπάρχουν αλλαγές των αρχείων σχεδόν καθημερινά.

Όσοι από εσάς θέλετε να βοηθήσετε να ολοκληρωθεί η μετάφραση, μπορείτε να ακολουθήσετε τα βήματα που περιγράφονται στην <a href="http://gnome.gr/contribute/" target="_blank">ιστοσελίδα συμμετοχής</a> ή να <a href="https://www.youtube.com/watch?v=sH-STvL82-Y" target="_blank">δείτε το βίντεο</a>.

Για οποιαδήποτε απορία έχετε, μπορούμε να σας βοηθήσουμε, είτε στο <a href="https://www.facebook.com/gnomegr" target="_blank">facebook</a>, <a href="https://plus.google.com/communities/101177026822277687297" target="_blank">google +</a>, <a href="http://gnome.gr/contact/irc-gnome-el/" target="_blank">IRC</a> (επιμείνετε λίγο εκεί) ή καλύτερα στην <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα αλληλογραφίας</a>.
