---
title: Επικοινωνία
---

  * Η [λίστα αλληλογραφίας](https://mail.gnome.org/mailman/listinfo/gnome-el-list) της ελληνικής κοινότητας του GNOME.
  * To κανάλι **#gnome-el** στο δίκτυο Freenode ή στο <a href="https://wiki.gnome.org/Community/GettingInTouch/IRC#Step_2:_Add_the_GNOME_IRC_Server" target="_blank">GIMPNet</a>. Μπορείτε να συνδεθείτε στο κανάλι **#gnome-el** με έναν περιηγητή διαδικτύου. Από το Linux συνδεθείτε μέσω του <a title="Polari" href="https://wiki.gnome.org/Apps/Polari" target="_blank">Polari</a> ή του <a title="XChat" href="http://xchat.org/" target="_blank">XChat</a>, βρίσκοντας τα στην επισκόπηση «Δραστηριότητες» του GNOME shell. Εναλλακτικά μπορείτε να συνδεθείτε από τον περιηγητή σας, χρησιμοποιώντας το <a href="http://webchat.freenode.net/?channels=gnome-el" target="_blank">webchat</a> του freenode.
  * Μπορείτε επίσης να μας βρείτε και στα [social media](http://www.gnome.gr/#social_media).

* * *

<div class="alert alert-info">
  Το κύριο μέσο για την επικοινωνία αλλά και το συντονισμό της ελληνικής κοινότητας του GNOME είναι η λίστα αλληλογραφίας μας !
</div>
